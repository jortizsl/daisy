//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyerror.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	01-Dec-2016
 * @brief	Daisy error
 */

/*
 * INCLUDES
 */

#include "daisyerror.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start err namespace
namespace err
{

int error = 0;

// Convert an error code to a string
char *strerror(int errnum)
{
	switch (errnum)
	{
	// No error
	case NOERR:
		strcpy(detail::errorstr, "[NOERR] No error");
		break;

	// Segment error
	case EINVSEG:
		strcpy(detail::errorstr, "[EINVSEG] Invalid segment");
		break;

	// Shared memory errors
	case ESHMOPEN:
		strcpy(detail::errorstr, "[ESHMOPEN] Error opening shared memory");
		break;
	case ESHMTRUNC:
		strcpy(detail::errorstr, "[ESHMTRUNC] Error truncating shared memory");
		break;
	case ESHMSIZE:
		strcpy(detail::errorstr, "[ESHMSIZE] Invalid size of shared memory");
		break;
	case ESHMMAP:
		strcpy(detail::errorstr, "[ESHMMAP] Error mapping shared memory");
		break;
	case ESHMINV:
		strcpy(detail::errorstr, "[ESHMINV] Invalid shared memory pointer");
		break;
	case ESHMUNMAP:
		strcpy(detail::errorstr, "[ESHMUNMAP] Error unmapping shared memory");
		break;
	case ESHMUNLINK:
		strcpy(detail::errorstr, "[ESHMUNLINK] Error unlinking shared memory");
		break;

	// Semaphore errors
	case ESEMINIT:
		strcpy(detail::errorstr, "[ESEMINIT] Error initing semaphore");
		break;
	case ESEMDES:
		strcpy(detail::errorstr, "[ESEMDES] Error destroying semaphore");
		break;
	case ESEMLOCK:
		strcpy(detail::errorstr, "[ESEMLOCK] Error locking semaphore");
		break;
	case ESEMUNLOCK:
		strcpy(detail::errorstr, "[ESEMUNLOCK] Error unlocking semaphore");
		break;

	// Standard TFTP errors
	case ENODEF:
		strcpy(detail::errorstr, "[ENODEF] Not defined, see error message (if any)");
		break;
	case ENOTFOUND:
		strcpy(detail::errorstr, "[ENOTFOUND] File not found");
		break;
	case EACCVIOL:
		strcpy(detail::errorstr, "[EACCVIOL] Access violation");
		break;
	case EFULL:
		strcpy(detail::errorstr, "[EFULL] Disk full or allocation exceeded");
		break;
	case EILLOP:
		strcpy(detail::errorstr, "[EILLOP] Illegal TFTP operation");
		break;
	case EUNKID:
		strcpy(detail::errorstr, "[EUNKID] Unknown transfer ID");
		break;
	case EFILEEXISTS:
		strcpy(detail::errorstr, "[EFILEEXISTS] File already exists");
		break;
	case ENOUSER:
		strcpy(detail::errorstr, "[ENOUSER] Not such user");
		break;

	// Other TFTP errors
	case ESOCK:
		strcpy(detail::errorstr, "[ESOCK] Error creating socket");
		break;
	case EBIND:
		strcpy(detail::errorstr, "[EBIND] Errr binding socket");
		break;
	case ERESVHOST:
		strcpy(detail::errorstr, "[ERESVHOST] Error resolving host");
		break;
	case ESENDREQ:
		strcpy(detail::errorstr, "[ESENDREQ] Error sending request");
		break;
	case ERECV:
		strcpy(detail::errorstr, "[ERECV] Error receiving data");
		break;
	case ETIMEOUT:
		strcpy(detail::errorstr, "[ETIMEOUT] Timeout");
		break;
	case EBLK:
		strcpy(detail::errorstr, "[EBLK] Wrong data block");
		break;
	case EOPCODE:
		strcpy(detail::errorstr, "[EOPCODE] Unexpected opcode");
		break;
	case EACK:
		strcpy(detail::errorstr, "[EACK] Error sending/receiving ACK");
		break;
	case EOPEN:
		strcpy(detail::errorstr, "[EOPEN] Error opening file or memory for read or write");
		break;
	case EMODE:
		strcpy(detail::errorstr, "[EMODE] Wrong mode or compression");
		break;

	// Server & client errors
	case EMSQCRT:
		strcpy(detail::errorstr, "[EMSQCRT] Error creating message queue");
		break;
	case ENOMSQ:
		strcpy(detail::errorstr, "[ENOMSQ] Message queue not initialized");
		break;
	case EMSQSEND:
		strcpy(detail::errorstr, "[EMSQSEND] Error sending message queue message");
		break;
	case ESETOPT:
		strcpy(detail::errorstr, "[ESETOPT] Error setting socket option");
		break;

	// Unknown error
	default:
		strcpy(detail::errorstr, "[?] Unknown error");
	}

	return detail::errorstr;
}


	/* detail namespace for private functions */

namespace detail
{
	char errorstr[ERROR_STR_LEN] = "";
}

// End err namespace
}

// End daisy namespace
}
