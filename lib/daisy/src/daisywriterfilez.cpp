//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisywriterfilez.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	13-Dec-2016
 * @brief	Daisy writer from compressed file
 */

/*
 * INCLUDES
 */

#include <time.h>

#include "daisywriterfilez.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: FileWriterZ
 */

FileWriterZ::FileWriterZ() :
	FileWriter()
{
	this->type = TYPE_FILE_Z;

	this->bufferOut = NULL;
	this->bufferOutLen = 0;

	this->firstChunk = true;
}

FileWriterZ::~FileWriterZ()
{
	if (this->bufferOut != NULL)
		delete [] this->bufferOut;
	this->bufferOut = NULL;
	this->bufferOutLen = 0;
}

bool FileWriterZ::open(
	const char *name,
	unsigned long beg,
	unsigned long end)
{
	// Open file
	if (!FileWriter::open(name))
		return false;

	// We are starting a new stream
	this->firstChunk = true;

	return true;
}

void FileWriterZ::close()
{
	// Stop inflater
	this->inf.stopInflate();

	// Write data to file
	this->file.write((char*)this->bufferOut, this->bufferOutLen);

	// Close file
	FileWriter::close();

	if (this->bufferOut != NULL)
		delete [] this->bufferOut;
	this->bufferOut = NULL;
	this->bufferOutLen = 0;

	// Print stats
	//this->inf.printStats();
}

void FileWriterZ::abort()
{
	// Stop inflater
	this->inf.abortInflate();

	// Close file
	FileWriter::close();

	if (this->bufferOut != NULL)
		delete [] this->bufferOut;
	this->bufferOut = NULL;
	this->bufferOutLen = 0;

	// Print stats
	//this->inf.printStats();
}

int FileWriterZ::write(const char *buffer, unsigned long lBuffer)
{
	// The first data contains the file size
	if (this->firstChunk)
	{
		this->firstChunk = false;

		if (this->bufferOutLen != 0)
			delete [] this->bufferOut;

		// Init buffer
		this->bufferOutLen = *(uint64_t*)buffer;
		this->bufferOut = new unsigned char[this->bufferOutLen];

		// Init inflater
		this->inf.init(this->bufferOut, this->bufferOutLen);

		// Start inflater
		this->inf.startInflate();

		// Put next chunk
		this->inf.putNextChunk(
			(unsigned char*)&buffer[sizeof(uint64_t)],
			lBuffer - sizeof(uint64_t));

		return lBuffer - sizeof(uint64_t);
	}
	else
	{
		this->inf.putNextChunk((unsigned char*)buffer, lBuffer);
		return lBuffer;
	}
}

// End daisy namespace
}
