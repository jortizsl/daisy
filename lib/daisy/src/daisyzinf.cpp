//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyzinf.cpp
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	09-Dec-2016
 * @brief	Class for deflating serialized data using Z lib
 */

/*
 * INCLUDES
 */

#include "daisyzinf.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start z namespace
namespace z
{


/*
 * CLASS
 */

Inflater::Inflater()
{
	this->bufferInLen = 0;
	this->bufferIn = NULL;

	this->bufferOutLen = 0;
	this->bufferOut = NULL;

	this->running = false;
}

Inflater::~Inflater()
{
	this->stopInflate();

	if (this->bufferIn != NULL)
		delete [] this->bufferIn;
}

bool Inflater::init(
	unsigned char *bufferOut,
	unsigned long bufferOutLen)
{
    // Stop inflate if necessary
	this->stopInflate();

	// Keep buffer out address and size
	this->bufferOut = bufferOut;
	this->bufferOutLen = bufferOutLen;

	// Init deflate
	this->strm.zalloc = Z_NULL;
    this->strm.zfree = Z_NULL;
    this->strm.opaque = Z_NULL;
    this->strm.avail_in = 0;
    this->strm.next_in = Z_NULL;

    if (inflateInit(&this->strm) != Z_OK)
    {
    	this->bufferOut = NULL;
    	this->bufferOutLen = 0;
    	return false;
    }

	// Allocate input buffer
    unsigned long lengthBound = deflateBound(&this->strm, bufferOutLen);
	if (this->bufferIn == NULL)
	{
		this->bufferInLen = lengthBound;
		this->bufferIn = new unsigned char[this->bufferInLen];
	}
	else if (this->bufferInLen != lengthBound)
	{
		this->bufferInLen = lengthBound;
		delete [] this->bufferIn;
		this->bufferIn = new unsigned char[this->bufferInLen];
	}
	this->bufferInCurr = 0;
	this->bufferInPos = 0;

	// Return OK
    return true;
}

void Inflater::startInflate()
{
	if (this->bufferOut != NULL && this->bufferOutLen != 0)
	{
		this->running = true;
		pthread_create(&this->threadInflate, NULL, Inflater::runInflate, this);
	}
}

void Inflater::stopInflate()
{
	if (this->running)
	{
		pthread_join(this->threadInflate, NULL);
	}
}

void Inflater::abortInflate()
{
	if (this->running)
	{
		this->running = false;
		pthread_join(this->threadInflate, NULL);
	}
}

bool Inflater::putNextChunk(
	unsigned char *buffer, unsigned long size)
{
	if (this->bufferInPos + size > this->bufferInLen)
		return false;

	memcpy(&this->bufferIn[this->bufferInPos], buffer, size);
	this->bufferInPos += size;

	return true;
}

void Inflater::printStats()
{
	this->timeTotal = this->timeInit + this->timeLoop;
	double timeOthers =
		this->timeLoop -
		this->timeZ -
		this->timeAux;
	double bandwidth =
		(double)this->bufferInLen / this->timeTotal * 1000 / 1024 * 8;
	std::cout << "Total processing time: " << this->timeTotal << " ms\n";
	std::cout << "  Time init: " << this->timeInit << " ms\n";
	std::cout << "  Time loop: " << this->timeLoop << " ms\n";
	std::cout << "    Time z     : " << this->timeZ << " ms\n";
	std::cout << "    Time aux   : " << this->timeAux << " ms\n";
	std::cout << "    Time others: " << timeOthers << " ms\n";
	std::cout << "  Buffer size : " << this->bufferInLen << " bytes\n";
	std::cout << "  Bandwidth   : " << bandwidth << " kbps\n";
}

void *Inflater::runInflate(void *data)
{
	struct timespec timeInit;
	struct timespec timeFinish;
	struct timespec time0;
	struct timespec time1;

	clock_gettime(CLOCK_REALTIME, &time0);

	Inflater *inf = (Inflater*)data;
	int ret;

	inf->bufferInCurr = 0;
	inf->bufferOutPos = 0;
	unsigned long bufferInPosAux;

	inf->timeLoop = 0.0;
	inf->timeZ = 0.0;
	inf->timeAux = 0.0;

	clock_gettime(CLOCK_REALTIME, &time1);
	inf->timeInit += utils::getms(time0, time1);

	clock_gettime(CLOCK_REALTIME, &timeInit);

    do
    {
    	bufferInPosAux = inf->bufferInPos;

    	if (bufferInPosAux <= inf->bufferInCurr)
    	{
    		usleep(100);
    		continue;
    	}

    	inf->strm.avail_in = bufferInPosAux - inf->bufferInCurr;
        inf->strm.next_in = &inf->bufferIn[inf->bufferInCurr];
        inf->bufferInCurr = bufferInPosAux;

        do
        {
            inf->strm.avail_out = inf->bufferOutLen - inf->bufferOutPos;
            inf->strm.next_out = &inf->bufferOut[inf->bufferOutPos];

            clock_gettime(CLOCK_REALTIME, &time0);
            ret = inflate(&inf->strm, Z_NO_FLUSH);
            clock_gettime(CLOCK_REALTIME, &time1);
            inf->timeZ += utils::getms(time0, time1);

            if (ret != Z_OK && ret != Z_STREAM_END)
            {
            	inf->running = false;
            	inf->bufferOutPos = 0;
            	break;
            }

            inf->bufferOutPos = inf->strm.total_out;

            if (ret == Z_STREAM_END)
            {
            	inf->running = false;
            	break;
            }

        } while (inf->strm.avail_out == 0 && inf->running);
    } while (ret != Z_STREAM_END && inf->running);

    clock_gettime(CLOCK_REALTIME, &time0);
    inflateEnd(&inf->strm);
    clock_gettime(CLOCK_REALTIME, &time1);
	inf->timeInit += utils::getms(time0, time1);
    inf->running = false;

    clock_gettime(CLOCK_REALTIME, &timeFinish);
    inf->timeLoop = utils::getms(timeInit, timeFinish);
	return NULL;
}

// End z namespace
}

// End daisy namespace
}
