//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisymem.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	30-Nov-2016
 * @brief	Daisy library
 */

/*
 * INCLUDES
 */

#include "daisymem.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start mem namespace
namespace mem
{

daisy::Client client;

// detail namespace for private variables and functions
namespace detail
{
	// Memory segments
	std::trie<Segment> segments;

	// Release all the resources
	void finish()
	{
		// Free the mmemory of the segments
		std::trie<Segment>::iterator segment;
		for (segment = segments.begin();
			 segment != segments.end();
			 segment++)
		{
			unlockSegment(&segment->second);
			deallocateSegment(&segment->second);
		}

		// Delete the segments
		segments.clear();
	}

	// Register a new segment
	bool registerSegment(
		const std::string &name,
		ID id,
		unsigned long size)
	{
		// Check if the segment already exists
		std::trie<Segment>::iterator segment;
		segment = segments.find(name.c_str());

		if (segment != segments.end())
		{
			// Check the other segments
			if (segment->second.id != id ||
				segment->second.sizeData != size)
				return false;

			// Add a new user
			segment->second.users++;

			// Check if we need to allocate memory
			return allocateSegment(&segment->second);
		}

		// Create the new segment
		Segment segmentNew;
		segmentNew.name = name;
		segmentNew.nameShared = std::string("/daisy-shm-") + name;
		segmentNew.id = id;
		segmentNew.users = 1;

		segmentNew.size = sizeof(sem_t) + sizeof(uint8_t) + size;
		segmentNew.addr = NULL;
		segmentNew.sem = NULL;
		segmentNew.sizeData = size;
		segmentNew.data = NULL;

		// Allocate the segment
		if (!allocateSegment(&segmentNew))
			return false;

		// Add the segment
		return segments.insert(name, segmentNew).second;
	}

	// Unregister segment
	bool unregisterSegment(const std::string &name)
	{
		// Look for the segment
		std::trie<Segment>::iterator segment;
		segment = segments.find(name.c_str());

		if (segment == segments.end())
			return false;

		// Remove user
		segment->second.users--;

		if (segment->second.users == 0)
		{
			unlockSegment(&segment->second);
			deallocateSegment(&segment->second);
			segments.erase(segment);
		}

		return true;
	}

	// Get a memory segment
	Segment *getSegment(const std::string &name)
	{
		std::trie<Segment>::iterator segment;
		segment = segments.find(name);
		if (segment == segments.end())
			return NULL;
		else
			return &segment->second;
	}

	// Generate a segment ID from a name
	ID getId(const std::string &name)
	{
		ID id;
		memset(&id, 0, DAISY_IDSIZE);
		unsigned long crc = crc32(0, NULL, 0);
		crc = crc32(crc, (const Bytef*)name.c_str(), name.size());
		memcpy(&id, &crc, DAISY_IDSIZE);
		return id;
	}

	// Print ID
	void printId(ID &id)
	{
		printf("%02X%02X%02X%02X", id.id[0], id.id[1], id.id[2], id.id[3]);
	}

	// Allcates a memory segment in normal memory
	bool allocateSegment(
		Segment *segment,
		bool clear)
	{
		// Check segment
		if (segment == NULL)
		{
			err::error = err::EINVSEG;
			return false;
		}

		// Check if memory is already allocated
		if (segment->addr != NULL)
			return true;

		// Open shared memory segment
		int fd = shm_open(
			segment->nameShared.c_str(), O_CREAT | O_RDWR, S_IRWXU);

		// Check shared memory file descriptor
		if (fd == -1)
		{
			err::error = err::ESHMOPEN;
			return false;
		}

		// If the shared memory already exists, then the size would be different
		// of zero. If it's still zero, then we set the size
		struct stat sb;
		fstat(fd, &sb);

		if (sb.st_size == 0)
		{
			if (ftruncate(fd, segment->size) == -1)
			{
				err::error = err::ESHMTRUNC;
				return false;
			}
		}
		else if (sb.st_size != (long)segment->size)
		{
			err::error = err::ESHMSIZE;
			return false;
		}

		// Map the memory
		segment->addr = mmap(
			NULL, segment->size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

		// Check if memory was mapped properly
		if (segment->addr == NULL)
		{
			err::error = err::ESHMMAP;
			return false;
		}

		segment->sem = (sem_t*)segment->addr;
		segment->nproc = (uint8_t*)(((char*)segment->addr) + sizeof(sem_t));
		segment->data = (void*)(((char*)segment->addr) + sizeof(sem_t) + sizeof(uint8_t));

		// Init semaphore
		if (sem_init(segment->sem, 1, 1) == -1)
		{
			err::error = err::ESEMINIT;
			return false;
		}

		// Increment process counter
		if (sem_wait(segment->sem) == -1)
		{
			err::error = err::ESEMLOCK;
			return false;
		}
		(*segment->nproc)++;
		if (sem_post(segment->sem) == -1)
		{
			err::error = err::ESEMUNLOCK;
			return false;
		}

		// Return OK
		return true;
	}

	// Deallocates a memory segment
	bool deallocateSegment(
		Segment *segment)
	{
		// Check segment
		if (segment == NULL)
		{
			err::error = err::EINVSEG;
			return false;
		}

		// Check if memory is already deallocated
		if (segment->addr == NULL)
			return true;

		// Decrement process counter
		bool mustClose = false;
		if (sem_wait(segment->sem) == -1)
		{
			err::error = err::ESEMLOCK;
			return false;
		}
		(*segment->nproc)--;
		if ((*segment->nproc) == 0)
			mustClose = true;
		if (sem_post(segment->sem) == -1)
		{
			err::error = err::ESEMUNLOCK;
			return false;
		}

		// Destroy semaphore
		if (mustClose)
		{
			if (sem_destroy(segment->sem) == -1)
			{
				err::error = err::ESEMDES;
				return false;
			}
		}
		segment->sem = NULL;

		// Unmap the shared memory
		if (munmap(segment->addr, segment->size) == -1)
		{
			err::error = err::ESHMUNMAP;
			return false;
		}
		segment->size = 0;
		segment->addr = NULL;
		segment->sizeData = 0;
		segment->data = NULL;

		// Unlink shared memory
		if (mustClose)
		{
			if (shm_unlink(segment->nameShared.c_str()) == -1)
			{
				err::error = err::ESHMUNLINK;
				return false;
			}
		}

		// Return OK
		return true;
	}

	// Locks a memory segment
	bool lockSegment(
		Segment *segment)
	{
		if (segment == NULL)
		{
			err::error = err::EINVSEG;
			return false;
		}

		if (segment->sem == NULL)
		{
			err::error = err::ESHMINV;
			return false;
		}

		if (sem_wait(segment->sem) == -1)
		{
			err::error = err::ESEMLOCK;
			return false;
		}

		return true;
	}

	// Unlocks a memory segment
	bool unlockSegment(
		Segment *segment)
	{
		if (segment == NULL)
		{
			err::error = err::EINVSEG;
			return false;
		}

		if (segment->sem == NULL)
		{
			err::error = err::ESHMINV;
			return false;
		}

		if (sem_post(segment->sem) == -1)
		{
			err::error = err::ESEMUNLOCK;
			return false;
		}

		return true;
	}
}

// End mem namespace
}

// End daisy namespace
}
