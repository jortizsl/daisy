//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyserver.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	14-Mar-2017
 * @brief	Daisy server
 */

/*
 * INCLUDES
 */

#include "daisyserver.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{


/*
 * CLASS
 */

// Constructor
Server::Server()
{
	this->udp_s = 0;
	this->udp_lBuffer = 0;

	this->msq = 0;

	pthread_mutex_init(&this->reqMutex, NULL);

	this->running = false;
}

// Desctructor
Server::~Server()
{
}

// Start the server
bool Server::init(
	int udpport,
	const std::string &udpdev,
	const std::string &msqname,
	int tftpport,
	bool resetMsq)
{
	this->finish();
	this->running = true;


		/* UDP */

	// Get local and broadcast IP
	if (!this->setLocalNBroadcastAddr(udpdev.c_str()))
	{
		err::error = err::ERESVHOST;
    	this->running = false;
		return false;
	}

	// Create socket
	this->udp_s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (this->udp_s <= 0)
	{
		err::error = err::ESOCK;
    	this->running = false;
		return false;
	}

	// Configuring local address
	memset((void*)&this->udp_laddr, 0, sizeof(this->udp_laddr));
	this->udp_laddr.sin_family = AF_INET;
	this->udp_laddr.sin_port = htons(udpport);
	this->udp_laddr.sin_addr.s_addr = INADDR_ANY;

	// Set broadcast
	int broadcastOn = 1;
	if (setsockopt(this->udp_s, SOL_SOCKET, SO_BROADCAST,
		   &broadcastOn, sizeof(broadcastOn)) < 0)
	{
		err::error = err::ESETOPT;
		close(this->udp_s);
		this->udp_s = 0;
    	this->running = false;
		return false;
	}

	// Set port reuse
	int portreuseOn = 1;
    if (setsockopt(this->udp_s, SOL_SOCKET, SO_REUSEPORT,
    		&portreuseOn, sizeof(portreuseOn)) < 0)
    {
    	err::error = err::ESETOPT;
		close(this->udp_s);
		this->udp_s = 0;
    	this->running = false;
		return false;
    }

    // Bind socket
	if (bind(
		this->udp_s,
		(struct sockaddr*)&this->udp_laddr,
		sizeof(this->udp_laddr)) == -1)
	{
		err::error = err::EBIND;
		close(this->udp_s);
		this->udp_s = 0;
    	this->running = false;
		return false;
	}

	// Configuring broadcast address
	memset((void*)&this->udp_baddr, 0, sizeof(this->udp_baddr));
	this->udp_baddr.sin_family = AF_INET;
	this->udp_baddr.sin_port = htons(udpport);
	this->udp_baddr.sin_addr.s_addr = this->ipv4Broadcast.s_addr;

	// Launch UDP thread
	pthread_create(&this->udp_thread, NULL, Server::udp_run, (void*)this);


		/* Message queue */

	// Open message queue
	struct mq_attr attr;
	attr.mq_msgsize = server::MSQ_MSGSIZE;
	attr.mq_maxmsg = server::MSQ_MAXMSG;

	this->msq = mq_open(
		msqname.c_str(),
		O_CREAT | O_RDONLY,
		S_IRUSR | S_IWUSR,
		&attr);

	if (this->msq == (mqd_t)-1)
	{
		err::error = err::EMSQCRT;
		this->running = false;
		return false;
	}

	// Empty message queue
	if (resetMsq)
	{
		struct timespec timeout;
		ssize_t res;
		do
		{
			this->msq_lBuffer = server::BUFFER_SIZE;
			clock_gettime(CLOCK_REALTIME, &timeout);
			timeout.tv_sec += 1;
			res = mq_timedreceive(
				this->msq, this->msq_buffer, this->msq_lBuffer, 0, &timeout);
		} while (res > 0);
	}

	// Launch message queue thread
	pthread_create(&this->msq_thread, NULL, Server::msq_run, (void*)this);


		/* TFTP server */

	int ret;

	if ((ret = this->tftpServer.listen(tftpport)) != daisy::err::NOERR)
    {
		this->running = false;
    	return false;
    }

	this->tftpPort = tftpport;


		/* Finish */

	return true;
}

// Close all the services of the server
bool Server::finish()
{
	// Stop threads
	if (this->running)
	{
		this->running = false;
		pthread_join(this->udp_thread, NULL);
		pthread_join(this->msq_thread, NULL);
	}

	// Close UDP connection
	if (this->udp_s != 0)
	{
		close(this->udp_s);
		this->udp_s = 0;
	}

	// Close Message queue
	if (this->msq != 0)
	{
		mq_close(this->msq);
		this->msq = 0;
	}

	// Stop TFTP server
	int ret;

	if ((ret = this->tftpServer.finish()) != daisy::err::NOERR)
    	return false;

	// Return OK
	return true;
}

// UDP thread function
void *Server::udp_run(void *_)
{
	Server *srv = (Server*)_;

	struct sockaddr_in remoteAddr;
	socklen_t remoteAddrLen;

	unsigned short remotePort;
	//int32_t id;
	std::string name;
	std::string semName;
	uint32_t beg;
	uint32_t end;

	server::Request req;
	std::trie<server::Request>::iterator reqIt;
	server::Segment seg;
	std::deque<server::Segment>::iterator segIt;

	bool overlap;

	struct timeval timeout;
	timeout.tv_sec =  1;
    timeout.tv_usec = 0;

    if (setsockopt(srv->udp_s, SOL_SOCKET, SO_RCVTIMEO,
    		(char*)&timeout, sizeof(timeout)) < 0)
	{
    	err::error = err::ESETOPT;
    	return NULL;
	}

	while (srv->running)
	{
		// Receive data
		remoteAddrLen = sizeof(remoteAddr);
		srv->udp_lBuffer = recvfrom(
			srv->udp_s,
			srv->udp_buffer,
			server::BUFFER_SIZE,
			0,
			(struct sockaddr*)&remoteAddr,
			&remoteAddrLen);

		// Check if we received any data
		if (srv->udp_lBuffer <= 0)
			continue;

		// Process data
		memcpy(&remotePort, srv->udp_buffer, sizeof(unsigned short));
		//memcpy(&id, srv->udp_buffer, sizeof(int32_t));
		Server::getDataFromBuffer(
			&srv->udp_buffer[sizeof(unsigned short)],
			srv->udp_lBuffer - sizeof(unsigned short),
			name, semName, beg, end);

		// Display data for debug
#ifdef DAISYSERVER_DEBUG
		std::cout << "UDP:\n";
		std::cout << "  port   : " << remotePort << "\n";
		//std::cout << "  id  : " << id << "\n";
		std::cout << "  name   : " << name << "\n";
		std::cout << "  beg    : " << beg << "\n";
		if (end == server::END)
			std::cout << "  end    : END" << std::endl;
		else
			std::cout << "  end    : " << end << std::endl;
#endif

		// Check sender (we don't want to process data coming from ourselves)
		if (srv->ipv4Local.s_addr == remoteAddr.sin_addr.s_addr &&
			remotePort == srv->tftpPort)
		{
#ifdef DAISYSERVER_DEBUG
			std::cout << "Discarded" << std::endl;
#endif
			continue;
		}

		// Add data to list
		pthread_mutex_lock(&srv->reqMutex);
		reqIt = srv->requests.find(name);

		if (reqIt == srv->requests.end())
		{
			// Element not found, add it
			seg.remoteAddr = remoteAddr;
			//seg.id = id;
			seg.remotePort = remotePort;
			seg.beg = beg;
			seg.end = end;
			req.segments.push_back(seg);
			srv->requests.insert(name, req);
		}
		else
		{
			// Element found
			if (beg == 0 && end == server::END)
			{
				// Replace all the segments
				seg.remoteAddr = remoteAddr;
				//seg.id = id;
				seg.remotePort = remotePort;
				seg.beg = beg;
				seg.end = end;
				reqIt->second.segments.clear();
				reqIt->second.segments.push_back(seg);
			}
			else
			{
				overlap = false;

				for (segIt = reqIt->second.segments.begin();
					 segIt != reqIt->second.segments.end();
					 segIt++)
				{
					// Check full overlap
					if (beg <= segIt->beg && end >= segIt->end)
					{
						if (overlap)
						{
							// If we are already in overlap -> Delete segment
							segIt = reqIt->second.segments.erase(segIt);
						}
						else
						{
							// Full overlapping -> Replace segment
							overlap = true;
							segIt->remoteAddr = remoteAddr;
							//segIt->id = id;
							segIt->remotePort = remotePort;
							segIt->beg = beg;
							segIt->end = end;
						}
					}

					if (segIt == reqIt->second.segments.end())
						break;
				}

				// No overlapping -> Add segment
				if (!overlap)
				{
					seg.remoteAddr = remoteAddr;
					//seg.id = id;
					seg.remotePort = remotePort;
					seg.beg = beg;
					seg.end = end;
					reqIt->second.segments.push_back(seg);
				}
			}
		}

		// Print requests for debugging
#ifdef DAISYSERVER_DEBUG
		std::cout << "Requests:\n";
		for (reqIt = srv->requests.begin();
			 reqIt != srv->requests.end();
			 reqIt++)
		{
			std::cout << "  " << reqIt->first << ":\n";

			for (segIt = reqIt->second.segments.begin();
				 segIt != reqIt->second.segments.end();
				 segIt++)
			{
				std::cout << "    " << segIt->beg << " - ";
				if (segIt->end == server::END)
					std::cout << "END: ";
				else
					std::cout << segIt->end << ": ";
				std::cout << inet_ntoa(segIt->remoteAddr.sin_addr) << ":";
				std::cout << segIt->remotePort << "\n";
			}
		}
		std::cout << std::endl;
#endif

		pthread_mutex_unlock(&srv->reqMutex);
	}

	srv->running = false;
	return NULL;
}

// Message queue thread function
void *Server::msq_run(void *_)
{
	Server *srv = (Server*)_;

	struct timeval timeoutset;
	fd_set rdset;
	ssize_t t;

	server::Request req;
	std::trie<server::Request>::iterator reqIt;
	std::deque<server::Segment>::iterator segIt;

	char header;
	std::string name;
	std::string semName;
	uint32_t beg;
	uint32_t end;

	tftp::Client client;
	int ret;
	client.setBlockSize(32768);
	client.setMode(tftp::MODE_SHM);
	client.setCompression(tftp::COMPRESSION_NONE);

	while (srv->running)
	{
		// Wait for anster
		timeoutset.tv_sec = 1;
		timeoutset.tv_usec = 0;

		FD_ZERO(&rdset);
		FD_SET(srv->msq, &rdset);
		t = select(srv->msq + 1, &rdset, NULL, NULL, &timeoutset);

		if (t == 0)		// Timeout
			continue;
		else if (t < 0)	// Connection error
			break;

		// Receive data
		srv->msq_lBuffer = server::BUFFER_SIZE;
		srv->msq_lBuffer = mq_receive(
			srv->msq,
			srv->msq_buffer,
			srv->msq_lBuffer,
			0);

		// Check error
		if (srv->msq_lBuffer < 0)
			break;

		// Check if we received any data
		if (srv->msq_lBuffer == 0)
			continue;

		// Process data
		header = srv->msq_buffer[0];
		Server::getDataFromBuffer(
			&srv->msq_buffer[1], srv->msq_lBuffer - 1,
			name, semName, beg, end);

		// Display data for debug
#ifdef DAISYSERVER_DEBUG
		std::cout << "MSQ:\n";
		std::cout << "  header : " << header << "\n";
		std::cout << "  name   : " << name << "\n";
		std::cout << "  semName: " << semName << "\n";
		std::cout << "  beg    : " << beg << "\n";
		if (end == server::END)
			std::cout << "  end    : END" << std::endl;
		else
			std::cout << "  end    : " << end << std::endl;
#endif

		// If it's a put message, then broadcast through UDP
		if (header == 'p')
		{
			memcpy(srv->udp_buffer, &srv->tftpPort, sizeof(unsigned short));
			memcpy(
				&srv->udp_buffer[sizeof(unsigned short)],
				&srv->msq_buffer[1],
				srv->msq_lBuffer - 1);
			srv->udp_lBuffer = sizeof(unsigned short) + srv->msq_lBuffer - 1;

			if (sendto(
					srv->udp_s,
					srv->udp_buffer,
					srv->udp_lBuffer,
					0,
					(sockaddr*)&srv->udp_baddr,
					sizeof(srv->udp_baddr)) != srv->udp_lBuffer)
			{
				break;
			}
		}
		// If it's a get message, then manage list
		else if (header == 'g')
		{
			pthread_mutex_lock(&srv->reqMutex);
			reqIt = srv->requests.find(name);

			if (reqIt != srv->requests.end())
			{
				// There is a request with the same name -> Find overlaps
				segIt = reqIt->second.segments.begin();
				while (segIt != reqIt->second.segments.end())
				{
					if (!((segIt->beg < beg && segIt->end < beg) ||
						  (segIt->beg > end && segIt->end > end)))
					{
						// Found overlap -> Request TFTP transfer
						ret = client.readAndWait(
							reqIt->first.c_str(),
							reqIt->first.c_str(),
							segIt->remoteAddr,
							segIt->remotePort);

						if (ret != err::NOERR)
						{
#ifdef DAISYSERVER_DEBUG
							std::cout << "Error getting data [";
							std::cout << ret << "]" << std::endl;
#endif
						}

						// Delete request
						segIt = reqIt->second.segments.erase(segIt);

						if (segIt == reqIt->second.segments.end())
							break;
					}
					else
					{
						segIt++;
					}
				}
			}

			if (!semName.empty())
			{
				char semNameStr[10];

				semNameStr[0] = 'd';
				semNameStr[1] = 'a';
				semNameStr[2] = 'i';
				semNameStr[3] = 's';
				semNameStr[4] = 'y';
				semNameStr[5] = semName[0];
				semNameStr[6] = semName[1];
				semNameStr[7] = semName[2];
				semNameStr[8] = semName[3];
				semNameStr[9] = '\0';

				sem_t *sem = sem_open(semNameStr, 0);

				if (sem != SEM_FAILED)
				{
					sem_post(sem);
					sem_close(sem);
				}
			}

			// Print requests for debugging
#ifdef DAISYSERVER_DEBUG
			std::cout << "Updated requests:\n";
			for (reqIt = srv->requests.begin();
				 reqIt != srv->requests.end();
				 reqIt++)
			{
				std::cout << "  " << reqIt->first << ":\n";

				for (segIt = reqIt->second.segments.begin();
					 segIt != reqIt->second.segments.end();
					 segIt++)
				{
					std::cout << "    " << segIt->beg << " - ";
					if (segIt->end == server::END)
						std::cout << "END: ";
					else
						std::cout << segIt->end << ": ";
					std::cout << inet_ntoa(segIt->remoteAddr.sin_addr) << ":";
					std::cout << segIt->remotePort << "\n";
				}
			}
			std::cout << std::endl;
#endif

			pthread_mutex_unlock(&srv->reqMutex);
		}
		// Else, unknown header
		else
		{
			break;
		}
	}

	srv->running = false;
	return NULL;
}

// Extract name, begin and end from UDP or message queue
void Server::getDataFromBuffer(
	char *buffer,
	int lBuffer,
	std::string &name,
	std::string &semName,
	uint32_t &beg,
	uint32_t &end)
{
	name = "";
	int i = 0;

	for (i = 0; i < lBuffer; i++)
	{
		if (buffer[i] == '\0')
			break;

		name += buffer[i];
	}

	i++;

	if (i == lBuffer - (int)sizeof(uint32_t) * 2)
	{
		memcpy(
			&beg,
			&buffer[lBuffer - (int)sizeof(uint32_t) * 2],
			sizeof(uint32_t));
		memcpy(
			&end,
			&buffer[lBuffer - (int)sizeof(uint32_t)],
			sizeof(uint32_t));
	}
	else
	{
		beg = 0;
		end = server::END;
	}

	// Check if the name contains the semaphore name
	std::size_t semPos = name.find('/');
	if (semPos != std::string::npos)
	{
		semName = name.substr(0, semPos);
		name = name.substr(semPos + 1);
	}
	else
	{
		semName.clear();
	}
}

// Set local address and broadcast address
bool Server::setLocalNBroadcastAddr(const char *dev)
{
	struct ifreq ifc;
    int res;
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if (sockfd < 0)
    	return false;

    strcpy(ifc.ifr_name, dev);
    res = ioctl(sockfd, SIOCGIFADDR, &ifc);
    close(sockfd);

    if (res < 0)
    	return false;

    this->ipv4Local = ((struct sockaddr_in*)&ifc.ifr_addr)->sin_addr;
    this->ipv4Broadcast.s_addr =
    	(this->ipv4Local.s_addr & 0x00FFFFFF) + 0xFF000000;

#ifdef DAISYSERVER_DEBUG
    char ip[16];
    strcpy(ip, inet_ntoa(this->ipv4Local));
    std::cout << "Local address: " << ip << std::endl;
    strcpy(ip, inet_ntoa(this->ipv4Broadcast));
    std::cout << "Broadcast address: " << ip << std::endl;
#endif

    return true;
}

// End daisy namespace
}
