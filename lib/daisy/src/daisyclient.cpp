//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyclient.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	14-Mar-2017
 * @brief	Daisy client
 */

/*
 * INCLUDES
 */

#include "daisyclient.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{


/*
 * CLASS
 */

// Constructor
Client::Client()
{
	this->msq = 0;
	this->msq_lBuffer = 0;
	pthread_mutex_init(&this->sems_mutex, NULL);
	this->rng = gsl_rng_alloc(gsl_rng_default);
	gsl_rng_set(this->rng, time(NULL));
	this->timeout.tv_sec = 1;
	this->timeout.tv_nsec = 0;
}

// Desctructor
Client::~Client()
{
	this->finish();

	// Destroy RNG
	gsl_rng_free(this->rng);
	this->rng = NULL;
}

// Set parameters
bool Client::init(const std::string &msqname)
{
	this->finish();

	// Open message queue for communication with server
	struct mq_attr attr;
	attr.mq_msgsize = server::MSQ_MSGSIZE;
	attr.mq_maxmsg = server::MSQ_MAXMSG;
	this->msq = mq_open(
		msqname.c_str(),
		O_CREAT | O_WRONLY | O_NONBLOCK,
		S_IRUSR | S_IWUSR,
		&attr);

	if (this->msq == (mqd_t)-1)
	{
		this->msq = 0;
		err::error = err::EMSQCRT;
		return false;
	}

	// Return OK
	return true;
}

// Finish client
void Client::finish()
{
	// Close message queues
	if (this->msq != 0)
	{
		mq_close(this->msq);
		this->msq = 0;
	}

	// Close sempahores and destroy them
	std::list<ClientSem>::iterator semIt;

	pthread_mutex_lock(&this->sems_mutex);
	if (!this->sems.empty())
	{
		for (semIt = this->sems.begin(); semIt != this->sems.end(); semIt++)
		{
			sem_close(semIt->sem);
			sem_unlink(semIt->name);
		}

		this->sems.clear();
	}
	pthread_mutex_unlock(&this->sems_mutex);
}

// Send a put request to a remote server
bool Client::put(
	const std::string &name,
	uint32_t beg,
	uint32_t end)
{
	return this->req('p', name, beg, end);
}

// Send a get request to the local server asynchronously
bool Client::getAsync(
	const std::string &name,
	uint32_t beg,
	uint32_t end)
{
	return this->req('g', name, beg, end);
}

// Send a get request to the local server and prepare for wait for answer
ClientSem *Client::get(
	const std::string &name,
	uint32_t beg,
	uint32_t end)
{
	// Get semaphore
	ClientSem *sem = this->getSemaphore();
	if (sem == NULL)
		return NULL;

	// Generate name with semaphore
	std::string nameWsem = &sem->name[5];
	nameWsem.append("/");
	nameWsem.append(name);

	if (sem_wait(sem->sem) == -1)
	{
		daisy::err::error = daisy::err::ESEMLOCK;
		return NULL;
	}

	if (!this->req('g', nameWsem, beg, end))
	{
		sem_post(sem->sem);
		pthread_mutex_lock(&this->sems_mutex);
		sem->available = true;
		pthread_mutex_unlock(&this->sems_mutex);
		return NULL;
	}

	return sem;
}

// Send a get request to the local server and wait for answer
bool Client::getSync(
	const std::string &name,
	uint32_t beg,
	uint32_t end)
{
	ClientSem *sem = this->get(name, beg, end);

	if (sem == NULL)
	{
		daisy::err::error = daisy::err::ESEMINIT;
		return false;
	}

	return this->sync(sem);
}

// Waits for the sync signal
bool Client::sync(ClientSem *sem)
{
	if (this->timeout.tv_sec == 0 && this->timeout.tv_nsec == 0)
	{
		if (sem_wait(sem->sem) == -1)
		{
			daisy::err::error = daisy::err::ESEMLOCK;
			return false;
		}
	}
	else
	{
		struct timespec timeabs;

		if (clock_gettime(CLOCK_REALTIME, &timeabs) == -1)
		{
			daisy::err::error = daisy::err::ETIMEOUT;
			return false;
		}

		timeabs.tv_nsec += this->timeout.tv_nsec;
		timeabs.tv_sec += this->timeout.tv_sec;

		if (sem_timedwait(sem->sem, &timeabs) == -1)
		{
			if (errno == ETIMEDOUT)
				daisy::err::error = daisy::err::ETIMEOUT;
			else
				daisy::err::error = daisy::err::ESEMLOCK;

			return false;
		}
	}

	if (sem_post(sem->sem) == -1)
	{
		daisy::err::error = daisy::err::ESEMUNLOCK;
		return false;
	}

	pthread_mutex_lock(&this->sems_mutex);
	sem->available = true;
	pthread_mutex_unlock(&this->sems_mutex);
	return true;
}

// Set timeout for the synchronized operations
void Client::setTimeout(struct timespec &timeout)
{
	this->timeout = timeout;
}

// Send a request to the local server
bool Client::req(
	char header,
	const std::string &name,
	uint32_t beg,
	uint32_t end)
{
	if (this->msq == 0)
	{
		err::error = err::ENOMSQ;
		return false;
	}

	if (beg >= end)
	{
		err::error = err::EINVSEG;
		return false;
	}

	// Generate message
	this->msq_buffer[0] = header;
	memcpy(&this->msq_buffer[1], name.c_str(), name.size() + 1);
	this->msq_lBuffer = name.size() + 2;

	if (beg != 0 || end != server::END)
	{
		memcpy(
			&this->msq_buffer[this->msq_lBuffer],
			&beg, sizeof(uint32_t));
		this->msq_lBuffer += sizeof(uint32_t);
		memcpy(
			&this->msq_buffer[this->msq_lBuffer],
			&end, sizeof(uint32_t));
		this->msq_lBuffer += sizeof(uint32_t);
	}

	// Send message
	if (mq_send(this->msq, this->msq_buffer, this->msq_lBuffer, 0) == -1)
	{
		err::error = err::EMSQSEND;
		return false;
	}

	// Return OK
	return true;
}

// Get a new names semaphore, and create one if necessary
ClientSem *Client::getSemaphore()
{
	std::list<ClientSem>::iterator semIt;

	pthread_mutex_lock(&this->sems_mutex);

	if (!this->sems.empty())
	{
		// Find available semaphore
		for (semIt = this->sems.begin(); semIt != this->sems.end(); semIt++)
		{
			if (semIt->available)
			{
				semIt->available = false;
				pthread_mutex_unlock(&this->sems_mutex);
				return &(*semIt);
			}
		}
	}

	// Not available semaphore, create a new one
	ClientSem sem;
	if (this->rng != NULL)
	{
		unsigned int id = gsl_rng_uniform_int(this->rng, 1 << 24);
		unsigned char id1 = id & 0x3F;
		unsigned char id2 = (id >> 6) & 0x3F;
		unsigned char id3 = (id >> 12) & 0x3F;
		unsigned char id4 = (id >> 18) & 0x3F;

		sem.name[0] = 'd';
		sem.name[1] = 'a';
		sem.name[2] = 'i';
		sem.name[3] = 's';
		sem.name[4] = 'y';
		sem.name[5] = daisy::server::SEM_NAME_CODED[id1];
		sem.name[6] = daisy::server::SEM_NAME_CODED[id2];
		sem.name[7] = daisy::server::SEM_NAME_CODED[id3];
		sem.name[8] = daisy::server::SEM_NAME_CODED[id4];
		sem.name[9] = '\0';

		sem.sem = sem_open(
			sem.name,
			O_CREAT | O_EXCL,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP,
			1);

		if (sem.sem == SEM_FAILED)
		{
			pthread_mutex_unlock(&this->sems_mutex);
			return NULL;
		}

		sem.available = false;
		this->sems.push_back(sem);
		pthread_mutex_unlock(&this->sems_mutex);
		return &this->sems.back();
	}
	else
	{
		std::cout << "RNG not ready" << std::endl;
	}

	pthread_mutex_unlock(&this->sems_mutex);
	return NULL;
}

// End daisy namespace
}
