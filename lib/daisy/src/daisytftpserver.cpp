//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisytftpserver.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	05-Dec-2016
 * @brief	Daisy TFTP server
 */

/*
 * INCLUDES
 */

#include "daisytftpserver.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start tftp namespace
namespace tftp
{


/*
 * CLASS: Server
 */

Server::Server()
{
	this->state = STATE_CLOSED;
	this->lBuffer = 0;
	this->port = tftp::PORT_DEFAULT;

	this->timeInit = 0.0;
	this->timeDisk = 0.0;
	this->timeTransmit = 0.0;
	this->timeTotal = 0.0;
	this->timeAux = 0.0;
	this->fileSize = 0;
}

Server::~Server()
{
	this->finish();
}

int Server::listen(int port)
{
	this->state = STATE_LISTENING;
	this->port = port;
	pthread_create(&this->threadListen, NULL, Server::runListen, (void*)this);
	return err::NOERR;
}

int Server::finish()
{
	if (this->state == STATE_LISTENING)
	{
		this->state = STATE_MUSTCLOSE;
		pthread_join(this->threadListen, NULL);
	}
	else
	{
		return err::NOERR;
	}

	return err::NOERR;
}

int Server::getState()
{
	return this->state;
}

void Server::printStats()
{
	double timeOthers =
		this->timeTotal -
		this->timeInit -
		this->timeDisk -
		this->timeTransmit -
		this->timeAux;
	double bandwidth =
		(double)this->fileSize / this->timeTotal * 1000 / 1024 * 8;
	std::cout << "Total processing time: " << this->timeTotal << " ms\n";
	std::cout << "  Time init    : " << this->timeInit << " ms\n";
	std::cout << "  Time disk    : " << this->timeDisk << " ms\n";
	std::cout << "  Time transmit: " << this->timeTransmit << " ms\n";
	std::cout << "  Time aux     : " << this->timeAux << " ms\n";
	std::cout << "  Time others  : " << timeOthers << " ms\n";
	std::cout << "  Filesize     : " << this->fileSize << " bytes\n";
	std::cout << "  Bandwidth    : " << bandwidth << " kbps\n";
}

void *Server::runListen(void *arg)
{
	// Variables
	Server *server = (Server*)arg;
	int udpSocket;
	struct sockaddr_in remoteAddr;
	socklen_t remoteAddrLen = sizeof(remoteAddr);
	struct sockaddr_in localAddr;
	char filename[FILENAME_LENGTH];
	char modeStr[MODE_LENGTH];
	char option[OPTION_LENGTH];
	char optionVal[OPTION_LENGTH];
	char optionStr[OPTIONSTR_LENGTH];
	int optionStrLen = 0;
	int block;
	int t;
	int endTransfer;
	int bytesRead;
	int mode = MODE_OCTET;
	int compression = COMPRESSION_NONE;
	int blockSize = BLOCK_SIZE_DEFAULT;

	struct timespec timeInit;
	struct timespec timeFinish;
	struct timespec time0;
	struct timespec time1;

	// Create the UDP socket
	udpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (udpSocket <= 0)
	{
		err::error = err::ESOCK;
		server->state = STATE_ERROR;
		return NULL;
	}

	// Configuring local address
	localAddr.sin_family = AF_INET;
	localAddr.sin_port = htons(server->port);
	localAddr.sin_addr.s_addr = INADDR_ANY;

	if (bind(udpSocket, (struct sockaddr*)&localAddr, sizeof(localAddr)) == -1)
	{
		err::error = err::EBIND;
		server->state = STATE_ERROR;
		close(udpSocket);
		return NULL;
	}

	// Main loop
	while (server->state == STATE_LISTENING)
	{
		t = server->waitForAnswer(udpSocket, &remoteAddr, &remoteAddrLen);

		if (t < 0)
		{
			err::error = err::ERECV;
			server->state = STATE_ERROR;
			close(udpSocket);
			return NULL;
		}
		else if (t == 0)
		{
			continue;
		}

		if (server->lBuffer < 4)
		{
			server->sendError(
				udpSocket,
				remoteAddr,
				tftp::ENODEF,
				"Wrong packet");

			// Wait for the ACK
			t = server->waitForAnswer(udpSocket, &remoteAddr, &remoteAddrLen);
		}
		else if (server->buffer[0] == 0 &&			// RRQ
			server->buffer[1] == OP_RRQ)
		{
			clock_gettime(CLOCK_REALTIME, &timeInit);

			server->timeInit = 0.0;
			server->timeDisk = 0.0;
			server->timeTransmit = 0.0;
			server->timeTotal = 0.0;
			server->timeAux = 0.0;
			server->fileSize = 0;

			if (server->buffer[server->lBuffer - 1] != 0)
			{
				server->sendError(
					udpSocket,
					remoteAddr,
					tftp::ENODEF,
					"Wrong packet format");

				// Wait for the ACK
				t = server->waitForAnswer(
					udpSocket, &remoteAddr, &remoteAddrLen);
			}
			else
			{
				// Get filename, mode and options
				mode = MODE_OCTET;
				compression = COMPRESSION_NONE;

				int pos = 2;
				filename[0] = '\0';
				modeStr[0] = '\0';
				option[0] = '\0';
				optionVal[0] = '\0';
				bool errorSettings = false;
				bool withOptions = false;
				optionStrLen = 0;

				while (1)
				{
					if (filename[0] == '\0')
					{
						server->getStringFromBuffer(filename, pos);
					}
					else if (modeStr[0] == '\0')
					{
						server->getStringFromBuffer(modeStr, pos);
						server->tolower(modeStr);

						// Check mode
						if (strcmp(modeStr, "octet") == 0)
						{
							mode = MODE_OCTET;
						}
						else if (strcmp(modeStr, "shm") == 0)
						{
							mode = MODE_SHM;
						}
						else
						{
							server->sendError(
								udpSocket,
								remoteAddr,
								tftp::ENODEF,
								"Mode not implemented");

							// Wait for the ACK
							t = server->waitForAnswer(
								udpSocket, &remoteAddr, &remoteAddrLen);

							errorSettings = true;
							break;
						}
					}
					else if (option[0] == '\0')
					{
						if (!server->getStringFromBuffer(option, pos))
							break;
					}
					else if (optionVal[0] == '\0')
					{
						if (!server->getStringFromBuffer(optionVal, pos))
							break;

						if (strcmp(option, "comp") == 0)
						{
							if (strcmp(optionVal, "zlib") == 0)
							{
								//std::cout << "Received compression with zlib option" << std::endl;
								compression = COMPRESSION_Z;
								withOptions = true;
								memcpy(&optionStr[optionStrLen], "comp\0zlib\0", 10);
								optionStrLen += 10;
							}
						}
						else if (strcmp(option, "blksize") == 0)
						{
							blockSize = atoi(optionVal);
							//std::cout << "Received blocksize request ";
							//std::cout << blockSize << std::endl;
							withOptions = true;
							int opLen = sprintf(&optionStr[optionStrLen], "blksize");
							optionStrLen += opLen + 1;
							opLen = sprintf(&optionStr[optionStrLen], "%d", blockSize);
							optionStrLen += opLen + 1;
						}

						option[0] = '\0';
						optionVal[0] = '\0';
					}
				}

				if (errorSettings)
					continue;

				if (withOptions)
				{
					// Send OACK
					server->buffer[0] = 0;
					server->buffer[1] = OP_OACK;
					memcpy(&server->buffer[2], optionStr, optionStrLen);
					server->lBuffer = 2 + optionStrLen;

					clock_gettime(CLOCK_REALTIME, &time0);
					sendto(
						udpSocket,
						server->buffer,
						server->lBuffer,
						0,
						(struct sockaddr*)&remoteAddr,
						sizeof(remoteAddr));
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeTransmit += utils::getms(time0, time1);

					// Wait for the ACK
					clock_gettime(CLOCK_REALTIME, &time0);
					t = server->waitForAnswer(udpSocket, &remoteAddr, &remoteAddrLen);
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeTransmit += utils::getms(time0, time1);

					if (t < 0)
					{
						err::error = err::ERECV;
						server->state = STATE_ERROR;
						close(udpSocket);
						return NULL;
					}
					else if (t > 0)
					{
						// OK
					}
					else
					{
						// Timeout -> Exit
						break;
					}
				}

				// Read file
				clock_gettime(CLOCK_REALTIME, &time0);

				daisy::Reader *reader = daisy::tftp::readerFactory(
					mode, compression);

				if (reader == NULL)
				{
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeDisk += utils::getms(time0, time1);

					server->sendError(
						udpSocket,
						remoteAddr,
						tftp::ENODEF,
						"Wrong mode or compression");

					// Wait for the ACK
					t = server->waitForAnswer(
						udpSocket, &remoteAddr, &remoteAddrLen);
				}
				else if (!reader->open(filename))
				{
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeDisk += utils::getms(time0, time1);

					server->sendError(
						udpSocket,
						remoteAddr,
						tftp::ENOTFOUND,
						"Error opening file");

					// Wait for the ACK
					t = server->waitForAnswer(
						udpSocket, &remoteAddr, &remoteAddrLen);

					delete reader;
				}
				else
				{
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeDisk += utils::getms(time0, time1);

					block = 1;

					while (1)
					{
						server->buffer[0] = 0;
						server->buffer[1] = OP_DATA;
						server->buffer[2] = (block >> 8) & 0xFF;
						server->buffer[3] = block & 0xFF;

						clock_gettime(CLOCK_REALTIME, &time0);
						bytesRead = reader->read((char*)server->buffer + 4, blockSize);
						clock_gettime(CLOCK_REALTIME, &time1);
						server->timeDisk += utils::getms(time0, time1);
						server->lBuffer = 4 + bytesRead;
						server->fileSize += bytesRead;

						// Send DATA
						clock_gettime(CLOCK_REALTIME, &time0);
						sendto(
							udpSocket,
							server->buffer,
							server->lBuffer,
							0,
							(struct sockaddr*)&remoteAddr,
							sizeof(remoteAddr));
						clock_gettime(CLOCK_REALTIME, &time1);
						server->timeTransmit += utils::getms(time0, time1);

						// Wait for the ACK
						clock_gettime(CLOCK_REALTIME, &time0);
						t = server->waitForAnswer(udpSocket, &remoteAddr, &remoteAddrLen);
						clock_gettime(CLOCK_REALTIME, &time1);
						server->timeTransmit += utils::getms(time0, time1);

						if (t < 0)
						{
							err::error = err::ERECV;
							server->state = STATE_ERROR;
							close(udpSocket);
							delete reader;
							return NULL;
						}
						else if (t > 0)
						{
							block++;
						}
						else
						{
							// Timeout -> Exit
							break;
						}

						if (bytesRead == 0 || bytesRead < blockSize)
							break;
					}

					clock_gettime(CLOCK_REALTIME, &time0);
					reader->close();
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeDisk += utils::getms(time0, time1);

					delete reader;
				}
			}

			clock_gettime(CLOCK_REALTIME, &timeFinish);
			server->timeTotal = utils::getms(timeInit, timeFinish);
			//server->printStats();
		}
		else if (server->buffer[0] == 0 &&	// WRQ
			server->buffer[1] == OP_WRQ)
		{
			clock_gettime(CLOCK_REALTIME, &timeInit);

			server->timeInit = 0.0;
			server->timeDisk = 0.0;
			server->timeTransmit = 0.0;
			server->timeTotal = 0.0;
			server->timeAux = 0.0;
			server->fileSize = 0;

			if (server->buffer[server->lBuffer - 1] != 0)
			{
				server->sendError(
					udpSocket,
					remoteAddr,
					tftp::ENODEF,
					"Wrong packet format");

				// Wait for the ACK
				t = server->waitForAnswer(
					udpSocket, &remoteAddr, &remoteAddrLen);
			}
			else
			{
				// Get filename, mode and options
				mode = MODE_OCTET;
				compression = COMPRESSION_NONE;

				int pos = 2;
				filename[0] = '\0';
				modeStr[0] = '\0';
				option[0] = '\0';
				optionVal[0] = '\0';
				bool errorSettings = false;
				bool withOptions = false;
				optionStrLen = 0;

				while (1)
				{
					if (filename[0] == '\0')
					{
						server->getStringFromBuffer(filename, pos);
					}
					else if (modeStr[0] == '\0')
					{
						server->getStringFromBuffer(modeStr, pos);
						server->tolower(modeStr);

						// Check mode
						if (strcmp(modeStr, "octet") == 0)
						{
							mode = MODE_OCTET;
						}
						else if (strcmp(modeStr, "shm") == 0)
						{
							mode = MODE_SHM;
						}
						else
						{
							server->sendError(
								udpSocket,
								remoteAddr,
								tftp::ENODEF,
								"Mode not implemented");

							// Wait for the ACK
							t = server->waitForAnswer(
								udpSocket, &remoteAddr, &remoteAddrLen);

							errorSettings = true;
							break;
						}
					}
					else if (option[0] == '\0')
					{
						if (!server->getStringFromBuffer(option, pos))
							break;
					}
					else if (optionVal[0] == '\0')
					{
						if (!server->getStringFromBuffer(optionVal, pos))
							break;

						if (strcmp(option, "comp") == 0)
						{
							if (strcmp(optionVal, "zlib") == 0)
							{
								//std::cout << "Received compression with zlib option" << std::endl;
								compression = COMPRESSION_Z;
								withOptions = true;
								memcpy(&optionStr[optionStrLen], "comp\0zlib\0", 10);
								optionStrLen += 10;
							}
						}
						else if (strcmp(option, "blksize") == 0)
						{
							blockSize = atoi(optionVal);
							//std::cout << "Received blocksize request ";
							//std::cout << blockSize << std::endl;
							withOptions = true;
							int opLen = sprintf(&optionStr[optionStrLen], "blksize");
							optionStrLen += opLen + 1;
							opLen = sprintf(&optionStr[optionStrLen], "%d", blockSize);
							optionStrLen += opLen + 1;
						}

						option[0] = '\0';
						optionVal[0] = '\0';
					}
				}

				if (errorSettings)
					continue;

				endTransfer = 0;

				// Open file to write
				clock_gettime(CLOCK_REALTIME, &time0);
				daisy::Writer *writer = daisy::tftp::writerFactory(
					mode, compression);

				if (writer == NULL)
				{
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeDisk += utils::getms(time0, time1);

					server->sendError(
						udpSocket,
						remoteAddr,
						tftp::ENODEF,
						"Wrong mode or compression");

					// Wait for the ACK
					t = server->waitForAnswer(
						udpSocket, &remoteAddr, &remoteAddrLen);
				}
				else if (!writer->open(filename))
				{
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeDisk += utils::getms(time0, time1);

					server->sendError(
						udpSocket,
						remoteAddr,
						tftp::ENOTFOUND,
						"Error opening file");

					// Wait for the ACK
					t = server->waitForAnswer(
						udpSocket, &remoteAddr, &remoteAddrLen);

					delete writer;
				}
				else
				{
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeDisk += utils::getms(time0, time1);

					if (withOptions)
					{
						// Send OACK
						server->buffer[0] = 0;
						server->buffer[1] = OP_OACK;
						memcpy(&server->buffer[2], optionStr, optionStrLen);
						server->lBuffer = 2 + optionStrLen;

						clock_gettime(CLOCK_REALTIME, &time0);
						sendto(
							udpSocket,
							server->buffer,
							server->lBuffer,
							0,
							(struct sockaddr*)&remoteAddr,
							sizeof(remoteAddr));
						clock_gettime(CLOCK_REALTIME, &time1);
						server->timeTransmit += utils::getms(time0, time1);
					}
					else
					{
						// Send first ACK to start the transfer
						server->buffer[0] = 0;
						server->buffer[1] = OP_ACK;
						server->buffer[2] = 0;
						server->buffer[3] = 0;
						server->lBuffer = 4;

						clock_gettime(CLOCK_REALTIME, &time0);
						if (sendto(
								udpSocket,
								server->buffer,
								server->lBuffer,
								0,
								(struct sockaddr*)&remoteAddr,
								sizeof(remoteAddr)) <= 0)
						{
							err::error = err::ESENDREQ;
							server->state = STATE_ERROR;
							close(udpSocket);
							writer->close();
							delete writer;
							return NULL;
						}
						clock_gettime(CLOCK_REALTIME, &time1);
						server->timeTransmit += utils::getms(time0, time1);
					}

					int lastBlock = 0;
					endTransfer = 0;

					while (!endTransfer)
					{
						// Wait for the packet
						clock_gettime(CLOCK_REALTIME, &time0);
						t = server->waitForAnswer(udpSocket, &remoteAddr, &remoteAddrLen);
						clock_gettime(CLOCK_REALTIME, &time1);
						server->timeTransmit += utils::getms(time0, time1);

						if (t < 0)
						{
							err::error = err::ERECV;
							server->state = STATE_ERROR;
							close(udpSocket);
							writer->close();
							delete writer;
							return NULL;
						}
						else if (t == 0)
						{
							// Timeout -> Exit
							break;
						}
						else if (t == 4)	// Last packet empty
						{
							// Finish transfer
							endTransfer = 1;
						}
						else if (t < blockSize + 4)
						{
							block = (server->buffer[2] << 8) + server->buffer[3];

							if (block > lastBlock)
							{
								clock_gettime(CLOCK_REALTIME, &time0);
								writer->write((char*)server->buffer + 4, server->lBuffer - 4);
								clock_gettime(CLOCK_REALTIME, &time1);
								server->timeDisk += utils::getms(time0, time1);
								lastBlock = block;
							}

							// Finish transfer
							endTransfer = 1;
						}
						else
						{
							block = (server->buffer[2] << 8) + server->buffer[3];

							if (block > lastBlock)
							{
								clock_gettime(CLOCK_REALTIME, &time0);
								writer->write((char*)server->buffer + 4, server->lBuffer - 4);
								clock_gettime(CLOCK_REALTIME, &time1);
								server->timeDisk += utils::getms(time0, time1);
								lastBlock = block;
							}
						}

						server->fileSize += t - 4;

						// Send ACK
						server->buffer[0] = 0;
						server->buffer[1] = OP_ACK;
						//server->buffer[2] = server->buffer[2];
						//server->buffer[3] = server->buffer[3];
						server->lBuffer = 4;

						clock_gettime(CLOCK_REALTIME, &time0);
						if (sendto(
								udpSocket,
								server->buffer,
								server->lBuffer,
								0,
								(struct sockaddr*)&remoteAddr,
								sizeof(remoteAddr)) <= 0)
						{
							err::error = err::EACK;
							server->state = STATE_ERROR;
							close(udpSocket);
							writer->close();
							delete writer;
							return NULL;
						}
						clock_gettime(CLOCK_REALTIME, &time1);
						server->timeTransmit += utils::getms(time0, time1);
					}

					clock_gettime(CLOCK_REALTIME, &time0);
					writer->close();
					clock_gettime(CLOCK_REALTIME, &time1);
					server->timeDisk += utils::getms(time0, time1);

					delete writer;
				}
			}

			clock_gettime(CLOCK_REALTIME, &timeFinish);
			server->timeTotal = utils::getms(timeInit, timeFinish);
			//server->printStats();
		}
		else if (server->buffer[0] == 0 &&	// ERROR
			server->buffer[1] == OP_ERROR)
		{
			// Manage error code
		}
		else
		{
			server->sendError(
				udpSocket,
				remoteAddr,
				tftp::EILLOP,
				"Unexpected/unknown opcode");

			// Wait for the ACK
			t = server->waitForAnswer(udpSocket, &remoteAddr, &remoteAddrLen);
		}
	}

	server->state = STATE_CLOSED;
	close(udpSocket);
	return NULL;
}

int Server::sendError(
	int udpSocket,
	struct sockaddr_in remoteAddr,
	int errorCode,
	const char *message)
{
	this->buffer[0] = 0;
	this->buffer[1] = OP_ERROR;

	this->buffer[2] = (errorCode >> 8) & 0xFF;
	this->buffer[3] = errorCode & 0xFF;

	strcpy((char*)&this->buffer[4], message);

	this->lBuffer = (int)strlen(message) + 5;
	this->buffer[this->lBuffer - 1] = 0;

	return sendto(
		udpSocket,
		this->buffer,
		this->lBuffer,
		0,
		(struct sockaddr*)&remoteAddr,
		sizeof(remoteAddr));
}

int Server::waitForAnswer(
	int udpSocket,
	struct sockaddr_in *remoteAddr,
	socklen_t *remoteAddrLen)
{
	struct timeval timeoutset;
	fd_set rdset;
	int t;

	timeoutset.tv_sec = TIMEOUT_SEC;
	timeoutset.tv_usec = TIMEOUT_USEC;
	FD_ZERO(&rdset);
	FD_SET(udpSocket, &rdset);
	t = select(udpSocket + 1, &rdset, NULL, NULL, &timeoutset);

	if (t > 0)
	{
		this->lBuffer = recvfrom(
			udpSocket,
			this->buffer,
			BUFFER_SIZE,
			0,
			(struct sockaddr*)remoteAddr,
			remoteAddrLen);
		return this->lBuffer;
	}
	else
	{
		return t;
	}
}

void Server::tolower(char *buffer)
{
	int len = (int)strlen(buffer);
	int offset = 'A' - 'a';

	for (int i = 0; i < len; i++)
	{
		if (buffer[i] >= 'A' && buffer[i] <= 'Z')
			buffer[i] = buffer[i] - offset;
	}
}

bool Server::getStringFromBuffer(char *str, int &pos)
{
	if (pos >= this->lBuffer)
		return false;

	int offset = 0;

	while (pos < this->lBuffer)
	{
		str[offset] = this->buffer[pos];

		if (this->buffer[pos] == 0)
		{
			pos++;
			return true;
		}

		offset++;
		pos++;
	}

	return false;
}

// End tftp namespace
}

// End daisy namespace
}
