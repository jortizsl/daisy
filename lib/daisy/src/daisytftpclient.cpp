//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisytftpclient.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	05-Dec-2016
 * @brief	Daisy TFTP client
 */

/*
 * INCLUDES
 */

#include "daisytftpclient.h"

/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start tftp namespace
namespace tftp
{

/*
 * CLASS: Client
 */

Client::Client()
{
	this->state = STATE_IDLE;
	this->port = daisy::tftp::PORT_DEFAULT;
	this->lBuffer = 0;
	this->udp_s = 0;

	this->mode = MODE_OCTET;
	this->compression = COMPRESSION_NONE;

	this->timeInit = 0.0;
	this->timeDisk = 0.0;
	this->timeTransmit = 0.0;
	this->timeTotal = 0.0;
	this->timeAux = 0.0;
	this->fileSize = 0;
}

Client::~Client()
{
	// Stop the pending connections
	if (this->state == STATE_READING ||
		this->state == STATE_WRITING)
	{
		this->state = STATE_MUST_END;
		pthread_join(this->threadRW, NULL);
	}

	return;
}

void Client::setMode(int mode)
{
	this->mode = mode;
}

void Client::setCompression(int compression)
{
	this->compression = compression;
}

void Client::setBlockSize(int blockSize)
{
	this->blockSize = blockSize;
}

void Client::read(
	const char *remoteFile,
	const char *localFile,
	const char *address,
	int port)
{
	this->resolveAddress(address, port);
	this->read(remoteFile, localFile, this->remoteAddr, port);
}

void Client::write(
	const char *remoteFile,
	const char *localFile,
	const char *address,
	int port)
{
	this->resolveAddress(address, port);
	this->write(remoteFile, localFile, this->remoteAddr, port);
}

void Client::read(
	const char *remoteFile,
	const char *localFile,
	struct sockaddr_in &address,
	int port)
{
	this->state = STATE_READING;

	strcpy(this->remoteFile, remoteFile);
	strcpy(this->localFile, localFile);
	this->remoteAddr = address;
	this->port = port;

	pthread_create(&this->threadRW, NULL, this->runRead, (void*)this);
}

void Client::write(
	const char *remoteFile,
	const char *localFile,
	struct sockaddr_in &address,
	int port)
{
	this->state = STATE_WRITING;

	strcpy(this->remoteFile, remoteFile);
	strcpy(this->localFile, localFile);
	this->remoteAddr = address;
	this->port = port;

	pthread_create(&this->threadRW, NULL, this->runWrite, (void*)this);
}

int Client::getState()
{
	return this->state;
}

void Client::printStats()
{
	double timeOthers =
		this->timeTotal -
		this->timeInit -
		this->timeDisk -
		this->timeTransmit -
		this->timeAux;
	double bandwidth =
		(double)this->fileSize / this->timeTotal * 1000 / 1024 * 8;
	std::cout << "Total processing time: " << this->timeTotal << " ms\n";
	std::cout << "  Time init    : " << this->timeInit << " ms\n";
	std::cout << "  Time disk    : " << this->timeDisk << " ms\n";
	std::cout << "  Time transmit: " << this->timeTransmit << " ms\n";
	std::cout << "  Time aux     : " << this->timeAux << " ms\n";
	std::cout << "  Time others  : " << timeOthers << " ms\n";
	std::cout << "  Filesize     : " << this->fileSize << " bytes\n";
	std::cout << "  Bandwidth    : " << bandwidth << " kbps\n";
}

void *Client::runRead(void *arg)
{
	struct timespec timeInit;
	clock_gettime(CLOCK_REALTIME, &timeInit);
	struct timespec timeFinish;
	struct timespec time0 = timeInit;
	struct timespec time1;

	// Variables
	Client *client = (Client*)arg;
	int udpSocket;
	//struct sockaddr_in remoteAddr;
	//socklen_t remoteAddrLen;
	int block;
	int lastBlock = 0;
	char option[OPTION_LENGTH];
	char optionVal[OPTION_LENGTH];
	int compressionCurr;
	int blocksizeCurr;

	client->timeInit = 0.0;
	client->timeDisk = 0.0;
	client->timeTransmit = 0.0;
	client->timeTotal = 0.0;
	client->timeAux = 0.0;
	client->fileSize = 0;

	// Writer
	daisy::Writer *writer = NULL;

	// Create the UDP socket
	udpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (udpSocket <= 0)
	{
		err::error = err::ESOCK;
		client->state = STATE_ERROR;
		return NULL;
	}

	// Configure remote port
	client->remoteAddr.sin_port = htons(client->port);

	// Sending read request
	client->buffer[0] = 0;
	client->buffer[1] = OP_RRQ;
	client->lBuffer = 2;

	client->addStringToBuffer(client->remoteFile);

	// Add mode
	if (client->mode == MODE_OCTET)
	{
		client->addStringToBuffer("octet");
	}
	else if (client->mode == MODE_SHM)
	{
		client->addStringToBuffer("shm");
	}
	else
	{
		err::error = err::EMODE;
		client->state = STATE_ERROR;
		close(udpSocket);
		return NULL;
	}

	// Add options
	if (client->compression == COMPRESSION_NONE)
	{
		// Don't add compression options
	}
	else if (client->compression == COMPRESSION_Z)
	{
		client->addStringToBuffer("comp");
		client->addStringToBuffer("zlib");
	}

	if (client->blockSize != BLOCK_SIZE_DEFAULT)
	{
		client->addStringToBuffer("blksize");
		char blockSizeStr[16];
		sprintf(blockSizeStr, "%d", client->blockSize);
		client->addStringToBuffer(blockSizeStr);
	}

	// Reset options (might be confirmed by server)
	compressionCurr = COMPRESSION_NONE;
	blocksizeCurr = BLOCK_SIZE_DEFAULT;

	// Send request
	if (sendto(
			udpSocket,
			client->buffer,
			client->lBuffer,
			0,
			(struct sockaddr*)&client->remoteAddr,
			sizeof(client->remoteAddr)) <= 0)
	{
		err::error = err::ESENDREQ;
		client->state = STATE_ERROR;
		close(udpSocket);
		return NULL;
	}
	clock_gettime(CLOCK_REALTIME, &time1);
	client->timeTransmit += utils::getms(timeInit, time1);

	while (1)
	{
		// Wait for an answer
		clock_gettime(CLOCK_REALTIME, &time0);
		int t = client->waitForAnswer(
			udpSocket, &client->remoteAddr, &client->remoteAddrLen);
		clock_gettime(CLOCK_REALTIME, &time1);
		client->timeTransmit += utils::getms(time0, time1);

		if (t == -1)
		{
			err::error = err::EACK;
			client->state = STATE_ERROR;
			close(udpSocket);
			if (writer != NULL)
			{
				writer->abort();
				delete writer;
			}
			return NULL;
		}
		else if (t == 0)
		{
			err::error = err::ETIMEOUT;
			client->state = STATE_ERROR;
			close(udpSocket);
			if (writer != NULL)
			{
				writer->abort();
				delete writer;
			}
			return NULL;
		}

		// Check buffer length
		if (client->lBuffer < 4)
		{
			err::error = err::EBLK;
			client->state = STATE_ERROR;
			close(udpSocket);
			if (writer != NULL)
			{
				writer->abort();
				delete writer;
			}
			return NULL;
		}

			/* Check opcode */

		// ERROR
		if (client->buffer[0] == 0 &&
			client->buffer[1] == OP_ERROR)
		{
			// Answer with a ACK
			client->buffer[0] = 0;
			client->buffer[1] = OP_ACK;
			client->buffer[2] = 0;
			client->buffer[3] = 1;
			client->lBuffer = 4;

			clock_gettime(CLOCK_REALTIME, &time0);
			sendto(
				udpSocket,
				client->buffer,
				client->lBuffer,
				0,
				(struct sockaddr*)&client->remoteAddr,
				sizeof(client->remoteAddr));
			clock_gettime(CLOCK_REALTIME, &time1);
			client->timeTransmit += utils::getms(time0, time1);

			// Error
			int error = (client->buffer[2] << 8) + client->buffer[3];
			err::error = -(30 + error);	// Convert from standard TFTP error
										// code to Daisy error code
			client->state = STATE_ERROR;
			close(udpSocket);
			if (writer != NULL)
			{
				writer->abort();
				delete writer;
			}
			return NULL;
		}
		// DATA
		else if (client->buffer[0] == 0 &&
			client->buffer[1] == OP_DATA)
		{
			if (writer == NULL)
			{
				writer =
					daisy::tftp::writerFactory(client->mode, compressionCurr);

				if (writer == NULL)
				{
					err::error = err::EMODE;
					client->state = STATE_ERROR;
					return NULL;
				}

				if (!writer->open(client->localFile))
				{
					err::error = err::EOPEN;
					client->state = STATE_ERROR;
					delete writer;
					return NULL;
				}
			}

			if (client->lBuffer == 4)	// Last block without data
			{
				client->buffer[0] = 0;
				client->buffer[1] = OP_ACK;
				//client->buffer[2] = client->buffer[2];
				//client->buffer[3] = client->buffer[3];
				client->lBuffer = 4;

				clock_gettime(CLOCK_REALTIME, &time0);
				if (sendto(
						udpSocket,
						client->buffer,
						client->lBuffer,
						0,
						(struct sockaddr*)&client->remoteAddr,
						sizeof(client->remoteAddr)) <= 0)
				{
					err::error = err::EACK;
					client->state = STATE_ERROR;
					close(udpSocket);
					if (writer != NULL)
					{
						writer->abort();
						delete writer;
					}
					return NULL;
				}
				clock_gettime(CLOCK_REALTIME, &time1);
				client->timeTransmit += utils::getms(time0, time1);

				break;
			}
			if (client->lBuffer < blocksizeCurr + 4)	// Last block with data
			{
				// Proccess data
				block = (client->buffer[2] << 8) + client->buffer[3];

				clock_gettime(CLOCK_REALTIME, &time0);
				if (block > lastBlock)
				{
					writer->write(
						(char*)client->buffer + 4, client->lBuffer - 4);
					lastBlock = block;
				}
				clock_gettime(CLOCK_REALTIME, &time1);
				client->timeDisk += utils::getms(time0, time1);

				client->fileSize += client->lBuffer - 4;

				// Send ACK
				client->buffer[0] = 0;
				client->buffer[1] = OP_ACK;
				//client->buffer[2] = client->buffer[2];
				//client->buffer[3] = client->buffer[3];
				client->lBuffer = 4;

				clock_gettime(CLOCK_REALTIME, &time0);
				if (sendto(
						udpSocket,
						client->buffer,
						client->lBuffer,
						0,
						(struct sockaddr*)&client->remoteAddr,
						sizeof(client->remoteAddr)) <= 0)
				{
					err::error = err::EACK;
					client->state = STATE_ERROR;
					close(udpSocket);
					if (writer != NULL)
					{
						writer->abort();
						delete writer;
					}
					return NULL;
				}
				clock_gettime(CLOCK_REALTIME, &time1);
				client->timeTransmit += utils::getms(time0, time1);

				break;
			}
			else	// Normal block
			{
				// Proccess data
				block = (client->buffer[2] << 8) + client->buffer[3];

				clock_gettime(CLOCK_REALTIME, &time0);
				if (block > lastBlock)
				{
					writer->write(
						(char*)client->buffer + 4, client->lBuffer - 4);
					lastBlock = block;
				}
				clock_gettime(CLOCK_REALTIME, &time1);
				client->timeDisk += utils::getms(time0, time1);

				client->fileSize += client->lBuffer - 4;

				// Send ACK
				client->buffer[0] = 0;
				client->buffer[1] = OP_ACK;
				//client->buffer[2] = client->buffer[2];
				//client->buffer[3] = client->buffer[3];
				client->lBuffer = 4;

				clock_gettime(CLOCK_REALTIME, &time0);
				if (sendto(
						udpSocket,
						client->buffer,
						client->lBuffer,
						0,
						(struct sockaddr*)&client->remoteAddr,
						sizeof(client->remoteAddr)) <= 0)
				{
					err::error = err::EACK;
					client->state = STATE_ERROR;
					close(udpSocket);
					if (writer != NULL)
					{
						writer->abort();
						delete writer;
					}
					return NULL;
				}
				clock_gettime(CLOCK_REALTIME, &time1);
				client->timeTransmit += utils::getms(time0, time1);
			}
		}
		// OACK
		else if (client->buffer[0] == 0 ||
			client->buffer[1] == OP_OACK)
		{
			// Check options
			option[0] = '\0';
			optionVal[0] = '\0';
			int pos = 2;

			while (1)
			{
				if (option[0] == '\0')
				{
					if (!client->getStringFromBuffer(option, pos))
						break;
				}
				else if (optionVal[0] == '\0')
				{
					if (!client->getStringFromBuffer(optionVal, pos))
						break;

					if (strcmp(option, "comp") == 0)
					{
						if (strcmp(optionVal, "zlib") == 0)
						{
							//std::cout << "Server confirmed compression with zlib" << std::endl;
							compressionCurr = COMPRESSION_Z;
						}
					}
					else if (strcmp(option, "blksize") == 0)
					{
						blocksizeCurr = atoi(optionVal);
						//std::cout << "Server confirmed blocksize ";
						//std::cout << blocksizeCurr << std::endl;
					}

					option[0] = '\0';
					optionVal[0] = '\0';
				}
			}

			writer =
				daisy::tftp::writerFactory(client->mode, compressionCurr);

			if (writer == NULL)
			{
				err::error = err::EMODE;
				client->state = STATE_ERROR;
				return NULL;
			}

			if (!writer->open(client->localFile))
			{
				err::error = err::EOPEN;
				client->state = STATE_ERROR;
				delete writer;
				return NULL;
			}

			// Answer with a ACK
			client->buffer[0] = 0;
			client->buffer[1] = OP_ACK;
			client->buffer[2] = 0;
			client->buffer[3] = 0;
			client->lBuffer = 4;

			clock_gettime(CLOCK_REALTIME, &time0);
			if (sendto(
					udpSocket,
					client->buffer,
					client->lBuffer,
					0,
					(struct sockaddr*)&client->remoteAddr,
					sizeof(client->remoteAddr)) <= 0)
			{
				err::error = err::EACK;
				client->state = STATE_ERROR;
				close(udpSocket);
				if (writer != NULL)
				{
					writer->abort();
					delete writer;
				}
				return NULL;
			}
			clock_gettime(CLOCK_REALTIME, &time1);
			client->timeTransmit += utils::getms(time0, time1);
		}
		// Unexpected opcode
		else
		{
			err::error = err::EOPCODE;
			client->state = STATE_ERROR;
			close(udpSocket);
			if (writer != NULL)
			{
				writer->abort();
				delete writer;
			}
			return NULL;
		}
	}

	client->state = STATE_IDLE;
	close(udpSocket);

	clock_gettime(CLOCK_REALTIME, &time0);
	if (writer != NULL)
	{
		writer->close();
		delete writer;
	}
	clock_gettime(CLOCK_REALTIME, &time1);
	client->timeDisk += utils::getms(time0, time1);

	clock_gettime(CLOCK_REALTIME, &timeFinish);
	client->timeTotal = utils::getms(timeInit, timeFinish);
	return NULL;
}

void *Client::runWrite(void *arg)
{
	struct timespec timeInit;
	clock_gettime(CLOCK_REALTIME, &timeInit);
	struct timespec timeFinish;
	struct timespec time0 = timeInit;
	struct timespec time1;

	// Variables
	Client *client = (Client*)arg;
	int udpSocket;
	//struct sockaddr_in remoteAddr;
	//socklen_t remoteAddrLen;
	int block;
	int t;
	int bytesRead;
	char option[OPTION_LENGTH];
	char optionVal[OPTION_LENGTH];
	int compressionCurr;
	int blocksizeCurr;

	client->timeInit = 0.0;
	client->timeDisk = 0.0;
	client->timeTransmit = 0.0;
	client->timeTotal = 0.0;
	client->timeAux = 0.0;
	client->fileSize = 0;

	// Reader
	daisy::Reader *reader = NULL;

	// Create the UDP socket
	udpSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (udpSocket <= 0)
	{
		err::error = err::ESOCK;
		client->state = STATE_ERROR;
		return NULL;
	}

	// Configure remote port
	client->remoteAddr.sin_port = htons(client->port);

	// Sending write request
	client->buffer[0] = 0;
	client->buffer[1] = OP_WRQ;
	client->lBuffer = 2;

	client->addStringToBuffer(client->remoteFile);

	// Add mode
	if (client->mode == MODE_OCTET)
	{
		client->addStringToBuffer("octet");
	}
	else if (client->mode == MODE_SHM)
	{
		client->addStringToBuffer("shm");
	}
	else
	{
		err::error = err::EMODE;
		client->state = STATE_ERROR;
		close(udpSocket);
		return NULL;
	}

	// Add options
	if (client->compression == COMPRESSION_NONE)
	{
		// Don't add compression options
	}
	else if (client->compression == COMPRESSION_Z)
	{
		client->addStringToBuffer("comp");
		client->addStringToBuffer("zlib");
	}

	if (client->blockSize != BLOCK_SIZE_DEFAULT)
	{
		client->addStringToBuffer("blksize");
		char blockSizeStr[16];
		sprintf(blockSizeStr, "%d", client->blockSize);
		client->addStringToBuffer(blockSizeStr);
	}

	// Reset options (might be confirmed by server)
	compressionCurr = COMPRESSION_NONE;
	blocksizeCurr = BLOCK_SIZE_DEFAULT;

	clock_gettime(CLOCK_REALTIME, &time1);
	client->timeInit = utils::getms(time0, time1);

	// Send request
	clock_gettime(CLOCK_REALTIME, &time0);
	if (sendto(
			udpSocket,
			client->buffer,
			client->lBuffer,
			0,
			(struct sockaddr*)&client->remoteAddr,
			sizeof(client->remoteAddr)) <= 0)
	{
		err::error = err::ESENDREQ;
		client->state = STATE_ERROR;
		close(udpSocket);
		return NULL;
	}
	clock_gettime(CLOCK_REALTIME, &time1);
	client->timeTransmit += utils::getms(time0, time1);

	block = 1;

	while (1)
	{
		// Wait for an answer
		clock_gettime(CLOCK_REALTIME, &time0);
		t = client->waitForAnswer(
			udpSocket, &client->remoteAddr, &client->remoteAddrLen);
		clock_gettime(CLOCK_REALTIME, &time1);
		client->timeTransmit += utils::getms(time0, time1);

		if (t == -1)
		{
			err::error = err::EACK;
			client->state = STATE_ERROR;
			close(udpSocket);
			if (reader != NULL)
			{
				reader->abort();
				delete reader;
			}
			return NULL;
		}
		else if (t == 0)
		{
			err::error = err::ETIMEOUT;
			client->state = STATE_ERROR;
			close(udpSocket);
			if (reader != NULL)
			{
				reader->abort();
				delete reader;
			}
			return NULL;
		}

		if (client->lBuffer < 4)
		{
			err::error = err::EBLK;
			client->state = STATE_ERROR;
			close(udpSocket);
			if (reader != NULL)
			{
				reader->abort();
				delete reader;
			}
			return NULL;
		}

			/* Check opcode */

		// ERROR
		if (client->buffer[0] == 0 &&
			client->buffer[1] == OP_ERROR)
		{
			// Answer with a ACK
			client->buffer[0] = 0;
			client->buffer[1] = OP_ACK;
			client->buffer[2] = 0;
			client->buffer[3] = 1;
			client->lBuffer = 4;

			clock_gettime(CLOCK_REALTIME, &time0);
			sendto(
				udpSocket,
				client->buffer,
				client->lBuffer,
				0,
				(struct sockaddr*)&client->remoteAddr,
				sizeof(client->remoteAddr));
			clock_gettime(CLOCK_REALTIME, &time1);
			client->timeTransmit += utils::getms(time0, time1);

			// Error
			int error = (client->buffer[2] << 8) + client->buffer[3];
			err::error = -(30 + error);
			client->state = STATE_ERROR;
			close(udpSocket);
			if (reader != NULL)
			{
				reader->abort();
				delete reader;
			}
			return NULL;
		}
		// ACK
		else if (client->buffer[0] == 0 &&
			client->buffer[1] == OP_ACK)
		{
			if (reader == NULL)
			{
				reader = daisy::tftp::readerFactory(client->mode, compressionCurr);

				if (reader == NULL)
				{
					err::error = err::EMODE;
					client->state = STATE_ERROR;
					return NULL;
				}

				if (!reader->open(client->localFile))
				{
					err::error = err::EOPEN;
					client->state = STATE_ERROR;
					delete reader;
					return NULL;
				}
			}

			// Read and send data
			client->buffer[0] = 0;
			client->buffer[1] = OP_DATA;
			client->buffer[2] = (block >> 8) & 0xFF;
			client->buffer[3] = block & 0xFF;

			clock_gettime(CLOCK_REALTIME, &time0);
			bytesRead = reader->read((char*)client->buffer + 4, blocksizeCurr);
			client->lBuffer = 4 + bytesRead;
			clock_gettime(CLOCK_REALTIME, &time1);
			client->timeDisk += utils::getms(time0, time1);

			client->fileSize += bytesRead;

			// Send DATA
			clock_gettime(CLOCK_REALTIME, &time0);
			sendto(
				udpSocket,
				client->buffer,
				client->lBuffer,
				0,
				(struct sockaddr*)&client->remoteAddr,
				sizeof(client->remoteAddr));
			clock_gettime(CLOCK_REALTIME, &time1);
			client->timeTransmit += utils::getms(time0, time1);

			block++;

			if (bytesRead == 0 || bytesRead < blocksizeCurr)
				break;
		}
		// OACK
		else if (client->buffer[0] == 0 ||
			client->buffer[1] == OP_OACK)
		{
			// Check options
			option[0] = '\0';
			optionVal[0] = '\0';
			int pos = 2;

			while (1)
			{
				if (option[0] == '\0')
				{
					if (!client->getStringFromBuffer(option, pos))
						break;
				}
				else if (optionVal[0] == '\0')
				{
					if (!client->getStringFromBuffer(optionVal, pos))
						break;

					if (strcmp(option, "comp") == 0)
					{
						if (strcmp(optionVal, "zlib") == 0)
						{
							//std::cout << "Server confirmed compression with zlib" << std::endl;
							compressionCurr = COMPRESSION_Z;
						}
					}
					else if (strcmp(option, "blksize") == 0)
					{
						blocksizeCurr = atoi(optionVal);
						//std::cout << "Server confirmed blocksize ";
						//std::cout << blocksizeCurr << std::endl;
					}

					option[0] = '\0';
					optionVal[0] = '\0';
				}
			}

			reader = daisy::tftp::readerFactory(client->mode, compressionCurr);

			if (reader == NULL)
			{
				err::error = err::EMODE;
				client->state = STATE_ERROR;
				return NULL;
			}

			if (!reader->open(client->localFile))
			{
				err::error = err::EOPEN;
				client->state = STATE_ERROR;
				delete reader;
				return NULL;
			}

			// Read and send data
			client->buffer[0] = 0;
			client->buffer[1] = OP_DATA;
			client->buffer[2] = (block >> 8) & 0xFF;
			client->buffer[3] = block & 0xFF;

			clock_gettime(CLOCK_REALTIME, &time0);
			bytesRead = reader->read((char*)client->buffer + 4, blocksizeCurr);
			client->lBuffer = 4 + bytesRead;
			clock_gettime(CLOCK_REALTIME, &time1);
			client->timeDisk += utils::getms(time0, time1);

			client->fileSize += bytesRead;

			// Send DATA
			clock_gettime(CLOCK_REALTIME, &time0);
			sendto(
				udpSocket,
				client->buffer,
				client->lBuffer,
				0,
				(struct sockaddr*)&client->remoteAddr,
				sizeof(client->remoteAddr));
			clock_gettime(CLOCK_REALTIME, &time1);
			client->timeTransmit += utils::getms(time0, time1);

			block++;

			if (bytesRead == 0 || bytesRead < blocksizeCurr)
				break;
		}
		// Unexpected opcode
		else
		{
			err::error = err::EOPCODE;
			client->state = STATE_ERROR;
			close(udpSocket);
			if (reader != NULL)
			{
				reader->abort();
				delete reader;
			}
			return NULL;
		}
	}

	client->state = STATE_IDLE;
	close(udpSocket);
	clock_gettime(CLOCK_REALTIME, &time0);
	if (reader != NULL)
	{
		reader->close();
		delete reader;
	}
	clock_gettime(CLOCK_REALTIME, &time1);
	client->timeDisk += utils::getms(time0, time1);

	clock_gettime(CLOCK_REALTIME, &timeFinish);
	client->timeTotal = utils::getms(timeInit, timeFinish);
	return NULL;
}

int Client::sendError(
	int udpSocket,
	struct sockaddr_in remoteAddr,
	int errorCode,
	const char *message)
{
	this->buffer[0] = 0;
	this->buffer[1] = OP_ERROR;

	this->buffer[2] = (errorCode >> 8) & 0xFF;
	this->buffer[3] = errorCode & 0xFF;

	strcpy((char*)&this->buffer[4], message);

	this->lBuffer = strlen(message) + 5;
	this->buffer[this->lBuffer - 1] = 0;

	return sendto(
		udpSocket,
		this->buffer,
		this->lBuffer,
		0,
		(struct sockaddr*)&remoteAddr,
		sizeof(remoteAddr));
}

int Client::waitForAnswer(
	int udpSocket,
	struct sockaddr_in *remoteAddr,
	socklen_t *remoteAddrLen)
{
	struct timeval timeoutset;
	fd_set rdset;
	int t;

	timeoutset.tv_sec = TIMEOUT_SEC;
	timeoutset.tv_usec = TIMEOUT_USEC;
	FD_ZERO(&rdset);
	FD_SET(udpSocket, &rdset);
	t = select(udpSocket + 1, &rdset, NULL, NULL, &timeoutset);

	if (t > 0)
	{
		this->lBuffer = recvfrom(
			udpSocket,
			this->buffer,
			BUFFER_SIZE,
			0,
			(struct sockaddr*)remoteAddr,
			remoteAddrLen);
		return this->lBuffer;
	}
	else
	{
		return t;
	}
}

int Client::readAndWait(
	const char *remoteFile,
	const char *localFile,
	const char *address,
	int port)
{
	this->resolveAddress(address, port);
	return this->readAndWait(remoteFile, localFile, this->remoteAddr, port);
}

int Client::writeAndWait(
	const char *remoteFile,
	const char *localFile,
	const char *address,
	int port)
{
	this->resolveAddress(address, port);
	return this->writeAndWait(remoteFile, localFile, this->remoteAddr, port);
}

int Client::readAndWait(
	const char *remoteFile,
	const char *localFile,
	struct sockaddr_in &address,
	int port)
{
	this->read(remoteFile, localFile, address, port);

	pthread_join(this->threadRW, NULL);

	if (this->state == STATE_IDLE)
		return err::NOERR;
	else
		return err::error;
}

int Client::writeAndWait(
	const char *remoteFile,
	const char *localFile,
	struct sockaddr_in &address,
	int port)
{
	this->write(remoteFile, localFile, address, port);

	pthread_join(this->threadRW, NULL);

	if (this->state == STATE_IDLE)
		return err::NOERR;
	else
		return err::error;
}

void Client::addStringToBuffer(const char *str)
{
	int len = strlen(str);
	memcpy(&this->buffer[this->lBuffer], str, len);
	this->buffer[this->lBuffer + len] = 0;
	this->lBuffer += len + 1;
}

bool Client::getStringFromBuffer(char *str, int &pos)
{
	if (pos >= this->lBuffer)
		return false;

	int offset = 0;

	while (pos < this->lBuffer)
	{
		str[offset] = this->buffer[pos];

		if (this->buffer[pos] == 0)
		{
			pos++;
			return true;
		}

		offset++;
		pos++;
	}

	return false;
}

bool Client::resolveAddress(const char *address, int port)
{
	// Configure remote address
	this->remoteAddr.sin_family = AF_INET;
	this->remoteAddr.sin_port = htons(port);
	this->remoteAddr.sin_addr.s_addr = inet_addr(address);

	if (this->remoteAddr.sin_addr.s_addr == INADDR_NONE)
	{
		struct hostent *host = gethostbyname(address);

		if (host == NULL)
		{
			return false;
		}

		memcpy(
			&this->remoteAddr.sin_addr,
			host->h_addr_list[0],
			host->h_length);
	}

	return true;
}

// End tftp namespace
}

// End daisy namespace
}
