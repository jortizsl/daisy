//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyreaderfile.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	12-Dec-2016
 * @brief	Daisy reader form file
 */

/*
 * INCLUDES
 */

#include <time.h>

#include "daisyreaderfile.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: FileReader
 */

FileReader::FileReader()
{
	this->type = TYPE_FILE;
}

FileReader::~FileReader()
{
	return;
}

bool FileReader::open(
	const char *name,
	unsigned long beg,
	unsigned long end)
{
	this->file.open(name, std::ios::binary);

	if (!this->file)
		return false;

	return true;
}

void FileReader::close()
{
	this->file.close();
}

void FileReader::abort()
{
	this->close();
}

int FileReader::read(char *buffer, unsigned long lBuffer)
{
	if (this->file.eof())
		return 0;

	this->file.read(buffer, lBuffer);
	return this->file.gcount();
}

// End daisy namespace
}
