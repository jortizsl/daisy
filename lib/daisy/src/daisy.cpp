//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisy.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	30-Nov-2016
 * @brief	Daisy library
 */

/*
 * INCLUDES
 */

#include "daisy.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{
		/* detail namespace for private functions */

	namespace detail
	{

	}


		/* Other functions */

	// Performs a sanity check of the status of Daisy
	bool sanityCheck(bool verbose)
	{
		if (err::error != err::NOERR)
		{
			if (verbose)
				std::cout << err::strerror(err::error) << std::endl;
			return false;
		}

		return true;
	}

	// Release all the resources
	void finish()
	{
		mem::client.finish();
		mem::detail::finish();
	}

// End daisy namespace
}
