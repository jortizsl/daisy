//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisywritermemz.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	13-Dec-2016
 * @brief	Daisy writer from compressed chared memory
 */

/*
 * INCLUDES
 */

#include <time.h>

#include "daisywritermemz.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: MemWriterZ
 */

MemWriterZ::MemWriterZ() :
	MemWriter()
{
	this->type = TYPE_MEM_Z;
	this->firstChunk = true;
}

MemWriterZ::~MemWriterZ()
{
	this->close();
}

bool MemWriterZ::open(
	const char *name,
	unsigned long beg,
	unsigned long end)
{
	// Open shared memory
	if (!MemWriter::open(name))
		return false;

	// Init inflater
	this->inf.init(this->buffer, this->lBuffer);

	// Start inflater
	this->inf.startInflate();

	// We are starting a new stream
	this->firstChunk = true;

	// Return OK
	return true;
}

void MemWriterZ::close()
{
	// Stop inflater
	this->inf.stopInflate();

	// Close shared memory
	MemWriter::close();

	// Print stats
	//this->inf.printStats();
}

void MemWriterZ::abort()
{
	// Stop inflater
	this->inf.abortInflate();

	// Close shared memory
	MemWriter::close();

	// Print stats
	//this->inf.printStats();
}

int MemWriterZ::write(const char *buffer, unsigned long lBuffer)
{
	// The first data contains the file size
	if (this->firstChunk)
	{
		this->firstChunk = false;

		// Check buffer length
		uint64_t bufferOutLen = *(uint64_t*)buffer;
		if (bufferOutLen != this->lBuffer)
			return 0;

		// Put next chunk
		this->inf.putNextChunk(
			(unsigned char*)&buffer[sizeof(uint64_t)],
			lBuffer - sizeof(uint64_t));

		return lBuffer - sizeof(uint64_t);
	}
	else
	{
		this->inf.putNextChunk((unsigned char*)buffer, lBuffer);
		return lBuffer;
	}
}

// End daisy namespace
}
