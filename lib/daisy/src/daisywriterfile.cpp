//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisywriterfile.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	13-Dec-2016
 * @brief	Daisy writer form file
 */

/*
 * INCLUDES
 */

#include <time.h>

#include "daisywriterfile.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: FileWriter
 */

FileWriter::FileWriter()
{
	this->type = TYPE_FILE;
}

FileWriter::~FileWriter()
{
	return;
}

bool FileWriter::open(
	const char *name,
	unsigned long beg,
	unsigned long end)
{
	this->file.open(name, std::ios::binary);

	if (!this->file)
		return false;

	return true;
}

void FileWriter::close()
{
	this->file.close();
}

void FileWriter::abort()
{
	this->close();
}

int FileWriter::write(const char *buffer, unsigned long lBuffer)
{
	this->file.write(buffer, lBuffer);
	return lBuffer;
}

// End daisy namespace
}
