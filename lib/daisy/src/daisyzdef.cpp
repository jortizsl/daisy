//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyzdef.cpp
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	09-Dec-2016
 * @brief	Class for deflating serialized data using Z lib
 */

/*
 * INCLUDES
 */

#include "daisyzdef.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start z namespace
namespace z
{


/*
 * CLASS
 */

Deflater::Deflater()
{
	this->bufferInLen = 0;
	this->bufferIn = NULL;

	this->bufferOutLen = 0;
	this->bufferOutPos = 0;
	this->bufferOutCurr = 0;
	this->bufferOut = NULL;

	this->running = false;

	this->chunkSize = CHUNK_DEFAULT;
}

Deflater::~Deflater()
{
	this->stopDeflate();

	if (this->bufferOut != NULL)
		delete [] this->bufferOut;
}

bool Deflater::init(
	unsigned char *bufferIn,
	unsigned long bufferInLen,
	unsigned long chunkSize,
	int level)
{
	struct timespec time0;
	struct timespec time1;
	clock_gettime(CLOCK_REALTIME, &time0);

	// Stop deflate if necessary
	this->stopDeflate();

	// Keep buffer in address and size
	this->bufferIn = bufferIn;
	this->bufferInLen = bufferInLen;

	// Init deflate
	this->strm.zalloc = Z_NULL;
    this->strm.zfree = Z_NULL;
    this->strm.opaque = Z_NULL;
    if (deflateInit(&this->strm, level) != Z_OK)
    {
    	this->bufferIn = NULL;
    	this->bufferInLen = 0;
    	return false;
    }

	// Allocate output buffer
    unsigned long lengthBound = deflateBound(&this->strm, bufferInLen);
	if (this->bufferOut == NULL)
	{
		this->bufferOutLen = lengthBound;
		this->bufferOut = new unsigned char[this->bufferOutLen];
	}
	else if (this->bufferOutLen != lengthBound)
	{
		this->bufferOutLen = lengthBound;
		delete [] this->bufferOut;
		this->bufferOut = new unsigned char[this->bufferOutLen];
	}
	this->bufferOutPos = 0;
	this->bufferOutCurr = 0;

	clock_gettime(CLOCK_REALTIME, &time1);
	this->timeInit = utils::getms(time0, time1);

	// Return OK
	return true;
}


void Deflater::startDeflate()
{
	if (this->bufferIn != NULL && this->bufferInLen != 0)
	{
		this->running = true;
		pthread_create(&this->threadDeflate, NULL, Deflater::runDeflate, this);
	}
}

void Deflater::stopDeflate()
{
	if (this->running)
	{
		this->running = false;
		pthread_join(this->threadDeflate, NULL);
	}
}

void Deflater::abortDeflate()
{
	this->stopDeflate();
}

int Deflater::getNextChunk(
	unsigned char *buffer, unsigned long &size, int timeout)
{
	int iterations = 0;
	int waitTime = 100;	// microseconds
	int iterationsMax = timeout / waitTime;

	while (1)
	{
		if (this->bufferOutPos > this->bufferOutCurr + size)
		{
			memcpy(buffer, &this->bufferOut[this->bufferOutCurr], size);
			this->bufferOutCurr += size;
			return Deflater::NEW;
		}
		else if (!this->running)
		{
			memcpy(
				buffer,
				&this->bufferOut[this->bufferOutCurr],
				this->bufferOutPos - this->bufferOutCurr);
			size = this->bufferOutPos - this->bufferOutCurr;
			this->bufferOutCurr = bufferOutPos;
			return Deflater::END;
		}

		usleep(waitTime);
		if (timeout > 0)
		{
			if (iterations >= iterationsMax)
				return Deflater::TIMEOUT;
			iterations++;
		}
	}
}

void Deflater::printStats()
{
	this->timeTotal = this->timeInit + this->timeLoop;
	double timeOthers =
		this->timeLoop -
		this->timeZ -
		this->timeAux;
	double bandwidth =
		(double)this->bufferInLen / this->timeTotal * 1000 / 1024 * 8;
	std::cout << "Total processing time: " << this->timeTotal << " ms\n";
	std::cout << "  Time init: " << this->timeInit << " ms\n";
	std::cout << "  Time loop: " << this->timeLoop << " ms\n";
	std::cout << "    Time z     : " << this->timeZ << " ms\n";
	std::cout << "    Time aux   : " << this->timeAux << " ms\n";
	std::cout << "    Time others: " << timeOthers << " ms\n";
	std::cout << "  Buffer size : " << this->bufferInLen << " bytes\n";
	std::cout << "  Bandwidth   : " << bandwidth << " kbps\n";
}

void *Deflater::runDeflate(void *data)
{
	struct timespec timeInit;
	struct timespec timeFinish;
	struct timespec time0;
	struct timespec time1;

	clock_gettime(CLOCK_REALTIME, &time0);

	Deflater *def = (Deflater*)data;
	unsigned long bufferInPos = 0;
	def->bufferOutPos = 0;
	int flush;
	int ret;

	def->timeLoop = 0.0;
	def->timeZ = 0.0;
	def->timeAux = 0.0;

	clock_gettime(CLOCK_REALTIME, &time1);
	def->timeInit += utils::getms(time0, time1);

	clock_gettime(CLOCK_REALTIME, &timeInit);

    do
    {
    	// Point to next chunk in buffer
    	def->strm.next_in = (Bytef*)&def->bufferIn[bufferInPos];

    	// Check if it's last chunk
    	if (bufferInPos + def->chunkSize >= def->bufferInLen)
    	{
    		def->strm.avail_in = def->bufferInLen - bufferInPos;
    		flush = Z_FINISH;
    	}
    	else
    	{
    		def->strm.avail_in = def->chunkSize;
    		flush = Z_NO_FLUSH;
    	}

        do
        {
            def->strm.avail_out = def->bufferOutLen - def->bufferOutPos;
            def->strm.next_out = &def->bufferOut[def->bufferOutPos];

            clock_gettime(CLOCK_REALTIME, &time0);
            if ((ret = deflate(&def->strm, flush)) == Z_STREAM_ERROR)
            {
            	def->running = false;
            	def->bufferOutLen = 0;
            	break;
            }
            clock_gettime(CLOCK_REALTIME, &time1);
            def->timeZ += utils::getms(time0, time1);
            def->bufferOutPos = def->strm.total_out;
        } while (def->strm.avail_in != 0 && def->running);

        bufferInPos += def->chunkSize;
    } while (flush != Z_FINISH && def->running);

    // Interrupted by error or manually
    if (ret != Z_STREAM_END || flush != Z_FINISH)
    	def->bufferOutPos = 0;

    clock_gettime(CLOCK_REALTIME, &time0);
    deflateEnd(&def->strm);
    clock_gettime(CLOCK_REALTIME, &time1);
	def->timeInit += utils::getms(time0, time1);

    def->running = false;

    clock_gettime(CLOCK_REALTIME, &timeFinish);
    def->timeLoop = utils::getms(timeInit, timeFinish);
	return NULL;
}

// End z namespace
}

// End daisy namespace
}
