//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyreaderfilez.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	12-Dec-2016
 * @brief	Daisy reader from compressed file
 */

/*
 * INCLUDES
 */

#include <time.h>

#include "daisyreaderfilez.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: FileReaderZ
 */

FileReaderZ::FileReaderZ() :
	FileReader()
{
	this->type = TYPE_FILE_Z;

	this->bufferIn = NULL;
	this->bufferInLen = 0;

	this->firstChunk = true;
}

FileReaderZ::~FileReaderZ()
{
	if (this->bufferIn != NULL)
		delete [] this->bufferIn;
	this->bufferIn = NULL;
	this->bufferInLen = 0;
}

bool FileReaderZ::open(
	const char *name,
	unsigned long beg,
	unsigned long end)
{
	// Open file
	if (!FileReader::open(name))
		return false;

	// Get file size
	this->file.seekg(0, std::ios::end);
	this->filesize = this->file.tellg();
    this->bufferInLen = this->filesize;
    this->file.seekg(0);

    // Allocate buffer
    this->bufferIn = new unsigned char[this->bufferInLen];

    // Read data from file
    this->file.read((char*)this->bufferIn, this->bufferInLen);

    // Init deflater
	this->def.init(
		this->bufferIn,
		this->bufferInLen);//,
		//daisy::z::Deflater::CHUNK_DEFAULT,
		//daisy::z::Deflater::LEVEL_DEFAULT);

	// Close file
	FileReader::close();

	// Start deflater
	this->def.startDeflate();

	// We are starting a new stream
	this->firstChunk = true;

	// Return OK
	return true;
}

void FileReaderZ::close()
{
	if (this->bufferIn != NULL)
		delete [] this->bufferIn;
	this->bufferIn = NULL;
	this->bufferInLen = 0;

	// Finish deflater
	this->def.stopDeflate();
	//this->def.printStats();
}

void FileReaderZ::abort()
{
	if (this->bufferIn != NULL)
		delete [] this->bufferIn;
	this->bufferIn = NULL;
	this->bufferInLen = 0;

	// Finish deflater
	this->def.abortDeflate();
	//this->def.printStats();
}

int FileReaderZ::read(char *buffer, unsigned long lBuffer)
{
	if (this->firstChunk)
	{
		this->firstChunk = false;
		memcpy(buffer, &this->filesize, sizeof(uint64_t));
		lBuffer -= sizeof(uint64_t);
		this->def.getNextChunk(
			(unsigned char*)&buffer[sizeof(uint64_t)],
			lBuffer, 0);
		return lBuffer + sizeof(uint64_t);
	}
	else
	{
		this->def.getNextChunk((unsigned char*)buffer, lBuffer, 0);
		return lBuffer;
	}
}

// End daisy namespace
}
