//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyreadermemz.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	12-Dec-2016
 * @brief	Daisy reader from compressed chared memory
 */

/*
 * INCLUDES
 */

#include <time.h>

#include "daisyreadermemz.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: MemReaderZ
 */

MemReaderZ::MemReaderZ() :
	MemReader()
{
	this->type = TYPE_MEM_Z;
	this->firstChunk = true;
}

MemReaderZ::~MemReaderZ()
{
	this->close();
}

bool MemReaderZ::open(
	const char *name,
	unsigned long beg,
	unsigned long end)
{
	// Open shared memory
	if (!MemReader::open(name))
		return false;

	// Init deflater
	this->def.init(
		this->buffer,
		this->lBuffer);

	// Start deflater
	this->def.startDeflate();

	// We are starting a new stream
	this->firstChunk = true;

	// Return OK
	return true;
}

void MemReaderZ::close()
{
	// Finish deflater
	this->def.stopDeflate();

	// Close shared memory
	MemReader::close();

	// Print stats
	//this->def.printStats();
}

void MemReaderZ::abort()
{
	// Finish deflater
	this->def.abortDeflate();

	// Close shared memory
	MemReader::close();

	// Print stats
	//this->def.printStats();
}

int MemReaderZ::read(char *buffer, unsigned long lBuffer)
{
	if (this->firstChunk)
	{
		this->firstChunk = false;

		// Write buffer length
		uint64_t bufferLength = this->lBuffer;
		memcpy(buffer, &bufferLength, sizeof(uint64_t));

		lBuffer -= sizeof(uint64_t);
		this->def.getNextChunk(
			(unsigned char*)&buffer[sizeof(uint64_t)],
			lBuffer, 0);

		return lBuffer + sizeof(uint64_t);
	}
	else
	{
		this->def.getNextChunk((unsigned char*)buffer, lBuffer, 0);
		return lBuffer;
	}
}

// End daisy namespace
}
