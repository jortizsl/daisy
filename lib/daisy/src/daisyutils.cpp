//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyutils.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	12-Dec-2016
 * @brief	Daisy utils
 */

/*
 * INCLUDES
 */

#include "daisyutils.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start utils namespace
namespace utils
{

double getms(struct timespec &time0, struct timespec &time1)
{
	return
    	(double)(time1.tv_sec - time0.tv_sec) * 1E3 +
		(double)(time1.tv_nsec - time0.tv_nsec) * 1E-6;
}

// End utils namespace
}

// End daisy namespace
}
