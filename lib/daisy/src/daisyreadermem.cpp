//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyreadermem.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	12-Dec-2016
 * @brief	Daisy reader from shared memory
 */

/*
 * INCLUDES
 */

#include <time.h>

#include "daisyreadermem.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: MemReader
 */

MemReader::MemReader()
{
	this->type = TYPE_MEM;

	this->addr = NULL;
	this->size = 0;

	this->buffer = NULL;
	this->lBuffer = 0;
	this->posBuffer = 0;
}

MemReader::~MemReader()
{
	this->close();
}

bool MemReader::open(
	const char *name,
	unsigned long beg,
	unsigned long end)
{
	// Open shared memory segment
	int fd = shm_open(name, O_CREAT | O_RDWR, S_IRWXU);

	// Check shared memory file descriptor
	if (fd == -1)
		return false;

	// Check file size
	struct stat sb;
	fstat(fd, &sb);

	if (sb.st_size == 0)
	{
		// The memory has not been initialized -> unlink and exit
		shm_unlink(name);
		return false;
	}

	this->size = sb.st_size;
	this->addr = mmap(
		NULL, this->size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	if (this->addr == NULL)
	{
		shm_unlink(name);
		return false;
	}

	if (this->size <= (int)(sizeof(sem_t) + sizeof(uint8_t)))
	{
		shm_unlink(name);
		return false;
	}

	this->lBuffer = this->size - sizeof(sem_t) - sizeof(uint8_t);
	this->buffer = (unsigned char*)(
		(unsigned char*)this->addr + sizeof(sem_t) + sizeof(uint8_t));
	this->posBuffer = 0;

	// Move position to begin
	if (beg > 0)
	{
		if (beg >= this->lBuffer)
		{
			shm_unlink(name);
			return false;
		}

		this->posBuffer = beg;
	}

	// Adjust length
	if (end > 0)
	{
		if (end < beg)
		{
			shm_unlink(name);
			return false;
		}

		this->lBuffer = end;
	}

	return true;
}

void MemReader::close()
{
	if (this->addr != NULL && this->size != 0)
		munmap(this->addr, this->size);

	this->addr = NULL;
	this->size = 0;
	this->buffer = NULL;
	this->lBuffer = 0;
}

void MemReader::abort()
{
	this->close();
}

int MemReader::read(char *buffer, unsigned long lBuffer)
{
	int bytesRead;

	if (this->lBuffer - this->posBuffer > lBuffer)
		bytesRead = lBuffer;
	else
		bytesRead = this->lBuffer - this->posBuffer;

	if (bytesRead <= 0)
		return 0;

	memcpy(buffer, &this->buffer[this->posBuffer], bytesRead);
	this->posBuffer += bytesRead;
	return bytesRead;
}

// End daisy namespace
}
