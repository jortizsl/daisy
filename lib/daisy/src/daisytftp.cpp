//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisytftp.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	05-Dec-2016
 * @brief	Daisy TFTP common
 */

/*
 * INCLUDES
 */

#include "daisytftp.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start tftp namespace
namespace tftp
{

/*
double getms(struct timespec &time0, struct timespec &time1)
{
	return
    	(double)(time1.tv_sec - time0.tv_sec) * 1E3 +
		(double)(time1.tv_nsec - time0.tv_nsec) * 1E-6;
}*/

// Gets the proper writer depending on the mode and compression.
Writer *writerFactory(int mode, int compression)
{
	if (mode == MODE_OCTET)
	{
		if (compression == COMPRESSION_NONE)
			return new daisy::FileWriter;
		else if (compression == COMPRESSION_Z)
			return new daisy::FileWriterZ;
		else
			return NULL;
	}
	else if (mode == MODE_SHM)
	{
		if (compression == COMPRESSION_NONE)
			return new daisy::MemWriter;
		else if (compression == COMPRESSION_Z)
			return new daisy::MemWriterZ;
		else
			return NULL;
	}
	else
	{
		return NULL;
	}
}

// Gets the proper reader depending on the mode and compression.
Reader *readerFactory(int mode, int compression)
{
	if (mode == MODE_OCTET)
	{
		if (compression == COMPRESSION_NONE)
			return new daisy::FileReader;
		else if (compression == COMPRESSION_Z)
			return new daisy::FileReaderZ;
		else
			return NULL;
	}
	else if (mode == MODE_SHM)
	{
		if (compression == COMPRESSION_NONE)
			return new daisy::MemReader;
		else if (compression == COMPRESSION_Z)
			return new daisy::MemReaderZ;
		else
			return NULL;
	}
	else
	{
		return NULL;
	}
}

// End tftp namespace
}

// End daisy namespace
}
