//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	test_tftpclient.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	05-Dec-2016
 * @brief	Daisy TFTP client test
 */

/*
 * INCLUDES
 */

#include <signal.h>
#include <time.h>
#include <getopt.h>

#include "daisy.h"
#include "daisytftpclient.h"


/*
 * VARIABLES
 */

bool running = true;


/*
 * FUNCTIONS
 */

// Manages termination signals
void signalHandler(int signum)
{
	switch (signum)
    {
    	case SIGINT:
    	case SIGTERM:
    	case SIGKILL:
    		running = false;
    		break;
    }
}


/*
 * DISPLAY HELP FUNCTION
 */

void display_help()
{
	printf("usage: test_tftpclient put|get ADDR REMOTE LOCAL [OPTIONS]\n");
	printf("\n");
	printf("Options:\n");
	printf("  -h, --help           Displays this message\n");
	printf("  -b, --block-size     Block size\n");
	printf("  -z                   Use zlib compresion\n");
	printf("  -m                   Transfer mode (octet, shm)\n");
	printf("  -p, --port           Port (optional, default = %d)\n",
		daisy::tftp::PORT_DEFAULT);
}


/*
 * MAIN
 */

int main(int argc, char *argv[])
{
		/* Manage arguments */

	char short_options[] = "hb:m:zp:";
	struct option long_options[] = {
		{"help",        no_argument,       0, 'h'},
		{"block-size",  required_argument, 0, 'b'},
		{"mode",        required_argument, 0, 'm'},
		{"zlib",        no_argument,       0, 'z'},
		{"port",        required_argument, 0, 'p'},
		{0, 0, 0, 0}};
	int opt_index;
	int opt;
	int opt_blockSize = daisy::tftp::BLOCK_SIZE_DEFAULT;
	bool opt_z = false;
	int opt_port = daisy::tftp::PORT_DEFAULT;
	int opt_mode = daisy::tftp::MODE_OCTET;

	// Start from third argument (first and second arguments are used later)
	optind = 5;

	if (argc < 5)
	{
		display_help();
		exit(-1);
	}

	while (1)
	{
		// getopt_long stores the option index here
		opt_index = 0;

		opt = getopt_long(
			argc, argv, short_options, long_options, &opt_index);

		// Detect the end of the options
		if (opt == -1)
			break;

		switch (opt)
		{
		case 0:
			if (long_options[opt_index].flag != 0)
				break;
			printf("option %s", long_options[opt_index].name);
			if (optarg)
				printf (" with arg %s", optarg);
			printf ("\n");
			break;
		case 'h':
			display_help();
			exit(0);
			break;
		case 'b':
			opt_blockSize = atoi(optarg);
			break;
		case 'm':
			if (strcmp(optarg, "octet") == 0)
			{
				opt_mode = daisy::tftp::MODE_OCTET;
			}
			else if (strcmp(optarg, "shm") == 0)
			{
				opt_mode = daisy::tftp::MODE_SHM;
			}
			else
			{
				printf("wrong mode [%s]\n", optarg);
				display_help();
				exit(-1);
			}
			break;
		case 'z':
			opt_z = true;
			break;
		case 'p':
			opt_port = atoi(optarg);
			break;
		case '?':
			display_help();
			exit(-1);
			break;
		case ':':
			printf("missing parameter for argument\n");
			display_help();
			exit(-1);
			break;
		default:
			printf("invalid option %c (%d)\n", opt, opt);
			display_help();
			exit(-1);
		}
	}

	int compression = daisy::tftp::COMPRESSION_NONE;

	if (opt_z)
		compression = daisy::tftp::COMPRESSION_Z;


		/* Set signal handlers */

	signal(SIGINT,  signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGKILL, signalHandler);


    	/* Configure client */

    daisy::tftp::Client client;
    int ret;
    struct timespec timeInit;
    struct timespec timeFinish;

    client.setCompression(compression);
    client.setBlockSize(opt_blockSize);
    client.setMode(opt_mode);


    	/* Start transmission */

    if (strcmp(argv[1], "put") == 0)
    {
    	clock_gettime(CLOCK_REALTIME, &timeInit);
    	ret = client.writeAndWait(argv[3], argv[4], argv[2], opt_port);
    	clock_gettime(CLOCK_REALTIME, &timeFinish);
    }
    else if (strcmp(argv[1], "get") == 0)
    {
    	clock_gettime(CLOCK_REALTIME, &timeInit);
    	ret = client.readAndWait(argv[3], argv[4], argv[2], opt_port);
    	clock_gettime(CLOCK_REALTIME, &timeFinish);
    }
    else
    {
    	display_help();
    	return 0;
    }

    if (ret != daisy::err::NOERR)
    {
    	std::cout << "Error transmitting data [" << ret << "]" << std::endl;
    }
    else
    {
    	client.printStats();
    }

	return 0;
}
