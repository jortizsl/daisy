//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	test_tftpserver.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	05-Dec-2016
 * @brief	Daisy TFTP server test
 */

/*
 * INCLUDES
 */

#include <signal.h>

#include "daisy.h"
#include "daisytftpserver.h"


/*
 * VARIABLES
 */

bool running = true;


/*
 * FUNCTIONS
 */

// Manages termination signals
void signalHandler(int signum)
{
	switch (signum)
    {
    	case SIGINT:
    	case SIGTERM:
    	case SIGKILL:
    		running = false;
    		break;
    }
}


/*
 * MAIN
 */

int main(int argc, char *argv[])
{
	signal(SIGINT,  signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGKILL, signalHandler);

    daisy::tftp::Server server;
    int ret;
    int port = daisy::tftp::PORT_DEFAULT;

    if (argc == 2)
    	port = atoi(argv[1]);

    if ((ret = server.listen(port)) != daisy::err::NOERR)
    {
    	std::cout << "Error starting TFTP server" << std::endl;
    	return 0;
    }

    while (running)
    {
    	if (server.getState() == daisy::tftp::STATE_ERROR)
    	{
    		std::cout << "Error in the server: ";
    		std::cout << daisy::err::strerror(daisy::err::error) << std::endl;
    		break;
    	}
    	sleep(1);
    }

    if ((ret = server.finish()) != daisy::err::NOERR)
    {
    	std::cout << "Error finishing TFTP server" << std::endl;
    	return 0;
    }

	return 0;
}
