//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	test_z.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	08-Dec-2016
 * @brief	Daisy z classes (deflate and inflate) testing
 */

/*
 * INCLUDES
 */

#include <fstream>
#include <iostream>
#include "daisy.h"
#include "daisyzdef.h"
#include "daisyzinf.h"


/*
 * DEFINES
 */

#define CHUNK_SIZE	512


/*
 * FUNCTINS
 */

void printUsage()
{
	std::cout << "usage: test_z FILEIN FILEOUT\n";
	std::cout << "\n";
	std::cout << "Options:\n";
	std::cout << "\n";
	std::cout << "    FILEIN\n";
	std::cout << "        Path of the input file\n";
	std::cout << "\n";
	std::cout << "    FILEOUT\n";
	std::cout << "        Path of the output file";
	std::cout << std::endl;
}


/*
 * MAIN
 */

int main(int argc, char *argv[])
{
	// Variables
	daisy::z::Deflater deflater;
	daisy::z::Inflater inflater;
    unsigned char *bufferIn;
    unsigned long bufferInLen;
    unsigned char *bufferOut;
    unsigned long bufferOutLen;
    unsigned char bufferMid[CHUNK_SIZE];
    unsigned long bufferMidLen = CHUNK_SIZE;
    int ret;

	// Check arguments
    if (argc != 3)
    {
    	printUsage();
    	return 0;
    }

    // Open source file
    std::ifstream fileSrc;
    fileSrc.open(argv[1]);
    if (!fileSrc)
    {
    	std::cout << "Error opening input file" << std::endl;
    	return 0;
    }

    // Open destiny file
	std::ofstream fileDst;
	fileDst.open(argv[2]);
    if (!fileDst)
    {
    	std::cout << "Error opening output file" << std::endl;
    	fileSrc.close();
    	return 0;
    }

    // Get file size and init buffers
    fileSrc.seekg(0, std::ios::end);
    bufferInLen = fileSrc.tellg();
    bufferOutLen = bufferInLen;
    fileSrc.seekg(0);
    bufferIn = new unsigned char[bufferInLen];
    bufferOut = new unsigned char[bufferOutLen];

    // Read data
    fileSrc.read((char*)bufferIn, bufferInLen);

    // Process data
    deflater.init(bufferIn, bufferInLen);
    inflater.init(bufferOut, bufferOutLen);

    deflater.startDeflate();
    inflater.startInflate();

    while (1)
    {
    	// Get next chunk from deflater
    	bufferMidLen = CHUNK_SIZE;
    	ret = deflater.getNextChunk(bufferMid, bufferMidLen);

    	if (ret == daisy::z::Deflater::TIMEOUT)
    	{
    		usleep(1E3);
    		continue;
    	}
    	else if (ret == daisy::z::Deflater::ERR)
    	{
    		break;
    	}

    	// Pass next chunk to inflater
    	inflater.putNextChunk(bufferMid, bufferMidLen);

    	// That was the last chunk
    	if (ret == daisy::z::Deflater::END)
    		break;
    }

    // Stop processing data
    deflater.stopDeflate();
    inflater.stopInflate();

    // Write data to output file
    fileDst.write((char*)bufferOut, bufferOutLen);

    // Close files
    fileSrc.close();
    fileDst.close();

    // Display statistics
    deflater.printStats();
    inflater.printStats();

	return 0;
}
