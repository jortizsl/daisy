//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	test_memoryread.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	13-Mar-2017
 * @brief	Daisy memory read test
 */

/*
 * INCLUDES
 */

#include <signal.h>
#include "daisy.h"


/*
 * VARIABLES
 */

bool running = true;


/*
 * FUNCTIONS
 */

// Manages termination signals
void signalHandler(int signum)
{
	switch (signum)
    {
    	case SIGINT:
    	case SIGTERM:
    	case SIGKILL:
    		running = false;
    		break;
    }
}


/*
 * MAIN
 */

int main(int argc, char *argv[])
{
	signal(SIGINT,  signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGKILL, signalHandler);

    char *msqName = NULL;

    if (argc >= 2)
    	msqName = argv[1];
    else
    	msqName = (char*)"/daisy";

    int size = 1;
    if (argc >= 3)
    	size = atoi(argv[2]);

	DsyMemArrDeclPub(int, varInt, size);
	DsyMemArrDeclPri(float, domain, varFloat, size);
    //DsyMemDeclPub(int, varInt);
	//DsyMemDeclPri(float, domain, varFloat);


	if (!daisy::sanityCheck(true))
		return 0;

	if (!daisy::mem::client.init(msqName))
		return 0;

	//int waitTime = 5E5;

	while (running)
	{
		//usleep(waitTime);
		//std::cout << "iteration " << i << std::endl;

		//usleep(waitTime);
		varInt.getSync();
		std::cout << "  varInt = " << varInt[0] << std::endl;
		//std::cout << "  varInt = " << (*varInt) << std::endl;

		//usleep(waitTime);
		varFloat.getSync();
		std::cout << "  varFloat = " << varFloat[0] << std::endl;
		//std::cout << "  varFloat = " << (*varFloat) << std::endl;
	}

	daisy::finish();
	return 0;
}
