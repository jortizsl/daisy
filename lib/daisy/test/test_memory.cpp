//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	test_memory.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	30-Nov-2016
 * @brief	Daisy memory test
 */

/*
 * INCLUDES
 */

#include <signal.h>
#include "daisy.h"


/*
 * VARIABLES
 */

bool running = true;


/*
 * FUNCTIONS
 */

// Manages termination signals
void signalHandler(int signum)
{
	switch (signum)
    {
    	case SIGINT:
    	case SIGTERM:
    	case SIGKILL:
    		running = false;
    		break;
    }
}


/*
 * MAIN
 */

int main(int argc, char *argv[])
{
	signal(SIGINT,  signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGKILL, signalHandler);

	DsyMemDeclPub(int, varInt);
	DsyMemDeclPri(float, domain, varFloat);

	if (!daisy::sanityCheck(true))
		return 0;

	int waitTime = 5E5;

	varInt.lock();
	(*varInt) = 0;
	varInt.unlock();

	varFloat.lock();
	(*varFloat) = 0.0;
	varFloat.unlock();

	for (int i = 0; i < 10; i++)
	{
		if (!running)
			break;

		usleep(waitTime);
		std::cout << "iteration " << i << std::endl;

		varInt.lock();
		usleep(waitTime);
		std::cout << "  varInt = " << (*varInt);
		(*varInt)++;
		std::cout << " -> " << (*varInt) << std::endl;
		varInt.unlock();

		varFloat.lock();
		usleep(waitTime);
		std::cout << "  varFloat = " << (*varFloat);
		(*varFloat) += 1.0;
		std::cout << " -> " << (*varFloat) << std::endl;
		varFloat.unlock();
	}

	std::cout << "final" << std::endl;

	varInt.lock();
	std::cout << "  varInt = " << (*varInt) << std::endl;
	varInt.unlock();

	varFloat.lock();
	std::cout << "  varFloat = " << (*varFloat) << std::endl;
	varFloat.unlock();

	daisy::finish();
	return 0;
}
