//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	test_zlib.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	08-Dec-2016
 * @brief	zlib test
 */

/*
 * INCLUDES
 */

//#include <stdio.h>
//#include <assert.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <zlib.h>


/*
 * DEFINES
 */

#define CHUNK 16384


/*
 * FUNCTIONS
 */

int deflate(std::ifstream &fileSrc, std::ofstream &fileDst, int level)
{
    int ret, flush;
    unsigned int have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, level);
    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
    do
    {
    	fileSrc.read((char*)in, CHUNK);
        strm.avail_in = fileSrc.gcount();
        flush = fileSrc.eof() ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do
        {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = deflate(&strm, flush);    /* no bad return value */
            if (ret == Z_STREAM_ERROR)
            	return ret;
            have = CHUNK - strm.avail_out;
            fileDst.write((char*)out, have);
        }while (strm.avail_out == 0);

        if (strm.avail_in != 0)
        {
        	// TODO
        }
        //assert(strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    }while (flush != Z_FINISH);

    if (ret != Z_STREAM_END)
    {
    	// TODO
    }
    //assert(ret == Z_STREAM_END);        /* stream will be complete */

    /* clean up and return */
    deflateEnd(&strm);
    return Z_OK;
}

/* Decompress from file source to file dest until stream ends or EOF.
   inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_DATA_ERROR if the deflate data is
   invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
   the version of the library linked do not match, or Z_ERRNO if there
   is an error reading or writing the files. */
int inflate(std::ifstream &fileSrc, std::ofstream &fileDst)
{
    int ret;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
    ret = inflateInit(&strm);
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do
    {
    	fileSrc.read((char*)in, CHUNK);
    	strm.avail_in = fileSrc.gcount();

        //if (ferror(source)) {
        //    (void)inflateEnd(&strm);
        //    return Z_ERRNO;
        //}
        if (strm.avail_in == 0)
            break;
        strm.next_in = in;

        /* run inflate() on input until output buffer not full */
        do
        {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            if (ret == Z_STREAM_ERROR)
            	return ret;

            switch (ret)
            {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                inflateEnd(&strm);
                return ret;
            }
            have = CHUNK - strm.avail_out;
            fileDst.write((char*)out, have);
        }while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    }while (ret != Z_STREAM_END);

    /* clean up and return */
    inflateEnd(&strm);
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

/* report a zlib or i/o error */
void zerr(int ret)
{
    fputs("zpipe: ", stderr);
    switch (ret) {
    case Z_ERRNO:
        if (ferror(stdin))
            fputs("error reading stdin\n", stderr);
        if (ferror(stdout))
            fputs("error writing stdout\n", stderr);
        break;
    case Z_STREAM_ERROR:
        fputs("invalid compression level\n", stderr);
        break;
    case Z_DATA_ERROR:
        fputs("invalid or incomplete deflate data\n", stderr);
        break;
    case Z_MEM_ERROR:
        fputs("out of memory\n", stderr);
        break;
    case Z_VERSION_ERROR:
        fputs("zlib version mismatch!\n", stderr);
    }
}

void printUsage()
{
	std::cout << "usage: test_zlib c|d FILEIN FILEOUT\n";
	std::cout << "\n";
	std::cout << "Options:\n";
	std::cout << "    c\n";
	std::cout << "        Compress file\n";
	std::cout << "\n";
	std::cout << "    d\n";
	std::cout << "        Decompress file\n";
	std::cout << "\n";
	std::cout << "    FILEIN\n";
	std::cout << "        Path of the input file\n";
	std::cout << "\n";
	std::cout << "    FILEOUT\n";
	std::cout << "        Path of the output file";
	std::cout << std::endl;
}

/*
 * MAIN
 */

int main(int argc, char *argv[])
{
    int ret = 0;

    if (argc != 4)
    {
    	printUsage();
    	return 0;
    }

    std::ifstream fileSrc;
    fileSrc.open(argv[2]);
    if (!fileSrc)
    {
    	std::cout << "Error opening input file" << std::endl;
    	return 0;
    }

	std::ofstream fileDst;
	fileDst.open(argv[3]);
    if (!fileDst)
    {
    	std::cout << "Error opening output file" << std::endl;
    	fileSrc.close();
    	return 0;
    }

    if (strcmp(argv[1], "c") == 0)
    {
    	ret = deflate(fileSrc, fileDst, Z_DEFAULT_COMPRESSION);
    	if (ret != Z_OK)
    		zerr(ret);
    }
    else if (strcmp(argv[1], "d") == 0)
    {
    	ret = inflate(fileSrc, fileDst);
    	if (ret != Z_OK)
    		zerr(ret);
    }
    else
    {
    	std::cerr << "Wrong argument" << std::endl;
    	printUsage();
    }

    fileSrc.close();
    fileDst.close();
    return ret;
}
