#include <iostream>
#include <stdint.h>

#include "daisy.h"

int main(int argc, char *argv[])
{
    std::cout << "Hello, World!" << std::endl;

    std::cout << "Define simple type" << std::endl;
    DsyType(int);
    daisy::mem::detail::printId(int_id);
    std::cout << std::endl;

    std::cout << "Define structure" << std::endl;
    DsyStruct(Test1, int a; float b;);
    daisy::mem::detail::printId(Test1_id);
    std::cout << std::endl;
    Test1 test1;
    test1.a = 0;
    test1.b = 0.0f;

    std::cout << "Define structure with same variables but different typing" << std::endl;
    DsyStruct(Test2, int a;  float b;);
    daisy::mem::detail::printId(Test2_id);
    std::cout << std::endl;

    return 0;
}