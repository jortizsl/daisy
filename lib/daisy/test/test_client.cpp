//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	test_client.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	15-Mar-2017
 * @brief	Daisy client test
 */

/*
 * INCLUDES
 */

#include <signal.h>

#include "daisy.h"
#include "daisyclient.h"
#include "daisyserver.h"


/*
 * VARIABLES
 */

bool running = true;


/*
 * FUNCTIONS
 */

// Manages termination signals
void signalHandler(int signum)
{
	switch (signum)
    {
    	case SIGINT:
    	case SIGTERM:
    	case SIGKILL:
    		running = false;
    		break;
    }
}

void printusage()
{
	std::cout << "usage: client p|g|s MSQ_NAME NAME [BEG] [END]" << std::endl;
}


/*
 * MAIN
 */

int main(int argc, char *argv[])
{
	if (argc < 4)
	{
		printusage();
		return 0;
	}

	signal(SIGINT,  signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGKILL, signalHandler);

    daisy::Client client;

    char operation;

    if (strcmp(argv[1], "p") == 0)
    {
    	operation = 'p';
    }
    else if (strcmp(argv[1], "g") == 0)
    {
    	operation = 'g';
    }
    else if (strcmp(argv[1], "s") == 0)
    {
    	operation = 's';
    }
    else
    {
    	printusage();
    	return 0;
    }

    char *msqname = argv[2];
    char *name = argv[3];
    uint32_t beg = 0;
    uint32_t end = daisy::server::END;

    if (argc == 6)
    {
    	beg = atoi(argv[4]);
    	end = atoi(argv[5]);

    	if (end == 0)
    		end = daisy::server::END;
    }

    client.init(msqname);
    
    bool res = true;

    if (operation == 'p')
    {
    	res = client.put(name, beg, end);
    }
    else if (operation == 'g')
    {
    	res = client.getAsync(name, beg, end);
    }
    else if (operation == 's')
    {
    	res = client.getSync(name, beg, end);
    }
    else
    {
        std::cout << "Unknown operation" << std::endl;
    }

    if (!res)
    {
        std::cout << "Error: " << daisy::err::strerror(daisy::err::error);
        std::cout << std::endl;
    }

    client.finish();

	return 0;
}
