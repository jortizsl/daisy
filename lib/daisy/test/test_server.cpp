//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	test_server.cpp
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	15-Mar-2017
 * @brief	Daisy server test
 */

/*
 * INCLUDES
 */

#include <signal.h>

#include "daisy.h"
#include "daisyserver.h"


/*
 * VARIABLES
 */

bool running = true;


/*
 * FUNCTIONS
 */

// Manages termination signals
void signalHandler(int signum)
{
	switch (signum)
    {
    	case SIGINT:
    	case SIGTERM:
    	case SIGKILL:
    		running = false;
    		break;
    }
}


/*
 * MAIN
 */

int main(int argc, char *argv[])
{
	if (argc != 5)
	{
		std::cout << "usage: server UDP_PORT UDP_DEV MSQ_NAME TFTP_PORT" << std::endl;
		return 0;
	}

	signal(SIGINT,  signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGKILL, signalHandler);

    daisy::Server server;
    int udpport = atoi(argv[1]);
    char *udpdev = argv[2];
    char *msqname = argv[3];
    int tftpport = atoi(argv[4]);

    server.init(udpport, udpdev, msqname, tftpport, true);

    while (running)
    {
    	sleep(1);
    }

    server.finish();

	return 0;
}
