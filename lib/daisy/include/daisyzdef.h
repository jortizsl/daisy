//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyzdef.h
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	09-Dec-2016
 * @brief	Class for deflating serialized data using Z lib
 */

#ifndef DAISYZDEF_H_
#define DAISYZDEF_H_

/*
 * INCLUDES
 */

#include <string.h>
#include <pthread.h>
#include <zlib.h>
#include <iostream>
#include "daisyutils.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start z namespace
namespace z
{


/*
 * CLASS
 */

class Deflater
{
private:
	unsigned long bufferInLen;
	unsigned char *bufferIn;

	unsigned long bufferOutLen;
	unsigned long bufferOutPos;
	unsigned long bufferOutCurr;
	unsigned char *bufferOut;

	pthread_t threadDeflate;
	bool running;
	z_stream strm;
	unsigned long chunkSize;

	// Profiling
	double timeTotal;
	double timeInit;
	double timeLoop;
	double timeZ;
	double timeAux;

public:
	static const int ERR		= 0;
	static const int NEW		= 1;
	static const int TIMEOUT	= 2;
	static const int END		= 3;
	static const int CHUNK_DEFAULT = 16384;
	static const int LEVEL_DEFAULT = 1;	// Z_DEFAULT_COMPRESSION

public:
	Deflater();
	~Deflater();

	bool init(
		unsigned char *bufferIn,
		unsigned long bufferInLen,
		unsigned long chunkSize = CHUNK_DEFAULT,
		int level = LEVEL_DEFAULT);

	void startDeflate();
	void stopDeflate();
	void abortDeflate();

	int getNextChunk(
		unsigned char *buffer,
		unsigned long &size,
		int timeout = 0);

	void printStats();

private:
	static void *runDeflate(void *data);
};

// End z namespace
}

// End daisy namespace
}

#endif	/* DAISYZDEF_H_ */
