//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisywritermem.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	13-Dec-2016
 * @brief	Daisy writer from shared memory
 */

#ifndef DAISYWRITERMEM_H_
#define DAISYWRITERMEM_H_

/*
 * INCLUDES
 */

#include <semaphore.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "daisywriter.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: MemWriter
 */

class MemWriter : public Writer
{
protected:
	// Shared memory address and size
	void *addr;
	int size;

	// Buffer and length
	unsigned char *buffer;
	unsigned long lBuffer;
	unsigned long posBuffer;

public:
	MemWriter();
	~MemWriter();
	bool open(
		const char *name,
		unsigned long beg = 0,
		unsigned long end = 0);
	void close();
	void abort();
	int write(const char *buffer, unsigned long lBuffer);
};

// End daisy namespace
}

#endif	/* DAISYWRITERMEM_H_ */
