//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisywriterfilez.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	13-Dec-2016
 * @brief	Daisy  writer from compressed file
 */

#ifndef DAISYWRITERFILEZ_H_
#define DAISYWRITERFILEZ_H_

/*
 * INCLUDES
 */

#include <stdint.h>

#include "daisywriterfile.h"
#include "daisyzinf.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: FileWriterZ
 */

class FileWriterZ : public FileWriter
{
private:
	unsigned char *bufferOut;
	unsigned long bufferOutLen;
	daisy::z::Inflater inf;
	bool firstChunk;

public:
	FileWriterZ();
	~FileWriterZ();
	bool open(
		const char *name,
		unsigned long beg = 0,
		unsigned long end = 0);
	void close();
	void abort();
	int write(const char *buffer, unsigned long lBuffer);
};

// End daisy namespace
}

#endif	/* DAISYWRITERFILEZ_H_ */
