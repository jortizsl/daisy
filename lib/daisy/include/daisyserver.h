//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyserver.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	14-Mar-2017
 * @brief	Daisy server
 */

#ifndef DAISYSERVER_H_
#define DAISYSERVER_H_

/*
 * INCLUDES
 */

#include <iostream>
#include <string.h>

#include <deque>
#include <list>
#include <string>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <net/if.h>
#include <bitset>

#include <errno.h>
#include <stdio.h>

#include <mqueue.h>

#include "daisytftpclient.h"
#include "daisytftpserver.h"

#include "trie.h"


/*
 * DEFINES
 */

//#define DAISYSERVER_DEBUG


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{


// Start server namespace
namespace server
{

/** Buffer size */
const int BUFFER_SIZE = 256;

/** Default port */
const int PORT_DEFAULT = 44390;

/** Message queue maximum message size */
const int MSQ_MSGSIZE = 256;

/** Message queue maximum number of messages */
const int MSQ_MAXMSG = 10;

/** Maximum value for the end of the segment */
const unsigned long END = (uint32_t)-1;

/** Semaphore codification */
const char SEM_NAME_CODED[65] =
	"abcdefghijklmnopqrstuvwxyz"
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"1234567890_-";

/** Memory segment information */
typedef struct
{
	struct sockaddr_in remoteAddr;	//!< Last address sending the request
	unsigned short remotePort;		//!< Port of the remote TFTP server
	//int32_t id;					//!< Last id sending the request
	uint32_t beg;					//!< Begin of the segment
	uint32_t end;					//!< End fo the segment (0 if till end)
}Segment;

/** Item of the request list */
typedef struct
{
	//std::string name;				//!< Name of the shared memory
	std::deque<Segment> segments;	//!< List of memory segments
}Request;

// End server namespace
}


/*
 * CLASS
 */

class Server
{
private:
		/** @name UDP conection */
	// @{
	int udp_s;								//!< UDP socket
	struct sockaddr_in udp_laddr;			//!< UDP local address
	struct sockaddr_in udp_baddr;			//!< UDP broadcast address
	pthread_t udp_thread;					//!< UDP thread
	int udp_lBuffer;						//!< UDP buffer length
	char udp_buffer[server::BUFFER_SIZE];	//!< UDP buffer
	struct in_addr ipv4Local;				//!< Local IPv4 address
	struct in_addr ipv4Broadcast;			//!< Broadcast IPv4 address
	// @}

		/** @name MSQ */
	// @{
	mqd_t msq;								//!< Message queue descriptor
	pthread_t msq_thread;					//!< Message queue thread
	int msq_lBuffer;						//!< Messqge queue buffer length
	char msq_buffer[server::BUFFER_SIZE];	//!< Messqge queue buffer
	// @}

	tftp::Server tftpServer;				//!< TFTP server
	int tftpPort;							//!< TFTP port

	std::trie<server::Request> requests;	//!< List of requests
	pthread_mutex_t reqMutex;	//!< Mutex to control the access to the request list

	bool running;							//!< Is the server running?

public:
	/** Constructor */
	Server();

	/** Desctructor */
	~Server();

	/** Init the server:
	 *   - Opens the UDP port
	 *   - Opens the Message Queue
	 *   - Starts the TFTP server
	 *
	 * @param [in] udpport		UDP port
	 * @param [in] udpdev		Broadcast network interface
	 * @param [in] msqname		Message queue name
	 * @param [in] tftpport		TFTP port
	 * @param [in] resetMsq		Reset message queue flag
	 * @return 					True if OK, False if error
	 */
	bool init(
		int udpport,
		const std::string &udpdev,
		const std::string &msqname,
		int tftpport,
		bool resetMsq);

	/** Close all the services of the server
	 * @return					True if OK, False if error
	 */
	bool finish();

private:
	/** UDP thread function */
	static void *udp_run(void *_);

	/** Message queue thread function */
	static void *msq_run(void *_);

	/** Extract name, begin and end from buffer (UDP or message queue) */
	static void getDataFromBuffer(
		char *buffer,
		int lBuffer,
		std::string &name,
		std::string &semName,
		uint32_t &beg,
		uint32_t &end);

	/** Set local address and broadcast address */
	bool setLocalNBroadcastAddr(const char *dev);
};

// End daisy namespace
}

#endif	/* DAISYSERVER_H_ */
