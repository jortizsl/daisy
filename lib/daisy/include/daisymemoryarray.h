//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisymemoryarray.h
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	30-Nov-2016
 * @brief	Memory array wrapper for Daisy
 */

#ifndef DAISYMEMORYARRAY_H_
#define DAISYMEMORYARRAY_H_

/*
 * INCLUDES
 */

#include <typeinfo>
#include "daisyerror.h"
#include "daisymem.h"


/*
 * MACROS
 */

#define DsyMemArr(type,name)						daisy::mem::Array<type> name

#define DsyMemArrDeclPub(type,name,count)			daisy::mem::Array<type> name(#name,count)
#define DsyMemArrDeclPri(type,domain,name,count)	daisy::mem::Array<type> name(#domain,#name,count)

#define DsyMemArrCstrPub(name,count)				name(#name,count)
#define DsyMemArrCstrPri(domain,name,count)			name(#domain,#name,count)

#define DsyMemArrInitPub(name,count)				name.init(#name,count)
#define DsyMemArrInitPri(domain,name,count)			name.init(#domain,#name,count)


/*
 * CLASS
 */

// Start daisy namespace
namespace daisy
{

// Start mem namespace
namespace mem
{

template <class T>
class Array
{
private:
	std::string name;
	Segment *segment;
	unsigned int count;
	static T dummy;
	ClientSem *sem;

public:
	Array(
		const char *name,
		unsigned int count)
	{
		this->segment = NULL;
		this->count = 0;
		this->sem = NULL;
		this->init(name, count);
	}

	Array(
		const char *domain,
		const char *name,
		unsigned int count)
	{
		this->segment = NULL;
		this->count = 0;
		this->sem = NULL;
		std::string fullname =
			std::string(domain) + std::string(".") + std::string(name);
		this->init(fullname.c_str(), count);
	}

	~Array()
	{
		if (this->segment != NULL && !this->name.empty())
			detail::unregisterSegment(this->name);

		this->name.clear();
		this->segment = NULL;
	}

	T &operator [] (unsigned int index)
	{
		if (this->segment == NULL)
			return Array::dummy;
		else if (index >= this->count)
			return Array::dummy;
		else
			return ((T*)this->segment->data)[index];
	}

	bool lock()
	{
		return detail::lockSegment(this->segment);
	}

	bool unlock()
	{
		return detail::unlockSegment(this->segment);
	}

	bool put()
	{
		return daisy::mem::client.put(this->segment->nameShared);
	}

	bool getAsync()
	{
		return daisy::mem::client.getAsync(this->segment->nameShared);
	}

	bool get()
	{
		this->sem = daisy::mem::client.get(this->segment->nameShared);
		return this->sem != NULL;
	}

	bool getSync()
	{
		return daisy::mem::client.getSync(this->segment->nameShared);
	}

	bool sync()
	{
		if (this->sem == NULL)
		{
			daisy::err::error = daisy::err::ESEMNOINIT;
			return false;
		}

		bool ret = daisy::mem::client.sync(this->sem);
		this->sem = NULL;
		return ret;
	}

	bool init(const char *name, unsigned int count)
	{
		this->segment = NULL;
		this->count = 0;

		std::string segmentType = typeid(T).name();
		ID id = detail::getId(segmentType);
		unsigned long size = sizeof(T) * count;

		if (!detail::registerSegment(
				name, id, size))
			return false;

		// Get pointer to the segment
		this->segment =	detail::getSegment(name);

		if (this->segment == NULL)
		{
			daisy::err::error = err::EINVSEG;
			return false;
		}

		if (this->segment->sizeData != size)
		{
			daisy::err::error = err::ESHMSIZE;
			this->segment = NULL;
			return false;
		}

		this->count = count;
		this->name = name;
		return true;
	}

	bool init(const char *domain, const char *name, unsigned int count)
	{
		std::string fullname =
			std::string(domain) + std::string(".") + std::string(name);
		return this->init(fullname.c_str(), count);
	}
};

template <class T>
T Array<T>::dummy;

// End mem namespace
}

// End daisy namespace
}

#endif	/* NUXMEMORY_H_ */
