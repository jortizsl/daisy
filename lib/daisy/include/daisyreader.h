//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyreader.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	12-Dec-2016
 * @brief	Daisy reader
 */

#ifndef DAISYREADER_H_
#define DAISYREADER_H_

/*
 * INCLUDES
 */

#include <iostream>
#include <string.h>
#include "daisyutils.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: Reader
 */

class Reader
{
protected:
	/** Reader type */
	int type;

public:
	// Reader types
	static const int TYPE_NONE		= 0;	//!< No type
	static const int TYPE_FILE		= 1;	//!< File
	static const int TYPE_FILE_Z	= 2;	//!< Compressed file
	static const int TYPE_MEM 		= 3;	//!< Memory
	static const int TYPE_MEM_Z 	= 4;	//!< Compressed memory

	/** Constructor */
	Reader() { this->type = TYPE_NONE; }

	/** Destructor */
	virtual ~Reader() {}

	/** Get reader type */
	int getType() { return this->type; }

	/** Open the reader
	 * @param [in] name		Name of the reader
	 * @param [in] beg		Start position
	 * @param [in] end		End position
	 * @return				True if the reader was open properly,
	 * 						False otherwise
	 */
	virtual bool open(
		const char *name,
		unsigned long beg = 0,
		unsigned long end = 0) = 0;

	/** Close the reader */
	virtual void close() = 0;

	/** Force the closing of the reader */
	virtual void abort() = 0;

	/** Read data and put it in the buffer
	 * @param [in] buffer	Buffer to place the data
	 * @param [in] lBuffer	Size of the buffer
	 * @return				Number of bytes writen in the buffer
	 */
	virtual int read(char *buffer, unsigned long lBuffer) = 0;
};

// End daisy namespace
}

#endif	/* DAISYREADER_H_ */
