//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisytftp.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	05-Dec-2016
 * @brief	Daisy TFTP common
 */

#ifndef DAISYTFTP_H_
#define DAISYTFTP_H_

/*
 * INCLUDES
 */

#include <time.h>

#include "daisyutils.h"

#include "daisyerror.h"

#include "daisywriter.h"
#include "daisywriterfile.h"
#include "daisywriterfilez.h"
#include "daisywritermem.h"
#include "daisywritermemz.h"

#include "daisyreader.h"
#include "daisyreaderfile.h"
#include "daisyreaderfilez.h"
#include "daisyreadermem.h"
#include "daisyreadermemz.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start tftp namespace
namespace tftp
{

/** Buffer size */
const int BUFFER_SIZE = 65536;

/** Timeout (seconds) */
const int TIMEOUT_SEC = 1;

/** Timeout (micro seconds) */
const int TIMEOUT_USEC = 0;

/** Filename length */
const int FILENAME_LENGTH = 256;

/** Address length */
const int ADDRESS_LENGTH = 256;

/** Mode length */
const int MODE_LENGTH = 32;

/** Option length */
const int OPTION_LENGTH = 256;

/** Option string length */
const int OPTIONSTR_LENGTH = 1024;

/** Default block size */
const int BLOCK_SIZE_DEFAULT = 512;

/** Default port */
const int PORT_DEFAULT = 44380;


	/** @name Op codes */
// @{
const int OP_RRQ	= 1;
const int OP_WRQ	= 2;
const int OP_DATA	= 3;
const int OP_ACK	= 4;
const int OP_ERROR	= 5;
const int OP_OACK	= 6;
// @}


	/** @name Standard TFTP errors */
// @{
const int ENODEF 		= 0;	//!< Not defined, see error message (if any)
const int ENOTFOUND 	= 1;	//!< File not found
const int EACCVIOL 		= 2;	//!< Access violation
const int EFULL 		= 3;	//!< Disk full or allocation exceeded
const int EILLOP 		= 4;	//!< Illegal TFTP operation
const int EUNKID		= 5;	//!< Unknown transfer ID
const int EFILEEXISTS	= 6;	//!< File already exists
const int ENOUSER 		= 7;	//!< Not such user
// @}


	/** @name Modes */
// @{
const int MODE_NETASCII	= 0;
const int MODE_OCTET	= 1;
const int MODE_MAIL		= 2;
const int MODE_SHM		= 3;
// @}


	/** @name Compression */
// @{
const int COMPRESSION_NONE	= 0;
const int COMPRESSION_Z		= 1;
// @}


	/** @name Errors */
// @{
// States for server
const int STATE_CLOSED 		= 0;
const int STATE_LISTENING	= 1;
const int STATE_MUSTCLOSE	= 2;
const int STATE_ERROR		= 3;

// State for client
const int STATE_IDLE		= 0;
const int STATE_READING		= 1;
const int STATE_WRITING		= 2;
const int STATE_MUST_END	= 4;
// @}

/** Gets the proper writer depending on the mode and compression.
 * @param [in] mode			Transmission mode (see symbols before)
 * @param [in] compression	Compression algorithm (see symbols before)
 * @return					Pointer to writer or null if mode or compression
 * 							are not recognized
 */
Writer *writerFactory(int mode, int compression);

/** Gets the proper reader depending on the mode and compression.
 * @param [in] mode			Transmission mode (see symbols before)
 * @param [in] compression	Compression algorithm (see symbols before)
 * @return					Pointer to reader or null if mode or compression
 * 							are not recognized
 */
Reader *readerFactory(int mode, int compression);

// End tftp namespace
}

// End daisy namespace
}

#endif	/* DAISYTFTPSERVER_H_ */
