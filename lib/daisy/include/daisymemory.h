//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisymemory.h
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	30-Nov-2016
 * @brief	Memory wrapper for Daisy
 */

#ifndef DAISYMEMORY_H_
#define DAISYMEMORY_H_

/*
 * INCLUDES
 */

#include <typeinfo>

#include "daisyerror.h"
#include "daisymem.h"


/*
 * MACROS
 */

#define DsyMem(type,name)				daisy::mem::Variable<type> name

#define DsyMemDeclPub(type,name)		daisy::mem::Variable<type> name(#name)
#define DsyMemDeclPri(type,domain,name)	daisy::mem::Variable<type> name(#domain,#name)

#define DsyMemCstrPub(name)				name(#name)
#define DsyMemCstrPri(domain,name)		name(#domain,#name)

#define DsyMemInitPub(name)				name.init(#name)
#define DsyMemInitPri(domain,name)		name.init(#domain,#name)


/*
 * CLASS
 */

// Start daisy namespace
namespace daisy
{

// Start mem namespace
namespace mem
{

template <class T>
class Variable
{
private:
	std::string name;
	Segment *segment;
	static T dummy;
	ClientSem *sem;

public:
	Variable(const char *name)
	{
		this->segment = NULL;
		this->sem = NULL;
		this->init(name);
	}

	Variable(
		const char *domain,
		const char *name)
	{
		this->segment = NULL;
		this->sem = NULL;
		std::string fullname =
			std::string(domain) + std::string(".") + std::string(name);
		this->init(fullname.c_str());
	}

	~Variable()
	{
		if (this->segment != NULL && !this->name.empty())
			detail::unregisterSegment(this->name);

		this->name.clear();
		this->segment = NULL;
	}

	T *operator ->()
	{
		if (this->segment == NULL)
			return NULL;
		else
			return (T*)this->segment->data;
	}

	T &operator *()
	{
		if (this->segment == NULL)
			return Variable::dummy;
		else
			return *(T*)this->segment->data;
	}

	bool lock()
	{
		return detail::lockSegment(this->segment);
	}

	bool unlock()
	{
		return detail::unlockSegment(this->segment);
	}

	bool put()
	{
		return daisy::mem::client.put(this->segment->nameShared);
	}

	bool getAsync()
	{
		return daisy::mem::client.getAsync(this->segment->nameShared);
	}

	bool get()
	{
		this->sem = daisy::mem::client.get(this->segment->nameShared);
		return this->sem != NULL;
	}

	bool getSync()
	{
		return daisy::mem::client.getSync(this->segment->nameShared);
	}

	bool put(void *begAddr, void *endAddr)
	{
		uint32_t beg = begAddr - (unsigned long)this->segment->data;
		uint32_t end = endAddr - (unsigned long)this->segment->data;
		return daisy::mem::client.put(this->segment->nameShared, beg, end);
	}

	bool getAsync(void *begAddr, void *endAddr)
	{
		uint32_t beg = begAddr - (unsigned long)this->segment->data;
		uint32_t end = endAddr - (unsigned long)this->segment->data;
		return daisy::mem::client.getAsync(this->segment->nameShared, beg, end);
	}

	bool get(void *begAddr, void *endAddr)
	{
		uint32_t beg = begAddr - (unsigned long)this->segment->data;
		uint32_t end = endAddr - (unsigned long)this->segment->data;
		this->sem = daisy::mem::client.get(this->segment->nameShared, beg, end);
		return this->sem != NULL;
	}

	bool getSync(void *begAddr, void *endAddr)
	{
		uint32_t beg = begAddr - (unsigned long)this->segment->data;
		uint32_t end = endAddr - (unsigned long)this->segment->data;
		return daisy::mem::client.getSync(this->segment->nameShared, beg, end);
	}

	bool sync()
	{
		if (this->sem == NULL)
		{
			daisy::err::error = daisy::err::ESEMNOINIT;
			return false;
		}

		bool ret = daisy::mem::client.sync(this->sem);
		this->sem = NULL;
		return ret;
	}

	bool init(const char *name)
	{
		this->segment = NULL;

		std::string segmentType = typeid(T).name();
		ID id = detail::getId(segmentType);
		unsigned long size = sizeof(T);

		if (!detail::registerSegment(name, id, size))
			return false;

		// Get pointer to the segment
		this->segment =	detail::getSegment(name);

		if (this->segment == NULL)
		{
			daisy::err::error = err::EINVSEG;
			return false;
		}

		if (this->segment->sizeData != size)
		{
			daisy::err::error = err::ESHMSIZE;
			this->segment = NULL;
			return false;
		}

		this->name = name;
		return true;
	}

	bool init(const char *domain, const char *name)
	{
		std::string fullname =
			std::string(domain) + std::string(".") + std::string(name);
		return this->init(fullname.c_str());
	}
};

template <class T>
T Variable<T>::dummy;

// End mem namespace
}

// End daisy namespace
}

#endif	/* DAISYMEMORY_H_ */
