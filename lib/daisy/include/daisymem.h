//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisymem.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	30-Nov-2016
 * @brief	Daisy library - Memory variables and functions
 */

#ifndef DAISYMEM_H_
#define DAISYMEM_H_

/*
 * INCLUDES
 */

	/* General */
#include <string.h>
#include <errno.h>

	/* Shared memory */
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

	/* CRC */
#include <zlib.h>

	/* Utils */
#include "trie.h"

	/* Daisy */
#include "daisytypes.h"
#include "daisyerror.h"
#include "daisyclient.h"


/*
 * MACROS
 */

#define DsyType(type)		\
	daisy::mem::ID type ## _id = daisy::mem::detail::getId(#type);
#define DsyStruct(name,def)	\
	typedef struct {def}__attribute__((packed)) name; \
	daisy::mem::ID name ## _id = daisy::mem::detail::getId(#def);


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start mem namespace
namespace mem
{

/*
 * TYPES
 */

/** ID to idenfity a memory segment */
typedef struct
{
	unsigned char id[DAISY_IDSIZE];	//!< Just a sequence of bytes
}ID;

/** Memory segment */
typedef struct
{
	// Local information
	std::string name;			//!< Name of the segment
	std::string nameShared;		//!< Name of the segment in shared memory format
	ID id;						//!< ID of the segment
	int users;					//!< Number of local users registered to the segment

	// Shared information
	uint32_t size;				//!< Total size of the allocated memory
	void *addr;					//!< Address of the allocated memory
	sem_t *sem;					//!< Semaphore for data access synchronization
	uint8_t *nproc;				//!< Total number of processes sharing the memory
	uint32_t sizeData;			//!< Size of the data segment
	void *data;					//!< Memory address of the data segment
}Segment;


extern daisy::Client client;	//!< Client to synchronize memory segments


	/** @name detail namespace for private variables and functions */
// @{
namespace detail
{
	/** Memory segments */
	extern std::trie<Segment> segments;

	/** Release all the resources */
	void finish();

	/** Register a new segment
	 * @param name	Name of the new segment
	 * @param id	ID of the new segment
	 * @param size	Size of the new segment
	 * @return		True if success, false otherwise
	 */
	bool registerSegment(
		const std::string &name,
		ID id,
		unsigned long size);

	/** Unregister segment
	 *  If there are no more users of the segment, the memory is deallocated
	 * @param name	Name of the segment to unregister
	 * @return		True if success, false otherwise
	 */
	bool unregisterSegment(const std::string &name);

	/** Get a memory segment
	 * @param name	Name of the segment
	 * @return		Pointer to the segment, or NULL if it was not found
	 */
	Segment *getSegment(const std::string &name);

	/** Generate a segment ID from a name
	 * @param name	Name to use for the generation of the ID
	 * @return		Segment ID
	 */
	ID getId(const std::string &name);

	/** Print ID
	 * @param id	ID
	 */
	void printId(ID &id);

	/** Allcates a memory segment in normal memory
	 * @param segment	Pointer to the memory segment
	 * @param clear		If true, the memory is writen with zeros (default
	 * 					parameter)
	 * @return			True if the memory was allocated succesfully,
	 * 					false otherwise
	 */
	bool allocateSegment(
		Segment *segment,
		bool clear = true);

	/** Deallocates a memory segment
	 * @param segment	Pointer to the memory segment
	 * @return			True if the memory was deallocated succesfully,
	 * 					false otherwise
	 */
	bool deallocateSegment(
		Segment *segment);

	/** Locks a memory segment
	 * @param segment	Pointer to the memory segment
	 * @return			True if the memory was locked succesfully,
	 * 					false otherwise
	 */
	bool lockSegment(
		Segment *segment);

	/** Unlocks a memory segment
	 * @param segment	Pointer to the memory segment
	 * @return			True if the memory was unlocked succesfully,
	 * 					false otherwise
	 */
	bool unlockSegment(
		Segment *segment);

	/** Operator to compare two segment IDs
	 * @param id1	First ID to compare
	 * @param id2	Second ID to compare
	 * @return		True if they are different,
	 * 				False if the are the same
	 */
	inline bool operator != (ID &id1, ID &id2)
	{
		return memcmp(&id1, &id2, sizeof(ID)) != 0;
	}

	/** Operator to compare two memory segments
	 * @param seg1	First segment to compare
	 * @param seg2	Second segment to compare
	 * @return		True if they are different,
	 * 				False if the are the same
	 */
	inline bool operator != (Segment &seg1, Segment &seg2)
	{
		return (seg1.name.compare(seg2.name) ||
				seg1.nameShared.compare(seg2.nameShared) ||
				seg1.id != seg2.id);
	}
}
// @}

// End mem namespace
}

// End daisy namespace
}

#endif	/* DAISYMEM_H_ */
