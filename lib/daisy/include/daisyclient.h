//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyclient.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	14-Mar-2017
 * @brief	Daisy client
 */

#ifndef DAISYCLIENT_H_
#define DAISYCLIENT_H_

/*
 * INCLUDES
 */

#include <string.h>

#include <deque>
#include <list>
#include <string>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include <mqueue.h>

#include <errno.h>
#include <stdio.h>

#include <gsl/gsl_rng.h>

#include "daisyserver.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{


/*
 * TYPES
 */

typedef struct
{
	sem_t *sem;
	char name[10];
	bool available;
}ClientSem;


/*
 * CLASS
 */

class Client
{
private:
		/** @name MSQ */
	// @{
	mqd_t msq;								//!< Message queue descriptor
	int msq_lBuffer;						//!< Message queue buffer length
	char msq_buffer[server::BUFFER_SIZE];	//!< Message queue buffer
	// @}

	pthread_mutex_t sems_mutex;		//!< Mutex to access to the sempahore list
	std::list<ClientSem> sems;		//!< List of semaphores
	struct timespec timeout;		//!< Timeout for the synchronized communication

	gsl_rng *rng;					//!< Random number generator

public:
	/** Constructor */
	Client();

	/** Desctructor */
	~Client();

	/** Init client
	 * @param [in] msqname	Message queue name
	 * @return				True if OK, False if error
	 */
	bool init(const std::string &msqname);

	/** Finish client */
	void finish();

	/** Send a put request to all the remote servers
	 * @param [in] name		Shared memory name
	 * @param [in] beg		Memory begin
	 * @param [in] end		Memory end
	 * @return				True if OK, False if error
	 */
	bool put(
		const std::string &name,
		uint32_t beg = 0,
		uint32_t end = server::END);

	/** Send a get request to the local server asynchronously
	 * @param [in] name		Shared memory name
	 * @param [in] beg		Memory begin
	 * @param [in] end		Memory end
	 * @return				True if OK, False if error
	 */
	bool getAsync(
		const std::string &name,
		uint32_t beg = 0,
		uint32_t end = server::END);

	/** Send a get request to the local server and prepare for wait for answer
	 * @param [in] name		Shared memory name
	 * @param [in] beg		Memory begin
	 * @param [in] end		Memory end
	 * @return				Pointer to semaphore
	 */
	ClientSem *get(
		const std::string &name,
		uint32_t beg = 0,
		uint32_t end = server::END);

	/** Send a get request to the local server and wait for answer
	 * @param [in] name		Shared memory name
	 * @param [in] beg		Memory begin
	 * @param [in] end		Memory end
	 * @return				True if OK, False if error
	 */
	bool getSync(
		const std::string &name,
		uint32_t beg = 0,
		uint32_t end = server::END);

	/** Waits for the sync signal
	 * @param [in] sem		Sempahore
	 * @return				True if OK, False if error
	 */
	bool sync(ClientSem *sem);

	/** Set timeout for the synchronized operations
	 * @param [in] timeout	Timeout
	 */
	void setTimeout(struct timespec &timeout);

private:
	/** Send a request to the local server
	 * @param [in] header	Header
	 * @param [in] name		Shared memory name
	 * @param [in] beg		Memory begin
	 * @param [in] end		Memory end
	 * @return				True if OK, False if error
	 */
	bool req(
		char header,
		const std::string &name,
		uint32_t beg = 0,
		uint32_t end = server::END);

	/** Get a new named semaphore, and create one if necessary
	 * @return				Available semaphore
	 */
	ClientSem *getSemaphore();
};

// End daisy namespace
}

#endif	/* DAISYCLIENT_H_ */
