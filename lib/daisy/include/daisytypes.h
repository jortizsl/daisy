//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisytypes.h
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	30-Nov-2016
 * @brief	This file declares all the Daisy types and symbols
 */

#ifndef DAISYTYPES_H_
#define DAISYTYPES_H_

/*
 * INCLUDES
 */

#include <stdint.h>
#include <list>
#include <semaphore.h>
#include "trie.h"


/*
 * DEFINES
 */

	/** @name Terminal colors */
// @{
// Reset
#define DAISY_RESET		"\033[0m"

// Regular
#define DAISY_TXT_BLK   "\033[0;30m"
#define DAISY_TXT_RED   "\033[0;31m"
#define DAISY_TXT_GRN   "\033[0;32m"
#define DAISY_TXT_YLW   "\033[0;33m"
#define DAISY_TXT_BLU   "\033[0;34m"
#define DAISY_TXT_PUR   "\033[0;35m"
#define DAISY_TXT_CYN   "\033[0;36m"
#define DAISY_TXT_WHT   "\033[0;37m"

// Bold
#define DAISY_BLD_BLK   "\033[1;30m"
#define DAISY_BLD_RED   "\033[1;31m"
#define DAISY_BLD_GRN   "\033[1;32m"
#define DAISY_BLD_YLW   "\033[1;33m"
#define DAISY_BLD_BLU   "\033[1;34m"
#define DAISY_BLD_PUR   "\033[1;35m"
#define DAISY_BLD_CYN   "\033[1;36m"
#define DAISY_BLD_WHT   "\033[1;37m"

// Underline
#define DAISY_UND_BLK   "\033[4;30m"
#define DAISY_UND_RED   "\033[4;31m"
#define DAISY_UND_GRN   "\033[4;32m"
#define DAISY_UND_YLW   "\033[4;33m"
#define DAISY_UND_BLU   "\033[4;34m"
#define DAISY_UND_PUR   "\033[4;35m"
#define DAISY_UND_CYN   "\033[4;36m"
#define DAISY_UND_WHT   "\033[4;37m"

// Background
#define DAISY_BAK_BLK	"\033[40m"
#define DAISY_BAK_RED	"\033[41m"
#define DAISY_BAK_GRN   "\033[42m"
#define DAISY_BAK_YLW   "\033[43m"
#define DAISY_BAK_BLU   "\033[44m"
#define DAISY_BAK_PUR   "\033[45m"
#define DAISY_BAK_CYN   "\033[46m"
#define DAISY_BAK_WHT   "\033[47m"
// @}

/** Size of the memory ID */
#define DAISY_IDSIZE	4

#endif	/* DAISYTYPES_H_ */
