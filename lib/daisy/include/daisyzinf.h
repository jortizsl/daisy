//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyzinf.h
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	09-Dec-2016
 * @brief	Class for inflating serialized data using Z lib
 */

#ifndef DAISYZINF_H_
#define DAISYZINF_H_

/*
 * INCLUDES
 */

#include <string.h>
#include <pthread.h>
#include <zlib.h>
#include <iostream>
#include "daisyutils.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start z namespace
namespace z
{


/*
 * CLASS
 */

class Inflater
{
private:
	unsigned long bufferInLen;
	unsigned long bufferInPos;
	unsigned long bufferInCurr;
	unsigned char *bufferIn;

	unsigned long bufferOutLen;
	unsigned long bufferOutPos;
	unsigned char *bufferOut;

	pthread_t threadInflate;
	bool running;
	z_stream strm;

	// Profiling
	double timeTotal;
	double timeInit;
	double timeLoop;
	double timeZ;
	double timeAux;

public:
	Inflater();
	~Inflater();

	bool init(
		unsigned char *bufferOut,
		unsigned long bufferOutLen);

	void startInflate();
	void stopInflate();
	void abortInflate();

	bool putNextChunk(
		unsigned char *buffer,
		unsigned long size);

	void printStats();

private:
	static void *runInflate(void *data);

	/*
private:
	unsigned long bufferInLen;
	unsigned char *bufferIn;

	unsigned long bufferOutLen;
	unsigned long bufferOutPos;
	unsigned long bufferOutCurr;
	unsigned char *bufferOut;

	pthread_t threadInflate;

	bool running;

	z_stream strm;
	unsigned long chunkSize;

public:
	static const int ERR		= 0;
	static const int NEW		= 1;
	static const int TIMEOUT	= 2;
	static const int END		= 3;
	static const int CHUNK_DEFAULT = 16384;

	bool init(
		unsigned char *bufferIn,
		unsigned long bufferInLen,
		unsigned long chunkSize = CHUNK_DEFAULT,
		int level = Z_DEFAULT_COMPRESSION);

	void startDeflate();
	void stopDeflate();

	int getNextChunk(
		unsigned char *buffer,
		unsigned long &size,
		int timeout = 0);

private:
	static void *runInflate(void *data);*/
};

// End z namespace
}

// End daisy namespace
}

#endif	/* DAISYZINF_H_ */
