//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisytftpserver.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	05-Dec-2016
 * @brief	Daisy TFTP server
 */

#ifndef DAISYTFTPSERVER_H_
#define DAISYTFTPSERVER_H_

/*
 * INLCUDES
 */

#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <iostream>
#include <fstream>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include "daisytftp.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start tftp namespace
namespace tftp
{


/*
 * CLASS: Server
 */

class Server
{
private:
	// UDP conection
	int port;

	// Pthreads
	pthread_t threadListen;

	// Read buffer
	unsigned char buffer[BUFFER_SIZE];
	int lBuffer;

	// State
	int state;

	// Profiling
	double timeInit;
	double timeDisk;
	double timeTransmit;
	double timeTotal;
	double timeAux;
	int fileSize;

public:
	Server();
	~Server();

	int listen(int port = 69);

	int finish();

	int getState();

	void printStats();

private:
	static void *runListen(void *arg);

	int sendError(
		int udpSocket,
		struct sockaddr_in remoteAddr,
		int errorCode,
		const char *message);

	int waitForAnswer(
		int udpSocket,
		struct sockaddr_in *remoteAddr,
		socklen_t *remoteAddrLen);

	static void tolower(char *buffer);

	bool getStringFromBuffer(char *str, int &pos);
};

// End tftp namespace
}

// End daisy namespace
}

#endif	/* DAISYTFTPSERVER_H_ */
