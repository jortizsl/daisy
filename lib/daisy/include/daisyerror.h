//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyerror.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	01-Dec-2016
 * @brief	Daisy error
 */

#ifndef DAISYERROR_H_
#define DAISYERROR_H_

/*
 * INCLUDES
 */

#include <string.h>
#include "daisytypes.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start err namespace
namespace err
{

/** Last error */
extern int error;

/** Convert an error code to a string */
char *strerror(int errnum);

/** Maximum length of error description */
const int ERROR_STR_LEN = 256;


	/** @name Error codes */
//@{
// No error
const int NOERR 		= 0;	//!< No error

// Segment error
const int EINVSEG 		= -1;	//!< Invalid segment

// Shared memory errors
const int ESHMOPEN 		= -10;	//!< Error opening shared memory
const int ESHMTRUNC 	= -11;	//!< Error truncating shared memory
const int ESHMSIZE 		= -12;	//!< Invalid size of shared memory
const int ESHMMAP 		= -13;	//!< Error mapping shared memory
const int ESHMINV 		= -14;	//!< Invalid shared memory pointer
const int ESHMUNMAP 	= -15;	//!< Error unmapping shared memory
const int ESHMUNLINK	= -16;	//!< Error unlinking shared memory

// Semaphore errors
const int ESEMINIT 		= -20;	//!< Error initing semaphore
const int ESEMDES 		= -21;	//!< Error destroying semaphore
const int ESEMLOCK 		= -22;	//!< Error locking semaphore
const int ESEMUNLOCK 	= -23;	//!< Error unlocking semaphore
const int ESEMNOINIT 	= -24;	//!< Semaphore not ready

// Standard TFTP errors
const int ENODEF 		= -30;	//!< Not defined, see error message (if any)
const int ENOTFOUND 	= -31;	//!< File not found
const int EACCVIOL 		= -32;	//!< Access violation
const int EFULL 		= -33;	//!< Disk full or allocation exceeded
const int EILLOP 		= -34;	//!< Illegal TFTP operation
const int EUNKID		= -35;	//!< Unknown transfer ID
const int EFILEEXISTS	= -36;	//!< File already exists
const int ENOUSER 		= -37;	//!< Not such user

// Other TFTP errors
const int ESOCK		 	= -40; 	//!< Error creating socket
const int EBIND			= -41;	//!< Error binding socket
const int ERESVHOST		= -42;	//!< Error resolving host
const int ESENDREQ		= -43;	//!< Error sending request
const int ERECV			= -44;	//!< Error receiving data
const int ETIMEOUT		= -45;	//!< Timeout
const int EBLK			= -46;	//!< Wrong data block
const int EOPCODE		= -47;	//!< Unexpected opcode
const int EACK			= -48;	//!< Error sending/receiving ACK
const int EOPEN			= -49;	//!< Error opening file or memory for read or write
const int EMODE			= -50;	//!< Wrong mode or compression

// Server & client errors
const int EMSQCRT		= -60;	//!< Error creating message queue
const int ENOMSQ		= -61;	//!< Message queue not initialized
const int EMSQSEND		= -62;	//!< Error sending message queue message
const int ESETOPT		= -63;	//!< Error setting socket option
//@}


	/** @name detail namespace for private variables and functions */
// @{
namespace detail
{
	/** Error string */
	extern char errorstr[ERROR_STR_LEN];
}
// @}

// End err namespace
}

// End daisy namespace
}

#endif	/* DAISYERROR_H_ */
