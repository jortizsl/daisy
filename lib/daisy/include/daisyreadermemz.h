//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisyreadermemz.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	12-Dec-2016
 * @brief	Daisy reader from compressed shared memory
 */

#ifndef DAISYREADERMEMZ_H_
#define DAISYREADERMEMZ_H_

/*
 * INCLUDES
 */

#include "daisyreadermem.h"
#include "daisyzdef.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: MemReaderZ
 */

class MemReaderZ : public MemReader
{
private:
	daisy::z::Deflater def;
	bool firstChunk;

public:
	MemReaderZ();
	~MemReaderZ();
	bool open(
		const char *name,
		unsigned long beg = 0,
		unsigned long end = 0);
	void close();
	void abort();
	int read(char *buffer, unsigned long lBuffer);
};

// End daisy namespace
}

#endif	/* DAISYREADERMEMZ_H_ */
