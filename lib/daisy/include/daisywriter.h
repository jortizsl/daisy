//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisywriter.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	12-Dec-2016
 * @brief	Daisy writer
 */

#ifndef DAISYWRITER_H_
#define DAISYWRITER_H_

/*
 * INCLUDES
 */

#include <iostream>
#include <string.h>
#include "daisyutils.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

/*
 * CLASS: Writer
 */

class Writer
{
protected:
	/** Writer type */
	int type;

public:
	// Reader types
	static const int TYPE_NONE		= 0;	//!< No type
	static const int TYPE_FILE		= 1;	//!< File
	static const int TYPE_FILE_Z	= 2;	//!< Compressed file
	static const int TYPE_MEM 		= 3;	//!< Memory
	static const int TYPE_MEM_Z 	= 4;	//!< Compressed memory

	/** Constructor */
	Writer() { this->type = TYPE_NONE; }

	/** Destructor */
	virtual ~Writer() {}

	/** Get writer type */
	int getType() { return this->type; }

	/** Open the writer
	 * @param [in] name		Name of the writer
	 * @param [in] beg		Start position
	 * @param [in] end		End position
	 * @return				True if the reader was open properly,
	 * 						False otherwise
	 */
	virtual bool open(
		const char *name,
		unsigned long beg = 0,
		unsigned long end = 0) = 0;

	/** Close the writer */
	virtual void close() = 0;

	/** Force the closing of the writer */
	virtual void abort() = 0;

	/** Write data from the buffer
	 * @param [in] buffer	Buffer to get the data
	 * @param [in] lBuffer	Size of the buffer
	 * @return				Number of bytes read from the buffer
	 */
	virtual int write(const char *buffer, unsigned long lBuffer) = 0;
};

// End daisy namespace
}

#endif	/* DAISYWRITER_H_ */
