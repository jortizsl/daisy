//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	daisytftpclient.h
 * @author	Jesus Ortiz
 * @version	1.0
 * @date	05-Dec-2016
 * @brief	Daisy TFTP client
 */

#ifndef DAISYTFTPCLIENT_H_
#define DAISYTFTPCLIENT_H_

/*
 * INLCUDES
 */

#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <iostream>
#include <fstream>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include "daisytftp.h"


/*
 * NAMESPACE
 */

// Start daisy namespace
namespace daisy
{

// Start tftp namespace
namespace tftp
{

/*
 * CLASS: Client
 */

class Client
{
private:
	// UDP conection
	int udp_s;
	struct sockaddr_in udp_addr;

	// Pthreads
	pthread_t threadRW;

	// Read buffer
	unsigned char buffer[BUFFER_SIZE];
	int lBuffer;

	// State
	int state;

	// Error
	//int lastError;

	// Information for the file transfer
	char remoteFile[FILENAME_LENGTH];
	char localFile[FILENAME_LENGTH];
	//char address[ADDRESS_LENGTH];
	struct sockaddr_in remoteAddr;
	socklen_t remoteAddrLen;
	int port;

	// Options
	int mode;
	int compression;
	int blockSize;

	// Profiling
	double timeInit;
	double timeDisk;
	double timeTransmit;
	double timeTotal;
	double timeAux;
	int fileSize;

public:
	Client();
	~Client();

	void setMode(int mode);

	void setCompression(int compression);

	void setBlockSize(int blockSize);

	void read(
		const char *remoteFile,
		const char *localFile,
		const char *address,
		int port = 69);
	void write(
		const char *remoteFile,
		const char *localFile,
		const char *address,
		int port = 69);

	int readAndWait(
		const char *remoteFile,
		const char *localFile,
		const char *address,
		int port = 69);
	int writeAndWait(
		const char *remoteFile,
		const char *localFile,
		const char *address,
		int port = 69);

	void read(
		const char *remoteFile,
		const char *localFile,
		struct sockaddr_in &address,
		int port = 69);
	void write(
		const char *remoteFile,
		const char *localFile,
		struct sockaddr_in &address,
		int port = 69);

	int readAndWait(
		const char *remoteFile,
		const char *localFile,
		struct sockaddr_in &address,
		int port = 69);
	int writeAndWait(
		const char *remoteFile,
		const char *localFile,
		struct sockaddr_in &address,
		int port = 69);

	int getState();

	void printStats();

private:
	static void *runRead(void *arg);

	static void *runWrite(void *arg);

	int sendError(
		int udpSocket,
		struct sockaddr_in remoteAddr,
		int errorCode,
		const char *message);

	int waitForAnswer(
		int udpSocket,
		struct sockaddr_in *remoteAddr,
		socklen_t *remoteAddrLen);

	void addStringToBuffer(const char *str);

	bool getStringFromBuffer(char *str, int &pos);

	bool resolveAddress(const char *address, int port);
};

// End tftp namespace
}

// End daisy namespace
}

#endif	/* DAISYTFTPCLIENT_H_*/
