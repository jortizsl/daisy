/**
 * @file         unordered_trie.h
 * @author       Jesús Ortiz
 * @version      1.0
 * @date         30-July-2015
 * @brief        Template unordered trie implementation
 */

#ifndef NORDERED_TRIE_H_
#define NORDERED_TRIE_H_

/*
 * INCLUDES
 */

#include <stdlib.h>
#include <string>
#include <iostream>
#include <algorithm>


// STD namespace start
namespace std
{


/** Class std::unordered_trie */
template <class T>
class unordered_trie
{
private:
	class node;
public:
	class iterator;

	/** Class std::unordered_trie<T>::node */
private:
	class node
	{
	protected:
		node *child;
		node *next;
		node *parent;
		node *prev;
		char key;
		pair<string, T> *val;
		friend class iterator;
		friend class unordered_trie;

	public:
		node()
		{
			this->child  = NULL;
			this->next   = NULL;
			this->parent = NULL;
			this->prev   = NULL;
			this->key    = '\0';
			this->val    = NULL;
		}

		~node()
		{
			if (this->val != NULL)
				delete this->val;
		}

		node *add(const char *key, const T &val)
		{
			// Illegal character
			if (key[0] < 0)
				return NULL;

			// Last character
			if (key[1] == '\0')
			{
				// Check character
				if (this->key == '\0')
				{
					this->key = key[0];
					this->newVal(val);
					return this;
				}
				else if (this->key == key[0])
				{
					if (this->val == NULL)		// The key already exists and it's empty
					{
						this->newVal(val);
						return this;
					}
					else
					{							// The key already exists and it's not empty
						return NULL;
					}
				}
				else if (this->next != NULL)	// Check next brother
				{
					return this->next->add(key, val);
				}
				else
				{
					// There is no brother with the name and it's the last
					// character -> Add new element
					this->next = new node;
					this->next->prev = this;
					this->next->parent = this->parent;
					return this->next->add(key, val);
				}
			}

			// Check character
			if (this->key == '\0')
			{
				// Save character
				this->key = key[0];

				if (this->child == NULL)
				{
					// Add the rest of the characters
					this->child = new node;
					this->child->parent = this;
					return this->child->add(key + 1, val);
				}
				else
				{
					// The child should be NULL
					return NULL;
				}
			}
			else if (this->key == key[0])
			{
				if (this->child == NULL)
				{
					// Add the rest of the characters
					this->child = new node;
					this->child->parent = this;
				}

				return this->child->add(key + 1, val);
			}
			else if (this->next != NULL)			// Check next brother
			{
				return this->next->add(key, val);
			}
			else
			{
				// There is no brother with the name
				// -> Add the rest of the characters
				this->next = new node;
				this->next->prev = this;
				this->next->parent = this->parent;
				return this->next->add(key, val);
			}
		}

		node *get(const char *key)
		{
			// Illegal character
			if (key[0] < 0)
				return NULL;

			// Last character
			if (key[1] == '\0')
			{
				if (this->key == key[0])			// Check character
				{
					if (this->val == NULL)
						return NULL;
					else
						return this;
				}
				else if (this->next != NULL)		// Check next brother
				{
					return this->next->get(key);
				}
				else								// Element not found
				{
					return NULL;
				}
			}

			// Check character
			if (this->key == key[0])				// Check character
			{
				if (this->child == NULL)			// There are no more children
					return NULL;
				else
					return this->child->get(key + 1);	// Look for in the child
			}
			else if (this->next != NULL)			// Check next brother
			{
				return this->next->get(key);
			}
			else									// Element not found
			{
				return NULL;
			}
		}

		void clear()
		{
			// Delete all children
			if (this->child != NULL)
			{
				node *aux = this->child;
				node *prevBrother;

				while (aux != NULL)
				{
					prevBrother = aux;
					aux = aux->next;
					delete prevBrother;
				}

				this->child = NULL;
			}
		}

		bool erase(const char *key)
		{
			// Illegal character
			if (key[0] < 0)
				return false;

			// Last character
			if (key[1] == '\0')
			{
				if (this->key == '\0')				// Check character, name not found
				{
					return false;
				}
				else if (this->key == key[0]) 		// Name found -> Delete reference
				{
					this->key = '\0';
					delete this->val;
					this->val = NULL;
					return true;
				}
				else if (this->next != NULL)		// Check next brother
				{
					return this->next->erase(key);
				}
				else // There is no brother with the name and it's the last character
				{
					return false;
				}
			}

			// Check character
			if (this->key == '\0')					// Name not found
			{
				return false;
			}
			else if (this->key == key[0])
			{
				if (this->child == NULL)			// Name not found
					return false;
				else
					return this->child->erase(key + 1);	// Remove from child
			}
			else if (this->next != NULL)			// Check next brother
			{
				return this->next->erase(key);
			}
			else									// There is no brother, not found
			{
				return false;
			}
		}

		node *firstNotEmpty()
		{
			node *n = NULL;

			if (this->val != NULL)
				return this;

			if (this->child != NULL)
			{
				n = this->child->firstNotEmpty();

				if (n != NULL)
					return n;
			}

			if (this->next != NULL)
			{
				n = this->next->firstNotEmpty();

				if (n != NULL)
					return n;
			}

			return NULL;
		}

		void newVal(const T &val)
		{
			this->val = new pair<string, T>;
			this->val->second = val;

			node *n = this;
			while (n != NULL)
			{
				this->val->first += n->key;
				n = n->parent;
			}

			reverse(this->val->first.begin(), this->val->first.end());
		}

		void printRef()
		{
			std::cout << "[" << this->key << "]" << std::endl;
			if (this->child != NULL)
				std::cout << "  child: " << this->child->key << std::endl;
			if (this->next != NULL)
				std::cout << "  next: " << this->next->key << std::endl;
			if (this->prev != NULL)
				std::cout << "  prev: " << this->prev->key << std::endl;
			if (this->parent != NULL)
				std::cout << "  parent: " << this->parent->key << std::endl;

			if (this->child != NULL)
			{
				std::cout << "**CHILD**" << std::endl;
				this->child->printRef();
			}

			if (this->next != NULL)
			{
				std::cout << "**NEXT**" << std::endl;
				this->next->printRef();
			}
		}
	};


	/** Class std::unordered_trie<T>::iterator */
public:
	class iterator
	{
	protected:
		node *n;
		node *root;
		friend class unordered_trie;
		static pair<string, T> dummy;

	public:
		iterator()
		{
			this->n = NULL;
			this->root = NULL;
		}

		iterator(const iterator &it)
		{
			this->n = it.n;
			this->root = it.root;
		}

		~iterator() {}

		pair<std::string, T> &operator *()
		{
			if (this->n != NULL)
				return *this->n->val;
			else
				return dummy;
		}

		pair<std::string, T> *operator ->()
		{

			if (this->n != NULL)
				return this->n->val;
			else
				return &dummy;
		}

		iterator &operator ++()
		{
			this->moveNext();
			return *this;
		}

		iterator operator ++(int)
		{
			iterator aux = *this;
			this->moveNext();
			return aux;
		}

		iterator &operator --()
		{
			this->movePrev();
			return *this;
		}

		iterator operator --(int)
		{
			iterator aux = *this;
			this->movePrev();
			return aux;
		}

		bool operator ==(const iterator &it)
		{
			return it.n == this->n;
		}

		bool operator !=(const iterator &it)
		{
			return it.n != this->n;
		}

		iterator &operator =(const iterator &it)
		{
			this->n = it.n;
			this->root = it.root;
			return *this;
		}

	private:
		void moveNext()
		{
			node *n = this->n;

			while (1)
			{
				// Check the child
				if (n->child != NULL)
				{
					n = n->child;
				}
				// Check the next
				else if (n->next != NULL)
				{
					n = n->next;
				}
				// Go back
				else
				{
					while (1)
					{
						n = n->parent;

						// We finished
						if (n == NULL)
						{
							this->n = NULL;
							return;
						}

						if (n->next != NULL)
						{
							n = n->next;
							break;
						}
					}
				}

				// Found a good one
				if (n->val != NULL)
				{
					this->n = n;
					return;
				}
			}
		}

		void movePrev()
		{
			// TODO
		}
	};


	/** Class std::unordered_trie<T>::const_iterator */
public:
	typedef const iterator const_iterator;


	/* Unordered trie variables */

private:
	iterator _begin;
	iterator _end;
	size_t _size;
	node root;


	/* Unordered trie functions */

public:
	unordered_trie()
	{
		this->_size = 0;
	}

	unordered_trie(const unordered_trie &x)
	{
		*this = x;
	}

	~unordered_trie()
	{

	}

	unordered_trie &operator= (const unordered_trie &x)
	{
		// TODO
		return this;
	}

	iterator begin() { return this->_begin; }
	iterator end() { return unordered_trie::_end; }
	const_iterator cbegin() { return this->_begin; }
	const_iterator cend() { return unordered_trie::_end; }

	const size_t size() { return this->_size; }
	const size_t max_size() { return 0; }
	const bool empty() { return this->_size == 0; }

	T &operator [](const string &key)
	{
		iterator it = this->find(key);
		if (it.n != NULL)
			return it->second;
		else
			return this->root.add(key.c_str(), T())->val->second;
	}

	pair<iterator, bool> insert(const string &key, const T &val)
	{
		iterator it;
		it.root = &this->root;
		it.n = this->root.add(key.c_str(), val);

		if (it.n != NULL)
		{
			this->_size++;
			this->updateBegin();
			return pair<iterator, bool>(it, true);
		}
		else
		{
			return pair<iterator, bool>(it, false);
		}
	}

	void erase(iterator position)
	{
		if (position.n != NULL)
		{
			if (position.n->val != NULL)
			{
				delete position.n->val;
				position.n->val = NULL;
				this->_size--;
				this->updateBegin();
			}
		}
	}

	size_t erase(const string &key)
	{
		if (this->root.erase(key.c_str()))
		{
			this->_size--;
			return 1;
		}
		else
		{
			return 0;
		}
	}

	void clear()
	{
		this->root.clear();
		this->_size = 0;
	}

	iterator find(const string &key)
	{
		iterator it;
		it.root = &this->root;
		it.n = this->root.get(key.c_str());
		return it;
	}

	const_iterator find(const string &key) const
	{
		return this->find(key);
	}

	void printRef()
	{
		this->root.printRef();
	}

private:
	void updateBegin()
	{
		this->_begin.n = this->root.firstNotEmpty();
		this->_begin.root = &this->root;
	}
};

template <class T>
pair<string, T> unordered_trie<T>::iterator::dummy;

// STD namespace end
}

#endif	/* NORDERED_TRIE_H_ */
