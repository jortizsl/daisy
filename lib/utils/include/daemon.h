/**
 * @file	daemon.h
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	07-Apr-2017
 * @brief	Functions to convert a process in a daemon (fork from NuxDaemon)
 */

#ifndef DAEMON_H_
#define DAEMON_H_

/*
 * INCLUDES
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>
#include <errno.h>
#include <pwd.h>
#include <signal.h>
#include <string>


/*
 * NAMESPACE
 */

namespace utils
{

namespace daemon
{


/*
 * DEFINES
 */

#define DAEMON_CLR_RESET		"\033[0m"
#define DAEMON_CLR_TIMESTAMP	"\033[0;32m"
#define DAEMON_CLR_USER			"\033[1;34m"
#define DAEMON_CLR_NAME			"\033[1;31m"


/*
 * VARIABLES
 */

extern std::string name;
extern bool running;
extern bool is_daemon;
extern std::string daemonUser;
extern char logUser[64];
extern int logPID;
extern bool use_colors;


/*
 * FUNCTIONS
 */

/** Starts the daemon system. Set signal functions. Configure log. */
void init(const std::string &name, bool daemon, const std::string &daemonUser);

/** Finish the daemon system. Basically close the log. */
void finish();


/*
 * LOGGING FUNCTIONS
 */

/** Open a log. If it's a daemon, it open the syslog.
 *  If it's not a daemon, it prints in the standard output.
 *  Search for openlog for reference.
 */
void nopenlog(const char *ident, int option, int facility);

/** Log a message. Search for openlog for reference. */
void nsyslog(int facility_priority, const char *format, ...);

/** Closes the log. Search for openlog for reference. */
void ncloselog();


/*
 * SIGNAL HANDLERS
 */

/** Manages the signals coming from the child process. */
void childSignalHandler(int signum);

/** Manages termination signals. */
void signalHandler(int signum);


/*
 * DAEMONIZE FUNCTION
 */

/** Daemonize the process */
void daemonize();


// End of namespace
}

}

#endif	/* DAEMON_H_ */
