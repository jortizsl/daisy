/**
 * @file         trie.h
 * @author       Jesús Ortiz
 * @version      1.0
 * @date         30-July-2015
 * @brief        Template trie implementation
 */

#ifndef TRIE_H_
#define TRIE_H_

/*
 * INCLUDES
 */

#include <stdlib.h>
#include <string>
#include <iostream>
#include <algorithm>


// STD namespace start
namespace std
{


/** Class std::trie */
template <class T>
class trie
{
private:
	class node;
public:
	class iterator;

	/** Class std::trie<T>::node */
private:
	class node
	{
	protected:
		node *child;
		node *next;
		node *parent;
		node *prev;
		char key;
		pair<string, T> *val;
		friend class iterator;
		friend class trie;

	public:
		node()
		{
			this->child  = NULL;
			this->next   = NULL;
			this->parent = NULL;
			this->prev   = NULL;
			this->key    = '\0';
			this->val    = NULL;
		}

		~node()
		{
			if (this->val != NULL)
				delete this->val;
			this->val = NULL;
		}

		node *add(const char *key, const T &val)
		{
			// Illegal character
			if (key[0] < 0)
				return NULL;

			// Last character
			if (key[1] == '\0')
			{
				// Check character
				if (this->key == '\0')
				{
					this->key = key[0];
					this->newVal(val);
					return this;
				}
				else if (this->key == key[0])
				{
					// The key already exists and it's empty
					if (this->val == NULL)
					{
						this->newVal(val);
						return this;
					}
					// The key already exists and it's not empty
					else
					{
						return NULL;
					}
				}
				// Check if the insertion is on the left
				else if (this->key > key[0])
				{
					node *aux = this->prev;
					this->prev = new node;
					this->prev->prev = aux;
					this->prev->parent = this->parent;
					this->prev->next = this;
					if (aux != NULL)
						aux->next = this->prev;
					else if (this->parent != NULL)
						this->parent->child = this->prev;
					this->prev->key = key[0];
					this->prev->newVal(val);
					return this->prev;
				}
				// Check next brother
				else if (this->next != NULL)
				{
					return this->next->add(key, val);
				}
				// There is no brother with the name and it's the last
				// character -> Add new element
				else
				{
					this->next = new node;
					this->next->prev = this;
					this->next->parent = this->parent;
					return this->next->add(key, val);
				}
			}

			// Check character
			if (this->key == '\0')
			{
				// Save character
				this->key = key[0];

				if (this->child == NULL)
				{
					// Add the rest of the characters
					this->child = new node;
					this->child->parent = this;
					return this->child->add(key + 1, val);
				}
				else
				{
					// The child should be NULL
					return NULL;
				}
			}
			else if (this->key == key[0])
			{
				if (this->child == NULL)
				{
					// Add the rest of the characters
					this->child = new node;
					this->child->parent = this;
				}

				return this->child->add(key + 1, val);
			}
			// Check if the insertion is on the left
			else if (this->key > key[0])
			{
				node *aux = this->prev;
				this->prev = new node;
				this->prev->prev = aux;
				this->prev->parent = this->parent;
				this->prev->next = this;
				if (aux != NULL)
					aux->next = this->prev;
				else if (this->parent != NULL)
					this->parent->child = this->prev;
				return this->prev->add(key, val);
			}
			// Check next brother
			else if (this->next != NULL)
			{
				return this->next->add(key, val);
			}
			// There is no brother with the name
			// -> Add the rest of the characters
			else
			{
				this->next = new node;
				this->next->prev = this;
				this->next->parent = this->parent;
				return this->next->add(key, val);
			}
		}

		node *get(const char *key)
		{
			// Illegal character
			if (key[0] < 0)
				return NULL;

			// Last character
			if (key[1] == '\0')
			{
				// Check character
				if (this->key == key[0])
				{
					if (this->val == NULL)
						return NULL;
					else
						return this;
				}
				// Check next brother
				else if (this->next != NULL)
				{
					return this->next->get(key);
				}
				// Element not found
				else
				{
					return NULL;
				}
			}

			// Check character
			if (this->key == key[0])
			{
				// There are no more children
				if (this->child == NULL)
					return NULL;
				// Look for in the child
				else
					return this->child->get(key + 1);
			}
			// Check next brother
			else if (this->next != NULL)
			{
				return this->next->get(key);
			}
			// Element not found
			else
			{
				return NULL;
			}
		}

		void clear()
		{
			// Delete all children
			if (this->child != NULL)
			{
				this->child->clear();
				delete this->child;
				this->child = NULL;
			}

			// Delete all brothers
			if (this->next != NULL)
			{
				this->next->clear();
				delete this->next;
				this->next = NULL;
			}

			this->key = '\0';
		}

		bool erase(const char *key)
		{
			// Illegal character
			if (key[0] < 0)
				return false;

			// Last character
			if (key[1] == '\0')
			{
				if (this->key == '\0')				// Check character, name not found
				{
					return false;
				}
				else if (this->key == key[0]) 		// Name found -> Delete reference
				{
					delete this->val;
					this->val = NULL;
					return true;
				}
				else if (this->next != NULL)		// Check next brother
				{
					return this->next->erase(key);
				}
				else // There is no brother with the name and it's the last character
				{
					return false;
				}
			}

			// Check character
			if (this->key == '\0')					// Name not found
			{
				return false;
			}
			else if (this->key == key[0])
			{
				if (this->child == NULL)			// Name not found
					return false;
				else
					return this->child->erase(key + 1);	// Remove from child
			}
			else if (this->next != NULL)			// Check next brother
			{
				return this->next->erase(key);
			}
			else									// There is no brother, not found
			{
				return false;
			}
		}

		node *firstNotEmpty()
		{
			node *n = NULL;

			if (this->val != NULL)
				return this;

			if (this->child != NULL)
			{
				n = this->child->firstNotEmpty();

				if (n != NULL)
					return n;
			}

			if (this->next != NULL)
			{
				n = this->next->firstNotEmpty();

				if (n != NULL)
					return n;
			}

			return NULL;
		}

		void newVal(const T &val)
		{
			this->val = new pair<string, T>;
			this->val->second = val;

			node *n = this;
			while (n != NULL)
			{
				this->val->first += n->key;
				n = n->parent;
			}

			reverse(this->val->first.begin(), this->val->first.end());
		}

		void printRef()
		{
			std::cout << "[" << this->key << "]" << std::endl;
			if (this->child != NULL)
				std::cout << "  child: " << this->child->key << std::endl;
			if (this->next != NULL)
				std::cout << "  next: " << this->next->key << std::endl;
			if (this->prev != NULL)
				std::cout << "  prev: " << this->prev->key << std::endl;
			if (this->parent != NULL)
				std::cout << "  parent: " << this->parent->key << std::endl;
			if (this->val != NULL)
				std::cout << "  with value" << std::endl;

			if (this->child != NULL)
			{
				std::cout << "**CHILD**" << std::endl;
				this->child->printRef();
			}

			if (this->next != NULL)
			{
				std::cout << "**NEXT**" << std::endl;
				this->next->printRef();
			}
		}
	};


	/** Class std::trie<T>::iterator */
public:
	class iterator
	{
	protected:
		node *n;
		friend class trie;
		static pair<string, T> dummy;

	public:
		iterator()
		{
			this->n = NULL;
		}

		iterator(const iterator &it)
		{
			this->n = it.n;
		}

		~iterator() {}

		pair<std::string, T> &operator *()
		{
			if (this->n != NULL)
				return *this->n->val;
			else
				return dummy;
		}

		pair<std::string, T> *operator ->()
		{
			if (this->n != NULL)
				return this->n->val;
			else
				return &dummy;
		}

		iterator &operator ++()
		{
			this->moveNext();
			return *this;
		}

		iterator operator ++(int)
		{
			iterator aux = *this;
			this->moveNext();
			return aux;
		}

		iterator &operator --()
		{
			this->movePrev();
			return *this;
		}

		iterator operator --(int)
		{
			iterator aux = *this;
			this->movePrev();
			return aux;
		}

		bool operator ==(const iterator &it)
		{
			return it.n == this->n;
		}

		bool operator !=(const iterator &it)
		{
			return it.n != this->n;
		}

		iterator &operator =(const iterator &it)
		{
			this->n = it.n;
			return *this;
		}

	private:
		void moveNext()
		{
			node *n = this->n;

			while (1)
			{
				// Check the child
				if (n->child != NULL)
				{
					n = n->child;
				}
				// Check the next
				else if (n->next != NULL)
				{
					n = n->next;
				}
				// Go back
				else
				{
					while (1)
					{
						n = n->parent;

						// We finished
						if (n == NULL)
						{
							this->n = NULL;
							return;
						}

						if (n->next != NULL)
						{
							n = n->next;
							break;
						}
					}
				}

				// Found a good one
				if (n->val != NULL)
				{
					this->n = n;
					return;
				}
			}
		}

		void movePrev()
		{
			// TODO
		}
	};


	/** Class std::trie<T>::const_iterator */
public:
	typedef const iterator const_iterator;


	/* Trie variables */

private:
	iterator _begin;
	iterator _end;
	size_t _size;
	node *root;


	/* Trie functions */

public:
	trie()
	{
		this->_size = 0;
		this->root = new node;
	}

	trie(const trie &x)
	{
		this->_size = 0;
		this->root = new node;
		*this = x;
	}

	~trie()
	{
		this->clear();
		delete this->root;
	}

	trie &operator= (const trie &x)
	{
		trie<T>::iterator it;
		trie<T> &xnc = const_cast<trie<T>&>(x);

		this->clear();
		for (it = xnc.begin(); it != xnc.end(); it++)
			this->insert(it->first, it->second);

		return *this;
	}

	iterator begin() { return this->_begin; }
	iterator end() { return trie::_end; }
	const_iterator cbegin() { return this->_begin; }
	const_iterator cend() { return trie::_end; }

	const size_t size() { return this->_size; }
	const size_t max_size() { return 0; }
	const bool empty() { return this->_size == 0; }

	T &operator [](const string &key)
	{
		iterator it = this->find(key);
		if (it.n != NULL)
		{
			return it->second;
		}
		else
		{
			// If we didn't find the key, add a new element
			node *aux = this->root->add(key.c_str(), T());

			// Check root
			if (this->root->prev != NULL)
				this->root = this->root->prev;

			// Return value
			this->updateBegin();
			return aux->val->second;
		}
	}

	pair<iterator, bool> insert(const string &key, const T &val)
	{
		iterator it;
		it.n = this->root->add(key.c_str(), val);

		if (it.n != NULL)
		{
			// Check root
			if (this->root->prev != NULL)
				this->root = this->root->prev;

			this->_size++;
			this->updateBegin();
			return pair<iterator, bool>(it, true);
		}
		else
		{
			return pair<iterator, bool>(it, false);
		}
	}

	void erase(iterator position)
	{
		if (position.n != NULL)
		{
			if (position.n->val != NULL)
			{
				delete position.n->val;
				position.n->val = NULL;
				this->_size--;
				this->updateBegin();
			}
		}
	}

	size_t erase(const string &key)
	{
		if (this->root->erase(key.c_str()))
		{
			this->_size--;
			this->updateBegin();
			return 1;
		}
		else
		{
			this->updateBegin();
			return 0;
		}
	}

	void clear()
	{
		this->root->clear();
		this->updateBegin();
		this->_size = 0;
	}

	iterator find(const string &key)
	{
		iterator it;
		it.n = this->root->get(key.c_str());
		return it;
	}

	const_iterator find(const string &key) const
	{
		return this->find(key);
	}

	void printRef()
	{
		this->root->printRef();
	}

private:
	void updateBegin()
	{
		this->_begin.n = this->root->firstNotEmpty();
	}
};

template <class T>
pair<string, T> trie<T>::iterator::dummy;

// STD namespace end
}

#endif	/* TRIE_H_ */
