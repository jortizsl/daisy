#include <iostream>
#include <string.h>

#include "trie.h"
#include "unordered_trie.h"

int main(int argc, char *argv[])
{
	// Variables
	std::trie<int>::iterator it;
	std::trie<int>::iterator element;
	int counter = 0;
	std::trie<int> trie;


	std::cout << "**** START ****" << std::endl;


	std::cout << "==== ADD ELEMENTS ... ";

	// Add elements
	const int nValues = 8;
	/*
	int values[nValues] = {
		   7,     3,     4,    12,  15,  11,    5,     9};
	const char *names[nValues] = {
		"to", "tea", "ted", "ten", "a", "i", "in", "inn"};*/
	int values[nValues] = {
		   7,     3,  15,     9,     4,    12,  11,    5};
	const char *names[nValues] = {
		"to", "tea", "a", "inn", "ten", "ted", "i", "in"};

	for (int i = 0; i < nValues; i++)
	{
		if (!trie.insert(names[i], values[i]).second)
		{
			std::cout << "[ERROR] Error adding [" << names[i] << "]" << std::endl;
			return -1;
		}
	}

	std::cout << "[OK]" << std::endl;


	std::cout << "==== PRINT ELEMENTS ====" << std::endl;

	// Get elements
	for (it = trie.begin(); it != trie.end(); it++)
	{
		std::cout << it->first << ": " << it->second << std::endl;
		counter++;
	}

	std::cout << "[OK]" << std::endl;


	std::cout << "==== GET ELEMENTS ... ";

	for (int i = 0; i < nValues; i++)
	{
		// Get elements
		element = trie.find(names[i]);

		if (element == trie.end())
		{
			std::cout << "[ERROR] Element not found" << std::endl;
			return -1;
		}

		if ((*element).second != values[i])
		{
			std::cout << "[ERROR] Wrong element value" << std::endl;
			return -1;
		}

		if (element->second != values[i])
		{
			std::cout << "[ERROR] Wrong element value" << std::endl;
			return -1;
		}
	}

	std::cout << "[OK]" << std::endl;


	std::cout << "==== GET WRONG ELEMENTS ... ";

	// Get elements
	element = trie.find("tone");

	if (element != trie.end())
	{
		std::cout << "[ERROR] Element should not be found" << std::endl;
		return -1;
	}

	std::cout << "[OK]" << std::endl;


	std::cout << "==== PRINT ELEMENTS ====" << std::endl;

	// Get elements
	for (int i = 0; i < nValues; i++)
	{
		std::cout << names[i] << ": " << trie[names[i]] << std::endl;
	}

	std::cout << "[OK]" << std::endl;


	trie["patata"] = 10;


	std::cout << "==== PRINT ELEMENTS ====" << std::endl;

	// Get elements
	counter = 0;

	for (it = trie.begin(); it != trie.end(); it++)
	{
		std::cout << it->first << ": " << it->second << std::endl;
		counter++;
	}

	if (counter != nValues + 1)
	{
		std::cout << "[ERROR] Wrong number of given elements" << std::endl;
		return -1;
	}

	std::cout << "[OK]" << std::endl;


	std::cout << "**** FINISH ****" << std::endl;

	// Return ok
	return 0;
}
