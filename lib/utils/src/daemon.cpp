/**
 * @file	daemon.cpp
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	07-Apr-2017
 * @brief	Functions to convert a process in a daemon (fork from daemon)
 */

/*
 * INCLUDES
 */

#include "daemon.h"


/*
 * NAMESPACE
 */

namespace utils
{

namespace daemon
{

/*
 * VARIABLES
 */

std::string name;
bool running = true;
bool is_daemon = false;
std::string daemonUser;
char logUser[64];
int logPID;
bool use_colors = false;


/*
 * FUNCTIONS
 */

// Starts the daemon system. Set signal functions. Configure log.
void init(const std::string &name, bool daemon, const std::string &daemonUser)
{
		/* Save variables */

	utils::daemon::is_daemon = daemon;
	utils::daemon::name = name;
    utils::daemon::daemonUser = daemonUser;


		/* Manage signals to exit gracefully */

    signal(SIGINT,  utils::daemon::signalHandler);
    signal(SIGTERM, utils::daemon::signalHandler);
    signal(SIGKILL, utils::daemon::signalHandler);


    	/* Initialize logging */

    /*
     * To log the messages in a different file edit the syslog configuration
     * file and add the following lines:
     *
     *   $Umask 0000
     *   $FileCreateMode 0644
     *   local0.*      -/var/log/nux.log
     *   $FileCreateMode 0640
     *   $Umask 0022
     *
     * In Debian the configuration file is:
     *
     *   /etc/rsyslog.conf
     *
     */

    utils::daemon::nopenlog(name.c_str(), LOG_PID, LOG_LOCAL0);
    utils::daemon::nsyslog(LOG_INFO, "starting");


    	/* Daemonize */

    if (is_daemon)
    	daemonize();
}

// Finish the daemon system. Basically close the log.
void finish()
{
	utils::daemon::nsyslog(LOG_INFO, "terminated");
    utils::daemon::ncloselog();
}


/*
 * LOGGING FUNCTIONS
 */

// Open a log
void nopenlog(const char *ident, int option, int facility)
{
	if (utils::daemon::is_daemon)
	{
		openlog(ident, option, facility);
	}
	else
	{
        uid_t uid = geteuid();
		struct passwd *pw = getpwuid(uid);
		strcpy(utils::daemon::logUser, pw->pw_name);
		utils::daemon::logPID = getpid();
	}
}

// Log a message
void nsyslog(int facility_priority, const char *format, ...)
{
	static time_t nowTimeRaw;
	static struct tm *nowTimeLocal;
	static char nowDateStr[64];
	static char nowTimeStr[64];

	va_list ap;
	va_start(ap, format);
	if (utils::daemon::is_daemon)
	{
		vsyslog(facility_priority, format, ap);
	}
	else
	{
		nowTimeRaw = time(0);
		nowTimeLocal = localtime(&nowTimeRaw);
		strftime(nowDateStr, 64, "%h %d", nowTimeLocal);
		strftime(nowTimeStr, 64, "%H:%M:%S", nowTimeLocal);
		if (use_colors)
		{
			printf(
				DAEMON_CLR_TIMESTAMP"%s %s "
				DAEMON_CLR_USER"%s "
				DAEMON_CLR_NAME"%s[%d]: "
				DAEMON_CLR_RESET,
				nowDateStr,
				nowTimeStr,
				logUser,
				utils::daemon::name.c_str(),
				logPID);
		}
		else
		{
			printf("%s %s %s %s[%d]: ",
				nowDateStr,
				nowTimeStr,
				logUser,
				utils::daemon::name.c_str(),
				logPID);
		}
		vprintf(format, ap);
		printf("\n");
	}
	va_end(ap);
}

// Closes the log
void ncloselog()
{
	if (utils::daemon::is_daemon)
		closelog();
}


/*
 * SIGNAL HANDLERS
 */

// Manages the signals coming from the child process
void childSignalHandler(int signum)
{
	switch (signum)
    {
    	case SIGALRM:
    		exit(1);
    		break;
    	case SIGUSR1:
    		exit(0);
    		break;
    	case SIGCHLD:
    		exit(1);
    		break;
    }
}

// Manages termination signals
void signalHandler(int signum)
{
	switch (signum)
    {
    	case SIGINT:
    	case SIGTERM:
    	case SIGKILL:
    		running = false;
    		break;
    }
}


/*
 * DAEMONIZE FUNCTION
 */

// Daemonize the process
void daemonize()
{
	pid_t pid;
    pid_t sid;
    pid_t parent;

    // Already a daemon
    if (getppid() == 1)
    	return;

    // Drop user if there is one, and we were run as root
    if (getuid() == 0 || geteuid() == 0)
    {
        struct passwd *pw = getpwnam(daemonUser.c_str());

        if (pw)
        {
            syslog(LOG_INFO, "setting user to %s", daemonUser.c_str());
            setuid(pw->pw_uid);
        }
    }

    // Trap signals that we expect to recieve
    signal(SIGCHLD, childSignalHandler);
    signal(SIGUSR1, childSignalHandler);
    signal(SIGALRM, childSignalHandler);

    // Fork off the parent process
    pid = fork();

    if (pid < 0)
    {
        syslog(LOG_ERR, "unable to fork daemon, code=%d (%s)",
        	errno, strerror(errno));
        exit(EXIT_FAILURE);
    }

    // If we got a good PID, then we can exit the parent process.
    if (pid > 0)
    {
        /* Wait for confirmation from the child via SIGTERM or SIGCHLD, or
           for two seconds to elapse (SIGALRM).  pause() should not return. */
        alarm(2);
        pause();
        exit(EXIT_FAILURE);
    }

    // At this point we are executing as the child process
    parent = getppid();

    // Cancel certain signals
    signal(SIGCHLD, SIG_DFL); // A child process dies
    signal(SIGTSTP, SIG_IGN); // Various TTY signals
    signal(SIGTTOU, SIG_IGN);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGHUP,  SIG_IGN); // Ignore hangup signal
    //signal(SIGTERM, SIG_DFL); // Die on SIGTERM

    // Change the file mode mask
    umask(0);

    // Create a new SID for the child process
    sid = setsid();

    if (sid < 0)
    {
        syslog(LOG_ERR, "unable to create a new session, code %d (%s)",
        	errno, strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Change the current working directory.  This prevents the current
       directory from being locked; hence not being able to remove it. */
    if ((chdir("/")) < 0)
    {
        syslog(LOG_ERR, "unable to change directory to %s, code %d (%s)",
        	"/", errno, strerror(errno));
        exit(EXIT_FAILURE);
    }

    // Redirect standard files to /dev/null
    freopen("/dev/null", "r", stdin);
    freopen("/dev/null", "w", stdout);
    freopen("/dev/null", "w", stderr);

    // Tell the parent process that we are A-okay
    kill(parent, SIGUSR1);
}


// End of namespace
}

}
