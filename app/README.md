Folder to put the applications.

Each project has at least two folders:

 - include -> Include files
 - src     -> Source files
 
In the root folder of the project it should be a CMakeLists.txt file to
compile the project with CMake.

