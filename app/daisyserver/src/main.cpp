//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	/app/daisyserver/src/main.cpp
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	07-Apr-2017
 * @brief	Daisy server
 */

/*
 * INCLUDES
 */

	/* General */
#include <getopt.h>

	/* YAML */
#include <yaml-cpp/yaml.h>

	/* Daisy */
#include "daemon.h"
#include "daisy.h"


/*
 * DEFINES
 */

// Daemon name
#define DAEMON_NAME "daisyserver"


/*
 * FUNCTIONS
 */

void display_help()
{
	printf("usage: daisyserver [OPTIONS]\n");
	printf("\n");
	printf("Options:\n");
	printf("  -h, --help           Displays this message\n");
	printf("  -L, --license        Display license\n");
	printf("  -d, --daemon         Daemonize server (Wuhahahaha!)\n");
	printf("  -c, --config=CONFIG  Configuration file\n");
	printf("  -u, --user=USER      Starts the daemon using USER\n");
}

void display_license()
{
	printf("Copyright (C) 2016 Jesus Ortiz\n");
	printf("\n");
	printf("This program is free software: you can redistribute it and/or modify\n");
    printf("it under the terms of the GNU General Public License as published by\n");
    printf("the Free Software Foundation, either version 3 of the License, or\n");
    printf("(at your option) any later version.\n");
    printf("\n");
    printf("This program is distributed in the hope that it will be useful,\n");
    printf("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
    printf("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
    printf("GNU General Public License for more details.\n");
    printf("\n");
    printf("You should have received a copy of the GNU General Public License\n");
    printf("along with this program.  If not, see <https://www.gnu.org/licenses/>.\n");
}

bool loadConfig(
	std::string &configFilename,
	int &udpport,
    std::string &udpdev,
    std::string &msqname,
	bool &msqreset,
    int &tftpport)
{
		/* Open file */

	std::ifstream configFile;
	configFile.open(configFilename.c_str());

	if (!configFile)
		return false;


		/* Parse file */

	YAML::Parser parser(configFile);
	YAML::Node doc;
	parser.GetNextDocument(doc);

	doc["udp-port"]     >> udpport;
	doc["udp-dev"]      >> udpdev;
	doc["mqueue-name"]  >> msqname;
	doc["mqueue-reset"] >> msqreset;
	doc["tftp-port"]    >> tftpport;


		/* Close file */

	configFile.close();

	return true;
}


/*
 * MAIN
 */

/** Main function */
int main(int argc, char *argv[])
{
		/* Manage arguments */

	char short_options[] = "dc:u:hL";
	struct option long_options[] = {
		{"daemon", no_argument,       0, 'd'},
		{"config", required_argument, 0, 'c'},
		{"user",   required_argument, 0, 'u'},
		{"help",   no_argument,       0, 'h'},
		{"license",no_argument,       0, 'L'},
		{0, 0, 0, 0}};
	int opt_index;
	int opt;
	uid_t uid = geteuid();
	struct passwd *pw = getpwuid(uid);
	char daemonUser[64];
	strcpy(daemonUser, pw->pw_name);
	std::string configFile =
		std::string(pw->pw_dir) +
		"/.daisy/etc/config/daisyserver.yaml";
	bool is_daemon = false;

	while (1)
	{
		// getopt_long stores the option index here
		opt_index = 0;

		opt = getopt_long(
			argc, argv, short_options, long_options, &opt_index);

		// Detect the end of the options
		if (opt == -1)
			break;

		switch (opt)
		{
		case 0:
			if (long_options[opt_index].flag != 0)
				break;
			printf ("option %s", long_options[opt_index].name);
			if (optarg)
				printf (" with arg %s", optarg);
			printf ("\n");
			break;
		case 'h':
			display_help();
			exit(0);
			break;
		case 'L':
			display_license();
			exit(0);
			break;
		case 'd':
			is_daemon = true;
			break;
		case 'c':
			configFile = optarg;
			break;
		case 'u':
			strcpy(daemonUser, optarg);
			break;
		case '?':
			display_help();
			exit(-1);
			break;
		default:
			printf("invalid option %c\n", opt);
			display_help();
			exit(-1);
		}
	}


		/* Load config file */

	int udpport;
    std::string udpdev;
    std::string msqname;
    bool msqreset;
    int tftpport;

    if (!loadConfig(configFile, udpport, udpdev, msqname, msqreset, tftpport))
    {
    	printf("Error loading configuration file\n");
    	return 0;
    }


		/* Init daemon */

	utils::daemon::use_colors = true;
	utils::daemon::init(DAEMON_NAME, is_daemon, daemonUser);


		/* Start daisy server */

    daisy::Server server;
    server.init(udpport, udpdev, msqname, tftpport, msqreset);

    while (utils::daemon::running)
    {
    	sleep(1);
    }

    server.finish();


		/* Finish daemon and exit */

    utils::daemon::finish();

	// Return OK
	return 0;
}
