//-----------------------------------------------------------------------------
//
// Copyright (C) 2016 Jesus Ortiz
//
// This file is part of Daisy.
//
// Daisy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Daisy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Daisy. If not, see <http://www.gnu.org/licenses/>.
//
//-----------------------------------------------------------------------------

/**
 * @file	app/daisymem/src/main.cpp
 * @author	Jesús Ortiz
 * @version	1.0
 * @date	20-Apr-2017
 * @brief	Daisy memory utility
 */

/*
 * INCLUDES
 */

	/* General */
#include <getopt.h>

	/* Folders and files */
#include <dirent.h>
#include <fnmatch.h>

	/* Shared memory */
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

	/* YAML */
#include <yaml-cpp/yaml.h>

	/* Daisy */
#include "daisy.h"


/*
 * FUNCTIONS
 */

bool openSegment(daisy::mem::Segment &segment, const std::string &name)
{
	// Open shared memory segment
	int fd = shm_open(name.c_str(), O_CREAT | O_RDWR, S_IRWXU);

	// Check shared memory file descriptor
	if (fd == -1)
	{
		daisy::err::error = daisy::err::ESHMOPEN;
		return false;
	}

	// The shared memory size has to be different of zero
	struct stat sb;
	fstat(fd, &sb);

	if (sb.st_size == 0)
	{
		daisy::err::error = daisy::err::ESHMTRUNC;
		return false;
	}
	segment.size = sb.st_size;
	segment.sizeData = sb.st_size - sizeof(sem_t) - sizeof(uint8_t);

	// Map the memory
	segment.addr = mmap(
		NULL, segment.size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	// Check if memory was mapped properly
	if (segment.addr == NULL)
	{
		daisy::err::error = daisy::err::ESHMMAP;
		return false;
	}

	segment.sem = (sem_t*)segment.addr;
	segment.nproc = (uint8_t*)(((char*)segment.addr) + sizeof(sem_t));
	segment.data = (void*)(((char*)segment.addr) + sizeof(sem_t) + sizeof(uint8_t));

	// Init semaphore
	if (sem_init(segment.sem, 1, 1) == -1)
	{
		daisy::err::error = daisy::err::ESEMINIT;
		return false;
	}

	// Increment process counter
	if (sem_wait(segment.sem) == -1)
	{
		daisy::err::error = daisy::err::ESEMLOCK;
		return false;
	}
	(*segment.nproc)++;
	if (sem_post(segment.sem) == -1)
	{
		daisy::err::error = daisy::err::ESEMUNLOCK;
		return false;
	}

	// Return OK
	return true;
}

bool closeSegment(daisy::mem::Segment &segment)
{
	// Decrement process counter
	bool mustClose = false;
	if (sem_wait(segment.sem) == -1)
	{
		daisy::err::error = daisy::err::ESEMLOCK;
		return false;
	}
	(*segment.nproc)--;
	if ((*segment.nproc) == 0)
		mustClose = true;
	if (sem_post(segment.sem) == -1)
	{
		daisy::err::error = daisy::err::ESEMUNLOCK;
		return false;
	}

	// Destroy semaphore
	if (mustClose)
	{
		if (sem_destroy(segment.sem) == -1)
		{
			daisy::err::error = daisy::err::ESEMDES;
			return false;
		}
	}
	segment.sem = NULL;

	// Unmap the shared memory
	if (munmap(segment.addr, segment.size) == -1)
	{
		daisy::err::error = daisy::err::ESHMUNMAP;
		return false;
	}
	segment.size = 0;
	segment.addr = NULL;
	segment.sizeData = 0;
	segment.data = NULL;

	// Unlink shared memory
	if (mustClose)
	{
		if (shm_unlink(segment.nameShared.c_str()) == -1)
		{
			daisy::err::error = daisy::err::ESHMUNLINK;
			return false;
		}
	}

	// Return OK
	return true;
}

bool deleteSegment(daisy::mem::Segment &segment)
{
	// Destroy semaphore
	if (sem_destroy(segment.sem) == -1)
	{
		daisy::err::error = daisy::err::ESEMDES;
		return false;
	}

	// Unmap the shared memory
	if (munmap(segment.addr, segment.size) == -1)
	{
		daisy::err::error = daisy::err::ESHMUNMAP;
		return false;
	}
	segment.size = 0;
	segment.addr = NULL;
	segment.sizeData = 0;
	segment.data = NULL;

	// Unlink shared memory
	if (shm_unlink(segment.nameShared.c_str()) == -1)
	{
		daisy::err::error = daisy::err::ESHMUNLINK;
		return false;
	}

	// Return OK
	return true;
}

void display_help()
{
	printf("usage: daisymem [OPTIONS]\n");
	printf("\n");
	printf("Options:\n");
	printf("  -h, --help           Displays this message\n");
	printf("  -L, --license        Display license\n");
	printf("  -l, --list           List segments in the shared memory\n");
	printf("  -i, --info=PATTERN   Displays segment(s) information\n");
	printf("  -d, --delete=PATTERN Deletes segment(s)\n");
	printf("  -D, --dump=SEGMENT   Dump segment data (it can be used with hexdump)\n");
}

void display_license()
{
	printf("Copyright (C) 2016 Jesus Ortiz\n");
	printf("\n");
	printf("This program is free software: you can redistribute it and/or modify\n");
    printf("it under the terms of the GNU General Public License as published by\n");
    printf("the Free Software Foundation, either version 3 of the License, or\n");
    printf("(at your option) any later version.\n");
    printf("\n");
    printf("This program is distributed in the hope that it will be useful,\n");
    printf("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
    printf("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
    printf("GNU General Public License for more details.\n");
    printf("\n");
    printf("You should have received a copy of the GNU General Public License\n");
    printf("along with this program.  If not, see <https://www.gnu.org/licenses/>.\n");
}


/*
 * MAIN
 */

/** Main function */
int main(int argc, char *argv[])
{
		/* Manage arguments */

	char short_options[] = "hLli:d:D:";
	struct option long_options[] = {
		{"help",   no_argument,       0, 'h'},
		{"license",no_argument,       0, 'L'},
		{"list",   no_argument,       0, 'l'},
		{"info",   required_argument, 0, 'i'},
		{"delete", required_argument, 0, 'd'},
		{"dump",   required_argument, 0, 'D'},
		{0, 0, 0, 0}};
	int opt_index;
	int opt;
	bool opt_list = false;
	std::string opt_info;
	std::string opt_delete;
	std::string opt_dump;

	while (1)
	{
		// getopt_long stores the option index here
		opt_index = 0;

		opt = getopt_long(
			argc, argv, short_options, long_options, &opt_index);

		// Detect the end of the options
		if (opt == -1)
			break;

		switch (opt)
		{
		case 0:
			if (long_options[opt_index].flag != 0)
				break;
			printf ("option %s", long_options[opt_index].name);
			if (optarg)
				printf (" with arg %s", optarg);
			printf ("\n");
			break;
		case 'h':
			display_help();
			exit(0);
			break;
		case 'L':
			display_license();
			exit(0);
			break;
		case 'l':
			opt_list = true;
			break;
		case 'i':
			opt_info = optarg;
			break;
		case 'd':
			opt_delete = optarg;
			break;
		case 'D':
			opt_dump = optarg;
			break;
		case '?':
			display_help();
			exit(-1);
			break;
		default:
			printf("invalid option %c\n", opt);
			display_help();
			exit(-1);
		}
	}


		/* Get list of segments in the shared memory */

	DIR *dir;
	struct dirent *ent;
	std::trie<daisy::mem::Segment> segments;
	daisy::mem::Segment segment;

	// Read file by directory
	if ((dir = opendir("/dev/shm/")) != NULL)
	{
		while ((ent = readdir(dir)) != NULL)
		{
			// Check prefix
			if (fnmatch("daisy-shm-*", ent->d_name, 0) != 0)
				continue;

			// Open segment and add to list
			if (!openSegment(segment, ent->d_name))
			{
				daisy::sanityCheck();
				daisy::err::error = daisy::err::NOERR;
			}
			else
			{
				segment.name = &ent->d_name[10];
				segment.nameShared = ent->d_name;
				segments.insert(segment.name, segment);
			}
		}

		closedir(dir);
	}


		/* Display list of segments */

	if (opt_list)
	{
		std::trie<daisy::mem::Segment>::iterator segIt;
		for (segIt = segments.begin(); segIt != segments.end(); segIt++)
			std::cout << segIt->first << "\n";
	}
	else if (!opt_info.empty())
	{
		std::trie<daisy::mem::Segment>::iterator segIt;

		for (segIt = segments.begin(); segIt != segments.end(); segIt++)
		{
			// Check expression
			if (fnmatch(opt_info.c_str(), segIt->first.c_str(), 0) != 0)
				continue;

			int semVal;
			if (sem_getvalue(segIt->second.sem, &semVal) != 0)
				semVal = -1;

			std::cout << segIt->first << "\n";
			std::cout << "  name       : " << segIt->second.name << "\n";
			std::cout << "  shared name: " << segIt->second.nameShared << "\n";
			std::cout << "  size       : " << segIt->second.size << "\n";
			std::cout << "  size data  : " << segIt->second.sizeData << "\n";
			std::cout << "  processes  : " << (int)*segIt->second.nproc << "\n";
			if (semVal == -1)
				std::cout << "  sem        : error" << "\n";
			else if (semVal == 0)
				std::cout << "  sem        : locked" << "\n";
			else
				std::cout << "  sem        : unlocked (" << semVal << ")\n";
		}
	}
	else if (!opt_delete.empty())
	{
		std::trie<daisy::mem::Segment>::iterator segIt = segments.begin();

		while (segIt != segments.end())
		{
			// Check expression
			if (fnmatch(opt_delete.c_str(), segIt->first.c_str(), 0) != 0)
			{
				segIt++;
				continue;
			}

			// Delete segment
			deleteSegment(segIt->second);
			segments.erase(segIt);
			segIt++;
		}
	}
	else if (!opt_dump.empty())
	{
		std::trie<daisy::mem::Segment>::iterator segIt;
		segIt = segments.find(opt_dump);

		if (segIt != segments.end())
			write(1, segIt->second.data, segIt->second.sizeData);
	}


		/* Deallocate segments */

	std::trie<daisy::mem::Segment>::iterator segIt;
	for (segIt = segments.begin(); segIt != segments.end(); segIt++)
	{
		closeSegment(segIt->second);
	}


		/* Exit */

	// Return OK
	return 0;
}
