Daisy
=====

Distributed Shared Memory library


File structure
--------------

The folders of the project are the following:

 - app     -> Folder containing source code of applications
 - etc     -> Folder containing non source code files
   - cmake   -> CMake files (find files and other auxiliary files)
   - scripts -> Folder containing scripts
 - lib     -> Folder containing source code of libraries
 
In the root folder of the project it should be a CMakeLists.txt file to
compile the project with CMake.


Compilation
-----------

These are the steps to compile Daisy (using only CMake):

    $ cd path/to/daisy/source
    daisy$ mkdir build
    daisy$ cd build
    daisy/build$ cmake ..
    daisy/build$ make
    daisy/build$ make install      (the default folder is a local folder)

To compile in debug mode run CMake with the following arguments:

    daisy/build$ cmake .. -DCMAKE_BUILD_TYPE=Debug

To compile in release mode run CMake with the following arguments:

    daisy/build$ cmake .. -DCMAKE_BUILD_TYPE=Release

These are the steps to compile Daisy (using configure.sh script):

    $ cd path/to/daisy/source
    daisy$ mkdir build
    daisy$ cd build
    daisy/build$ ../scripts/configure.sh    (see instructions below)
    daisy/build$ make
    daisy/build$ make install      (the default folder is a local folder)

The configure script accepts several parameters:

 - (-h), (--help). Displays a help message
 - (-g), (--debug). Compiles in debug mode
 - (-r), (--release). Compiles in release mode
 - (--reset). Removes all the contents of the folder before calling CMake
 - (-DCMAKEVARIABLE). Calls CMake with some variable

A typical call to the configuration script would be:

    daisy/build$ ../scripts/configure.sh --reset -r


Installation
------------

To install the programs you can use the following command:

    daisy/build$ make install

    
License
-------

Copyright (C) 2016 Jesus Ortiz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
