#!/bin/bash

#------------------------------------------------------------------------------
# Copyright (C) 2016 Jesus Ortiz
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
#
# This script creates a minimum chroot environment
# 
# Author:  Jesus Ortiz
# email:   jesus.ortiz@iit.it
# Data:    07-Mar-2017
# Version: 1.0
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONFIGURATION
#------------------------------------------------------------------------------

BASH_PATH="/bin/bash"
BUSYBOX_DOWNLOAD="https://busybox.net/downloads/binaries/1.26.2-i686/busybox"


#------------------------------------------------------------------------------
# COLORS
#------------------------------------------------------------------------------

COLOR_RESET="\033[0m"
COLOR_INFO="\033[0;32m"
COLOR_ITEM="\033[1;34m"
COLOR_QUES="\033[1;32m"
COLOR_WARN="\033[0;33m"
COLOR_CODE="\033[0m"
COLOR_BOLD="\033[1m"
COLOR_UNDE="\033[4m"


#------------------------------------------------------------------------------
# FUNCTIONS
#------------------------------------------------------------------------------

# Function to reset the terminal colors
function color_reset
{
  echo -ne "\033[0m"
}

# Function to display the help
function display_help
{
  color_reset
  echo -e "${COLOR_BOLD}NAME${COLOR_RESET}"
  echo -e "       create_chroot.sh - Script to create a minimal chroot tree"
  echo -e ""
  echo -e "${COLOR_BOLD}SYNOPSIS${COLOR_RESET}"
  echo -e "       ${COLOR_BOLD}create_chroot.sh${COLOR_RESET} [${COLOR_UNDE}OPTIONS${COLOR_RESET}]... PATH"
  echo -e ""
  echo -e "${COLOR_BOLD}DESCRIPTION${COLOR_RESET}"
  echo -e "       This script creates a minimal chroot tree"
  echo -e ""
  echo -e "       You can add the following options when calling the script:"
  echo -e ""
  echo -e "       ${COLOR_BOLD}--help${COLOR_RESET}, ${COLOR_BOLD}-h${COLOR_RESET}"
  echo -e "              Display this help message"
  echo -e ""
  echo -e "${COLOR_BOLD}AUTHORS${COLOR_RESET}"
  echo -e "       Written by Jesus Ortiz."
  echo -e ""
  echo -e "${COLOR_BOLD}REPORTING BUGS${COLOR_RESET}"
  echo -e "       Report bugs to <jesus.ortiz@iit.it>"
  echo -e ""
  echo -e "${COLOR_BOLD}COPYRIGHT${COLOR_RESET}"
  echo -e "       Copyright (C) 2016 Jesus Ortiz"
  echo -e ""     
  echo -e "       This program is free software: you can redistribute it and/or modify"
  echo -e "       it under the terms of the GNU General Public License as published by"
  echo -e "       the Free Software Foundation, either version 3 of the License, or"
  echo -e "       (at your option) any later version."
  echo -e ""
  echo -e "       This program is distributed in the hope that it will be useful,"
  echo -e "       but WITHOUT ANY WARRANTY; without even the implied warranty of"
  echo -e "       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
  echo -e "       GNU General Public License for more details."
  echo -e ""
  echo -e "       You should have received a copy of the GNU General Public License"
  echo -e "       along with this program. If not, see <http://www.gnu.org/licenses/>."
  echo -e ""
  color_reset
}

function copy_bin_with_libraries
{
  echo -e "  ${COLOR_INFO}Copying ${COLOR_ITEM}$1${COLOR_INFO}...${COLOR_RESET}"
  cp $1 ${CHROOT_PATH}/bin

  echo -e "  ${COLOR_INFO}Copying ${COLOR_ITEM}$1${COLOR_INFO} libraries...${COLOR_RESET}"
  LIBS=$( ldd $1 | awk '{print $(NF-1)}' )
  FIRST_ITEM=0
  for LIB in ${LIBS}
  do
    if [ $FIRST_ITEM == 0 ]; then
      FIRST_ITEM=$(( $FIRST_ITEM + 1 ))
    else
      cp $LIB ${CHROOT_PATH}/lib
    fi
  done

  # Copy manually this library
  cp /lib64/ld-linux-x86-64.so.2 ${CHROOT_PATH}/lib64
}


#------------------------------------------------------------------------------
# INIT THINGS
#------------------------------------------------------------------------------

# Reset terminal color
color_reset
echo ""


#------------------------------------------------------------------------------
# Manage arguments
#------------------------------------------------------------------------------

# Check number of arguments
if [ $# != 1 ]; then
  display_help
  exit
fi

# Check if first argument is the help
if [ $1 == "--help" ] || [ $1 == "-h" ]; then
  display_help
  exit
fi

# At this point, the only argument should be the path
CHROOT_PATH=$1
echo -e "  ${COLOR_INFO}Creating chroot in ${COLOR_ITEM}${CHROOT_PATH}${COLOR_INFO}...${COLOR_RESET}"


#------------------------------------------------------------------------------
# Create folders
#------------------------------------------------------------------------------

echo -e "  ${COLOR_INFO}Creating folders...${COLOR_RESET}"
mkdir -p $1
mkdir -p ${CHROOT_PATH}/{bin,dev/shm,dev/mqueue,lib,usr,etc,lib64}


#------------------------------------------------------------------------------
# Copy bash and libraries
#------------------------------------------------------------------------------

copy_bin_with_libraries ${BASH_PATH}


#------------------------------------------------------------------------------
# Install busybox
#------------------------------------------------------------------------------

echo -e "  ${COLOR_INFO}Downloading ${COLOR_ITEM}Busybox${COLOR_INFO}...${COLOR_RESET}"
wget ${BUSYBOX_DOWNLOAD}
mv busybox ${CHROOT_PATH}/bin
chmod +x ${CHROOT_PATH}/bin/busybox

echo -e "  ${COLOR_INFO}Creating symbolic links for ${COLOR_ITEM}Busybox${COLOR_INFO}...${COLOR_RESET}"
PWD_CURR=$( pwd )
cd ${CHROOT_PATH}
for i in $(busybox --list)
do
  ln -s /bin/busybox bin/$i
done
cd $PWD_CURR


#------------------------------------------------------------------------------
# Mount chroot dev partition
#------------------------------------------------------------------------------

echo -e "    ${COLOR_INFO}- Mount chroot dev partition (with root):"
echo -e "        ${COLOR_CODE}mount -t tmpfs tmpfs CHROOT-FOLDER/dev/shm${COLOR_RESET}"


#------------------------------------------------------------------------------
# Generate basic bashrc
#------------------------------------------------------------------------------

echo "PS1=\"${CHROOT_PATH}:\w\$ \"" > ${CHROOT_PATH}/etc/bash.bashrc


#------------------------------------------------------------------------------
# Display instructions and finish
#------------------------------------------------------------------------------

echo -e "  ${COLOR_INFO}Instructions:"
echo -e "    ${COLOR_INFO}- Install ${COLOR_ITEM}fakechroot${COLOR_INFO}"
echo -e "        ${COLOR_CODE}apt-get install fakechroot${COLOR_RESET}"
echo -e "    ${COLOR_INFO}- Configure ${COLOR_ITEM}fakechroot${COLOR_INFO}"
echo -e "    ${COLOR_INFO}- Enter in the chroot:"
echo -e "        ${COLOR_CODE}fakechroot chroot CHROOT-FOLDER${COLOR_RESET}"
color_reset

