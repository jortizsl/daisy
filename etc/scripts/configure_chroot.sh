#!/bin/bash

#------------------------------------------------------------------------------
# Copyright (C) 2016 Jesus Ortiz
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
#
# This script configures the chroot environment
# 
# Author:  Jesus Ortiz
# email:   jesus.ortiz@iit.it
# Data:    09-Mar-2017
# Version: 1.0
#
#------------------------------------------------------------------------------


#------------------------------------------------------------------------------
# FUNCTIONS
#------------------------------------------------------------------------------

# Function to reset the terminal colors
function color_reset
{
  echo -ne "\033[0m"
}

# Function to display the help
function display_help
{
  color_reset
  echo -e "${COLOR_BOLD}NAME${COLOR_RESET}"
  echo -e "       configure_chroot.sh - Script to configure the chroot environment"
  echo -e ""
  echo -e "${COLOR_BOLD}SYNOPSIS${COLOR_RESET}"
  echo -e "       ${COLOR_BOLD}configure_chroot.sh${COLOR_RESET} [${COLOR_UNDE}OPTIONS${COLOR_RESET}]... PATH"
  echo -e ""
  echo -e "${COLOR_BOLD}DESCRIPTION${COLOR_RESET}"
  echo -e "       This script configures the chroot environment:"
  echo -e ""
  echo -e "         - Mount a tmpfs on the dev/shm for enabling shared memory in the chroot"
  echo -e ""
  echo -e "       You can add the following options when calling the script:"
  echo -e ""
  echo -e "       ${COLOR_BOLD}--help${COLOR_RESET}, ${COLOR_BOLD}-h${COLOR_RESET}"
  echo -e "              Display this help message"
  echo -e ""
  echo -e "${COLOR_BOLD}AUTHORS${COLOR_RESET}"
  echo -e "       Written by Jesus Ortiz."
  echo -e ""
  echo -e "${COLOR_BOLD}REPORTING BUGS${COLOR_RESET}"
  echo -e "       Report bugs to <jesus.ortiz@iit.it>"
  echo -e ""
  echo -e "${COLOR_BOLD}COPYRIGHT${COLOR_RESET}"
  echo -e "       Copyright (C) 2016 Jesus Ortiz"
  echo -e ""     
  echo -e "       This program is free software: you can redistribute it and/or modify"
  echo -e "       it under the terms of the GNU General Public License as published by"
  echo -e "       the Free Software Foundation, either version 3 of the License, or"
  echo -e "       (at your option) any later version."
  echo -e ""
  echo -e "       This program is distributed in the hope that it will be useful,"
  echo -e "       but WITHOUT ANY WARRANTY; without even the implied warranty of"
  echo -e "       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
  echo -e "       GNU General Public License for more details."
  echo -e ""
  echo -e "       You should have received a copy of the GNU General Public License"
  echo -e "       along with this program. If not, see <http://www.gnu.org/licenses/>."
  echo -e ""
  color_reset
}


#------------------------------------------------------------------------------
# CONSTANTS
#------------------------------------------------------------------------------

# List of usefull colors
COLOR_RESET="\033[0m"
COLOR_INFO="\033[0;32m"
COLOR_ITEM="\033[1;34m"
COLOR_QUES="\033[1;32m"
COLOR_WARN="\033[0;33m"
COLOR_CODE="\033[0m"
COLOR_BOLD="\033[1m"
COLOR_UNDE="\033[4m"


#------------------------------------------------------------------------------
# INIT THINGS
#------------------------------------------------------------------------------

# Reset terminal color
color_reset
echo ""


#------------------------------------------------------------------------------
# Manage arguments
#------------------------------------------------------------------------------

# Check arguments
if [ $# == 1 ]; then
  if [ $1 == "--help" ] || [ $1 == "-h" ]; then
    display_help
    exit
  else
    CHROOT_PATH=$1
  fi
elif [ $# > 1 ]; then
  display_help
  exit
fi


#------------------------------------------------------------------------------
# RUN WITH SUDO
#------------------------------------------------------------------------------

# Check user and run as root if necessary
if [ "$USER" != "root" ]; then
  SUDO=$(sudo -v 2>&1)

  if [ -z "$SUDO" ]; then  # We can run the command with sudo
    echo -e "${COLOR_INFO}Running the script with sudo${COLOR_RESET}"
    sudo bash $0 $*
  else                        # We need to login as root
    echo -e "${COLOR_INFO}Login as root${COLOR_RESET}"
    su -c "bash $0 $*"
  fi

  exit
fi


#------------------------------------------------------------------------------
# MOUNT TMPFS on DEV/SHM
#------------------------------------------------------------------------------

mount -t tmpfs tmpfs ${CHROOT_PATH}/dev/shm

