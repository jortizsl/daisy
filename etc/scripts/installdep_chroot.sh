#!/bin/bash

#------------------------------------------------------------------------------
# Copyright (C) 2016 Jesus Ortiz
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
#
# This script install the libraries required by an executable inside chroot
# 
# Author:  Jesus Ortiz
# email:   jesus.ortiz@iit.it
# Data:    09-Mar-2017
# Version: 1.0
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# COLORS
#------------------------------------------------------------------------------

COLOR_RESET="\033[0m"
COLOR_INFO="\033[0;32m"
COLOR_ITEM="\033[1;34m"
COLOR_QUES="\033[1;32m"
COLOR_WARN="\033[0;33m"
COLOR_CODE="\033[0m"
COLOR_BOLD="\033[1m"
COLOR_UNDE="\033[4m"


#------------------------------------------------------------------------------
# FUNCTIONS
#------------------------------------------------------------------------------

# Function to reset the terminal colors
function color_reset
{
  echo -ne "\033[0m"
}

# Function to display the help
function display_help
{
  color_reset
  echo -e "${COLOR_BOLD}NAME${COLOR_RESET}"
  echo -e "       installdep_chroot.sh - Script to install all the libraries required"
  echo -e "       by an executable inside chroot."
  echo -e ""
  echo -e "${COLOR_BOLD}SYNOPSIS${COLOR_RESET}"
  echo -e "       ${COLOR_BOLD}installdep_chroot.sh${COLOR_RESET} [${COLOR_UNDE}OPTIONS${COLOR_RESET}]... CHROOT_PATH BIN_PATH"
  echo -e ""
  echo -e "${COLOR_BOLD}DESCRIPTION${COLOR_RESET}"
  echo -e "       This script install all the libraries required by an executable inside"
  echo -e "       chroot."
  echo -e ""
  echo -e "       You can add the following options when calling the script:"
  echo -e ""
  echo -e "       ${COLOR_BOLD}-h${COLOR_RESET}, ${COLOR_BOLD}--help${COLOR_RESET}"
  echo -e "              Display this help message"
  echo -e ""
  echo -e "${COLOR_BOLD}AUTHORS${COLOR_RESET}"
  echo -e "       Written by Jesus Ortiz."
  echo -e ""
  echo -e "${COLOR_BOLD}REPORTING BUGS${COLOR_RESET}"
  echo -e "       Report bugs to <jesus.ortiz@iit.it>"
  echo -e ""
  echo -e "${COLOR_BOLD}COPYRIGHT${COLOR_RESET}"
  echo -e "       Copyright (C) 2016 Jesus Ortiz"
  echo -e ""     
  echo -e "       This program is free software: you can redistribute it and/or modify"
  echo -e "       it under the terms of the GNU General Public License as published by"
  echo -e "       the Free Software Foundation, either version 3 of the License, or"
  echo -e "       (at your option) any later version."
  echo -e ""
  echo -e "       This program is distributed in the hope that it will be useful,"
  echo -e "       but WITHOUT ANY WARRANTY; without even the implied warranty of"
  echo -e "       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
  echo -e "       GNU General Public License for more details."
  echo -e ""
  echo -e "       You should have received a copy of the GNU General Public License"
  echo -e "       along with this program. If not, see <http://www.gnu.org/licenses/>."
  echo -e ""
  color_reset
}

function copy_dep_libraries
{
  echo -e "  ${COLOR_INFO}Copying ${COLOR_ITEM}$2${COLOR_INFO} libraries into ${COLOR_ITEM}$1/lib${COLOR_INFO}...${COLOR_RESET}"
  LIBS=$( ldd "$2" | awk '{print $(NF-1)}' )
  FIRST_ITEM=0
  for LIB in ${LIBS}
  do
    if [ $FIRST_ITEM == 0 ]; then
      FIRST_ITEM=$(( $FIRST_ITEM + 1 ))
    else
      cp $LIB $1/lib
    fi
  done
}


#------------------------------------------------------------------------------
# INIT THINGS
#------------------------------------------------------------------------------

# Reset terminal color
color_reset
echo ""


#------------------------------------------------------------------------------
# Manage arguments
#------------------------------------------------------------------------------

# Check number of arguments
if [ $# == 1 ]; then
  # Check if first argument is the help
  if [ $1 == "--help" ] || [ $1 == "-h" ]; then
    display_help
    exit
  else
    display_help
    exit
  fi
elif [ $# != 2 ]; then
  display_help
  exit
fi


#------------------------------------------------------------------------------
# Copy dependent libraries
#------------------------------------------------------------------------------

TARGET_PATH=$1/$2

# Check if the argument is a folder or a file
if [[ -d ${TARGET_PATH} ]]; then
  for file in "${TARGET_PATH}"/*
  do
    copy_dep_libraries $1 ${file}
  done
elif [[ -f ${TARGET_PATH} ]]; then
    copy_dep_libraries $1 $1/$2
else
    echo "Invalid path"
    exit
fi

exit

color_reset

