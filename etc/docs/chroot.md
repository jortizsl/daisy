How to create a chroot to run the software
==========================================

Prepare filesystem
------------------

Create folders:

        $ mkdir -pv CHROOT-FOLDER/{bin,dev/shm,lib,usr}

Copy bash:

        $ cp /bin/bash CHROOT-FOLDER/bin

Copy basic libraries:

        $ cp /lib/x86_64-linux-gnu/libc.so.6 CHROOT-FOLDER/lib
        $ cp /lib/x86_64-linux-gnu/libdl.so.2 CHROOT-FOLDER/lib
        $ cp /lib/x86_64-linux-gnu/libgcc_s.so.1 CHROOT-FOLDER/lib
        $ cp /lib/x86_64-linux-gnu/libncurses.so.5 CHROOT-FOLDER/lib
        $ cp /lib/x86_64-linux-gnu/libpthread.so.0 CHROOT-FOLDER/lib
        $ cp /lib/x86_64-linux-gnu/librt.so.1 CHROOT-FOLDER/lib
        $ cp /usr/lib/x86_64-linux-gnu/libstdc++.so.6 CHROOT-FOLDER/lib
        $ cp /lib/x86_64-linux-gnu/libtinfo.so.5 CHROOT-FOLDER/lib
        $ cp /lib/x86_64-linux-gnu/libz.so.1 CHROOT-FOLDER/lib
        $ cp /lib64/ld-linux-x86-64.so.2 CHROOT-FOLDER/lib64

To know what libraries you need to copy

        $ ldd BIN-FILE


Install Busybox
---------------

Download file:

        $ wget https://busybox.net/downloads/binaries/1.26.2-i686/busybox

Move busybox to our chroot:

        $ mv busybox CHROOT-FOLDER/bin
        $ chmod +x CHROOT-FOLDER/bin/busybox


Use fakechroot
--------------

Copy fakeroot configuration to the user:

        $ cp /etc/fakechroot/ ~/.fakechroot -r

Comment the line adding paths to "FAKECHROOT_EXCLUDE_PATH".

Launch fake chroot:

        $ fakechroot chroot CHROOT-FOLDER


Prepare dev
-----------

Mount a temporary filesystem on dev/shm (with root):

        $ mount -t tmpfs tmpfs CHROOT-FOLDER/dev/shm


Finish the installation of busybox
----------------------------------

        chroot$ exec /bin/busybox --install -s /bin

