Synchronization methods
=======================

Put negotiation
---------------

| Client 1 | Server 1        | Server 2        | Client 2 |
| -------- | --------------- | --------------- | -------- |
| put      |                 |                 |          |
| MSQ (p)  | MSQ (p)         |                 |          |
| continue | UDP (broadcast) | UDP (broadcast) |          |
|          | continue        | Add to list     |          |
|          |                 | continue        |          |


Get negotiation (not sync)
--------------------------

Existing segment:

| Client 1 | Server 1        | Server 2        | Client 2 |
| -------- | --------------- | --------------- | -------- |
| get      |                 |                 |          |
| MSQ (g)  | MSQ (g)         |                 |          |
| continue | Check list      |                 |          |
|          | TFTP read       | TFTP read       |          |
|          | continue        | continue        |          |


Non existing segment:

| Client 1 | Server 1        | Server 2        | Client 2 |
| -------- | --------------- | --------------- | -------- |
| get      |                 |                 |          |
| MSQ (g)  | MSQ (g)         |                 |          |
| continue | Check list      |                 |          |
|          | continue        |                 |          |


Get negotiation (notify, not sync)
----------------------------------

Existing segment:

| Client 1 | Server 1        | Server 2        | Client 2 |
| -------- | --------------- | --------------- | -------- |
| get      |                 |                 |          |
| lock     |                 |                 |          |
| MSQ (n)  | MSQ (n)         |                 |          |
| lock     | Check list      |                 |          |
|          | unlock          |                 |          |
| unlock   | TFTP read       | TFTP read       |          |
| continue | continue        | continue        |          |

Non existing segment:

| Client 1 | Server 1        | Server 2        | Client 2 |
| -------- | --------------- | --------------- | -------- |
| get      |                 |                 |          |
| lock     |                 |                 |          |
| MSQ (n)  | MSQ (n)         |                 |          |
| lock     | Check list      |                 |          |
|          | unlock          |                 |          |
| unlock   | continue        |                 |          |
| continue |                 |                 |          |


Get negotiation (sync)
----------------------

Existing segment:

| Client 1 | Server 1        | Server 2        | Client 2 |
| -------- | --------------- | --------------- | -------- |
| get      |                 |                 |          |
| lock     |                 |                 |          |
| MSQ (s)  | MSQ (s)         |                 |          |
| lock     | Check list      |                 |          |
|          | TFTP read       | TFTP read       |          |
| unlock   | unlock          | continue        |          |
| continue | continue        |                 |          |


Non existing segment:

| Client 1 | Server 1        | Server 2        | Client 2 |
| -------- | --------------- | --------------- | -------- |
| get      |                 |                 |          |
| lock     |                 |                 |          |
| MSQ (s)  | MSQ (s)         |                 |          |
| lock     | Check list      |                 |          |
| unlock   | unlock          |                 |          |
| continue | continue        |                 |          |


