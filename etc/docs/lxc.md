As root
-------

apt-get install uidmap
apt-get install cgmanager
echo 1 > /sys/fs/cgroup/cpuset/cgroup.clone_children
echo 1 > /proc/sys/kernel/unprivileged_userns_clone
usermod --add-subgids 100000-165536 $USER
usermod --add-subuids 100000-165536 $USER
systemctl start cgmanager
mkdir /usr/lib/x86_64-linux-gnu/lxc/proc
mount --bind /proc /usr/lib/x86_64-linux-gnu/lxc/proc
cgm create all $USER
cgm chown all $USER 1000 1000


As user
-------

cgm movepid all $USER $$
lxc-checkconfig
chmod +x .local
chmod +x .local/share/
lxc-create -t download -n test1
lxc-start -n test1 -d
lxc-ls
lxc-info -n test1
lxc-console -n test1
lxc-attach -n test1


LXC configuration
-----------------

mkdir ~/.config/lxc
cp /etc/lxc/default.conf ~/.config/lxc

Add the following lines in default.conf

lxc.network.type = veth
lxc.network.link = lxcbr0
lxc.network.flags = up
lxc.network.hwaddr = 00:16:3e:xx:xx:xx
lxc.id_map = u 0 100000 65537
lxc.id_map = g 0 100000 65537

lxc.id_map = u 0 1345184 65536
lxc.id_map = g 0 1345184 65536

To see what ID to put, run the following command:

grep $USER /etc/subuid
grep $USER /etc/subgid

