TFTP protocol, with options
===========================

TFTP standards
--------------

 - [RFC 1350](https://tools.ietf.org/html/rfc1350)
 - [RFC 2347](https://tools.ietf.org/html/rfc2347)


Brief description
-----------------

### Opcodes ###

        opcode  operation
          1     Read request (RRQ)
          2     Write request (WRQ)
          3     Data (DATA)
          4     Acknowledgment (ACK)
          5     Error (ERROR)
          6     Acknowledgment with options (OACK)

### Modes ###

"netascii", "octet", or "mail"

In our case we add "shm", for shared memory. The filename field indicates the
name of the area of the shared memory.

### RRQ ###

          2        n          1   m      1
        +--------+----------+---+------+---+
        | Opcode | Filename | 0 | Mode | 0 |
        +--------+----------+---+------+---+

Fields:

 - Opcode -> 01 (2 bytes)
 - Filaname -> null terminated string (n + 1 bytes)
 - Mode -> null terminated string (m + 1 bytes)

### RRQ with options ###

          2        n          1   m      1
        +--------+----------+---+------+--->
        | Opcode | Filename | 0 | Mode | 0 |
        +--------+----------+---+------+--->

          o1     1   v1       1    X    oN     1   vN       1       
        >------+---+--------+---+-----+------+---+--------+---+
        | Opt1 | 0 | Value1 | 0 | ... | OptN | 0 | ValueN | 0 |
        >------+---+--------+---+-----+------+---+--------+---+

Fields:

 - Opcode -> 01 (2 bytes)
 - Filaname -> null terminated string (n + 1 bytes)
 - Mode -> null terminated string (m + 1 bytes)
 - Opt1 -> null terminated string (o1 + 1 bytes)
 - Value 1 -> null terminated string (v1 + 1 bytes)
 - ...
 - OptN -> null terminated string (oN + 1 bytes)
 - Value N -> null terminated string (vN + 1 bytes)

### WRQ ###

          2        n          1   m      1
        +--------+----------+---+------+---+
        | Opcode | Filename | 0 | Mode | 0 |
        +--------+----------+---+------+---+

Fields:

 - Opcode -> 02 (2 bytes)
 - Filaname -> null terminated string (n + 1 bytes)
 - Mode -> null terminated string (m + 1 bytes)

### WRQ with options ###

          2        n          1   m      1
        +--------+----------+---+------+--->
        | Opcode | Filename | 0 | Mode | 0 |
        +--------+----------+---+------+--->

          o1     1   v1       1    X    oN     1   vN       1       
        >------+---+--------+---+-----+------+---+--------+---+
        | Opt1 | 0 | Value1 | 0 | ... | OptN | 0 | ValueN | 0 |
        >------+---+--------+---+-----+------+---+--------+---+

Fields:

 - Opcode -> 02 (2 bytes)
 - Filaname -> null terminated string (n + 1 bytes)
 - Mode -> null terminated string (m + 1 bytes)
 - Opt1 -> null terminated string (o1 + 1 bytes)
 - Value 1 -> null terminated string (v1 + 1 bytes)
 - ...
 - OptN -> null terminated string (oN + 1 bytes)
 - Value N -> null terminated string (vN + 1 bytes)

### DATA ###

          2        2         n
        +--------+---------+------+
        | Opcode | Block # | Data |
        +--------+---------+------+

Fields:

 - Opcode -> 03 (2 bytes)
 - Block # -> 2 bytes
 - Data -> n bytes

### ACK ###

          2        2
        +--------+---------+
        | Opcode | Block # |
        +--------+---------+

Fields:

 - Opcode -> 04 (2 bytes)
 - Block # -> 2 bytes


### ERROR ###

          2        2           n        1
        +--------+-----------+--------+---+
        | Opcode | ErrorCode | ErrMsg | 0 |
        +--------+-----------+--------+---+

Fields:

 - Opcode -> 05 (2 bytes)
 - ErrorCode -> 2 bytes
 - ErrMsg -> null terminated string (n + 1 bytes)

### OACK ###

          2        o1     1   v1       1    X    oN     1   vN       1 
        +--------+------+---+--------+---+-----+------+---+--------+---+
        | Opcode | Opt1 | 0 | Value1 | 0 | ... | OptN | 0 | ValueN | 0 |
        +--------+------+---+--------+---+-----+------+---+--------+---+

Fields:

 - Opcode -> 06 (2 bytes)
 - Opt1 -> null terminated string (o1 + 1 bytes)
 - Value 1 -> null terminated string (v1 + 1 bytes)
 - ...
 - OptN -> null terminated string (oN + 1 bytes)
 - Value N -> null terminated string (vN + 1 bytes)


Options
-------

Options of the protocol:

 - "beg", start byte of the transmission. value = start byte (only with shm)
 - "end", end byte of the transmission. value = end byte (only with shm)
 - "comp", send the data compressed. value = "zlib" (more to add later)
 - "blksize", block size. value = block size


Pipeline
--------

### Client request read file example, without options ###

        Client             Server
        -----------------  ----------
        RRQ                DATA (block 1)
        ACK (block 1)      DATA (block 2)
        ACK (...)          DATA (...)
        ACK (block n - 1)  DATA (block n) (data size is less than 512 or 0)
        ACK (block n)

### Client request write file example, without options ###

        Client          Server
        --------------  ----------
        WRQ             ACK (block 0)
        DATA (block 1)  ACK (block 1)
        DATA (...)      ACK (...)
        DATA (block n)  ACK (block n)

### Client request read file example, with options ###

        Client             Server
        -----------------  ----------
        RRQ (options)      OACK (options)
        ACK (block 0)      DATA (block 1)
        ACK (block 1)      DATA (block 2)
        ACK (...)          DATA (...)
        ACK (block n - 1)  DATA (block n) (data size is less than 512 or 0)
        ACK (block n)

### Client request write file example, with options ###

        Client          Server
        --------------  ----------
        WRQ (options)   OACK (options)
        DATA (block 1)  ACK (block 1)
        DATA (...)      ACK (...)
        DATA (block n)  ACK (block n)

